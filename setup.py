#!/usr/bin/env python
# encoding: UTF8

import glob
import pdb
from setuptools import setup, find_packages

name = 'Martin B. Eriksen'
email = 'martin.b.eriksen@gmail.com'

data_files = [
    ('sci/config', glob.glob('sci/config/*.yaml')),
    ('sci/config/pop', glob.glob('sci/config/pop/*.yaml')),
    ('sci/config/survey', glob.glob('sci/config/survey/*.yaml')),
    ('sci/data/detf', glob.glob('sci/data/detf/*'))]

print(data_files)

setup(
    name = 'sci',
    version = '2',
    packages = find_packages(),

    install_requires = [
        'Python >= 2.6',
        'numpy > 1.5',
        'ipdb',
        'pandas',
        'Scipy',
        'Emcee',
        'Tables'],
    author = name,
    author_email = email,
    data_files = data_files,
    license = 'Read LICENSE.txt',
    maintainer = name,
    maintainer_email = email,
    entry_points = {
        'brownthrower.task': [
         'chain = sci.tasks.chain:chain',
         'fisher = sci.tasks.fisher:fisher',
         'mcmc = sci.tasks.mcmc:mcmc',
         'meta = sci.tasks.meta:meta',
         'thpkg = sci.tasks.thpkg:thpkg']
    }
)
