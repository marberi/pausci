#!/usr/bin/env python
# encoding: UTF8

# Library to construct some terms for the Clenshaw
# Curtis integration (see Wikipedia). Useful since the
# factors are used different places.

import pdb
import numpy as np
from scipy.linalg import block_diag

def find_D(N):
    """Array D in the integration routine."""

    assert not N % 2
    n = np.arange(N/2+1)
    k = np.arange(N/2+1)


    D = (2./N)*np.cos((2.*np.pi/N)*np.outer(k,n))

    D = D.T
    D[0] = .5*D[0]
    D[-1] = .5*D[-1]
    D = D.T

    return D

def find_d(N):
    """Array d in the integration routine."""

    assert not N % 2
    k = np.arange(0, N/2+1)
    d = 2./(1-(2.*k)**2.)
    d[0] = 1.
    d[-1] = d[-1]/2.

    return d

def find_ker(N, xmin, xmax):
    """Sample points when integration between xmin
       and xmax.
    """

    n = np.arange(N/2+1)
    ker = np.cos(n*np.pi/N)
    ker1 = xmax+.5*(ker-1)*(xmax-xmin)
    ker2 = xmax+.5*(-ker-1)*(xmax-xmin)

    return ker1, ker2
