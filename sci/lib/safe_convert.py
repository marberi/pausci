#!/usr/bin/env python
# encoding: UTF8

# Parse input from the command line into objects.

from __future__ import print_function, unicode_literals
import pdb
import re
import numpy as np

def tobool(val):
    test = val.lower()

    if test == 'true':
        return True
    elif test == 'false':
        return False
    else:
        raise ValueError, 'Could not convert value to bool.'

def tolist(val):
    """Convert value to a list."""

    if ',' in val:
        return map(unicode.strip, val.split(','))
    else:
        stripped = val.strip()
        if not stripped:
            return []
        else:
            return [stripped]


def conv(cmdinput, def_conf):
    """Safely convert from the input."""

    def conv_param(s, def_conf):
        spl = s.split('=')
        assert 1 < len(spl), 'Not a valid key-value pair.'

        key,val = spl[0],'='.join(spl[1:])

        obj_type = type(def_conf[key])
        if obj_type == list:
            return key, tolist(val)
        elif obj_type == bool:
            return key, tobool(val)
        else:
            return key, obj_type(val)

    return [conv_param(x, def_conf) for x in cmdinput]
