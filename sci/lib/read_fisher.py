#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals

# Library for reading in Fisher matrices from files with
# different formats. Used both for loading DETF and 
# BAO from Francisco Castander fisher matrices.
#
# TODO: The code here is slightly clumsy. Weak comments
# and the tests looks misplaced.
import sys
import os
import numpy as np

from numpy.testing import assert_almost_equal


def get_elements(file_name, elem_first=True):
    """Get elements stored in file."""

    data_dir = os.path.join(sys.path[0], '..', 'data')

    if os.path.isfile(file_name):
        file_path = file_name
    else:
        file_path = os.path.join(data_dir, file_name)

    elements = []
    for line in open(file_path):
        tmp = line.strip().split(' ')
        while tmp.count(''):
            tmp.remove('')
  
        if elem_first:
            elements.append(tmp)
        else:
            elements.append([tmp[2]] + tmp[:2])

    return elements

def equal_lists(l1, l2):
    for e1, e2 in zip(l1, l2):
        if not e1 == e2:
            return False

    return True

cor_elems = [['5347758.68138519861', '1', '1'],
             ['39374.2642986917053', '1', '8'],
             ['4430430.61537387781', '2', '2'],
             ['15.0442872667029981', '4', '7']]

def test_get_elements():
    ans_elems = get_elements('test/fisher_matrix')

    assert equal_lists(ans_elems, cor_elems)
    # TODO: Test when the elements are last


def find_n(elements):
    n = 0.

    for val, x, y in elements:
        x = int(x)
        y = int(y)
        n = max(n, x, y)

    return n

def test_find_n():
    elems = [(1, 2, 13), (5, 1, 1), (2, 10, 11)]

    assert find_n(elems) == 13

test_find_n()

def get_data(file_name, elem_first=True, has_lower=False):
    elements = get_elements(file_name, elem_first)
    n = find_n(elements)
    A = np.zeros((n, n))
    test = np.zeros((n, n))

    for val, x, y in elements:
        x = int(x) - 1
        y = int(y) - 1
    
        assert not test[x,y]
        A[x, y] = float(val)
        test[x, y] = 1.
  
    if not has_lower:
        assert not np.sum(np.tril(A,-1))
        A = np.matrix(A + np.triu(A, 1).T)

    return A

def test_get_data():
    file_path = 'data/test/fisher_matrix'
    a = get_data(file_path)

    for val, x, y in cor_elems:
        val = float(val)
        x = int(x) - 1
        y = int(y) - 1

        assert_almost_equal(a[x, y], val)
        assert_almost_equal(a[y, x], val)

        a[x, y] = 0
        a[y, x] = 0

    assert not np.sum(a)
