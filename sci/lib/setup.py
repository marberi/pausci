#!/usr/bin/env python
# encoding: UTF8

import errno
import os
import pdb

import sci

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def setup(cls):
    """Create the cache directories."""

    if not sci.config.general['use_cache']:
        return cls

    cache_dir = sci.config.paths['cachedir']
    for d in sci.config.general['objs']:
        d_path = os.path.join(cache_dir, d)
        mkdir_p(d_path)

    return cls
