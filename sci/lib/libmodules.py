#!/usr/bin/env python
# encoding: UTF8

import pdb
from sci import bias,cosmo,corr,cov,deriv,observ,pop,power,syst

def import_modules(conf, pkg, to_calc):
    """Import all the modules which is needed for the Fisher matrix
       estimation of MCMC runs.
    """

    mod = conf['opts.module']

    par = conf.fid
    opts = conf['opts']

    m = {}
    pop_mod = mod['pop_pkg'] if pkg else mod['pop_thr']
    m['pop'] = getattr(pop, pop_mod)(conf, pkg, to_calc)

    m['bias'] = getattr(bias, mod['bias'])(conf, par)
    m['power'] = getattr(power, mod['power'])(conf)
    m['cosmo'] = getattr(cosmo, mod['cosmo'])(conf, par, m['power'])

    m['observ'] = getattr(observ, mod['observ'])(conf, pkg, m['pop'], m['cosmo'])
    m['corr'] = getattr(corr, mod['corr'])(m['bias'], conf, m['cosmo'], m['pop'], m['observ'])
    m['cov'] = getattr(cov, mod['cov'])(conf)
    m['syst'] = getattr(syst, mod['syst'])(conf, m['pop'])

    m['deriv'] = getattr(deriv, mod['deriv'])\
                 (conf, m['corr'], m['pop'], m['observ'], m['syst'])

#    conf.finalize(m['pop'])

    # These should be reviewed..
    m['corr'].power = m['power'] # HACK for debug...
    m['bias'].cosmo = m['cosmo'] # HACK
    m['bias'].power = m['power'] # HACK
   
    return m




