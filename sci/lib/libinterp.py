#!/usr/bin/env python
# encoding: UTF8

# Library for interpolation between points.
import pdb
import numpy as np
from scipy.interpolate import splev, splrep

from sci import config

def find_spl(par, obs, pre, zkey):
    """Construct a spline for the points to interpolate
       between.
    """

#    zkey = 'bias_z'
    ftype, popid = obs

    zvals = par['pop', popid, zkey]

#    par_pop = par['pop'][popid]
    fstr = config.general['fluct_pre'][ftype]
    nvals = [str(x) for x in range(len(zvals))]
    #bvals = [par_pop['%s%s%sp' % (pre,fstr,str(x))] for x in [1,2,3,4]]
    bvals = [par['pop', popid, '%s%s%s' % (pre,fstr,x)] for x in nvals]
    bvals = np.array(bvals)

#    pdb.set_trace()

    # Overall amplitude variable
    amp_var = "%s%s_amp" % (pre,fstr)
    amp = par['pop', popid, amp_var]

    bvals = amp*bvals

    if len(zvals) == 1:
        zvals = np.linspace(0.,2)
        bvals = bvals[0]*np.ones(len(zvals))

    return splrep(zvals, bvals, k=1)

def use_spl(par, z, obs, pre, zkey):
    """Create and evaluate a spline for the bias."""

    assert isinstance(obs, tuple)

    spl = find_spl(par, obs, pre, zkey)

    ans = splev(z, spl)

    return ans
