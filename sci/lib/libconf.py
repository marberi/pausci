#!usr/bin/env python
# encoding: UTF8

from __future__ import print_function, unicode_literals
import json
import os
import pdb
import sys
import copy
import numpy as np
import itertools as it

from sci import config
from sci.lib import obj_hash

class pardict(object):
    """Dictionary to store the parameters."""

    def _flatten(self, d, pre=()):
        res = {}
        for key, val in d.iteritems():
            newkey = pre+(key,)
            if isinstance(val, dict):
                res.update(self._flatten(val, newkey))
            else:
                res[newkey] = val
#                res['.'.join(newkey)] = val # BACKWARDS COMPATABILITY!!!

        return res

    def __init__(self, data_in):
        self._data = self._flatten(data_in)

    def _conv_key(self, key):
        if isinstance(key, tuple):
            return key
        else:
            return tuple(key.split('.'))

    def __getitem__(self, key):

        key = self._conv_key(key)

        return self._data[key]

    def __setitem__(self, key, val):
        key = self._conv_key(key)

        self._data[key] = val

    def hashid(self):

        return self._data[('hash',)]

    def export(self):
        """Flatten structure used when writing to a file."""

        tuple_keys = [key for key in self._data.keys() if isinstance(key, tuple)]

        return dict([('.'.join(key), self._data[key]) for key in tuple_keys])

    def update(self, E):
        for key,val in E.iteritems():
            new_key = self._conv_key(key)
            self._data[new_key] = val


class conf(object):
    msg_keytype = 'Can not split conf_key'
    msg_unique = 'Myconf keys should be unique.'
    msg_invalid = 'Not a configuration key: {}'

    def hashid(self):
        """Unique representation of the configuration object."""

        treenode = copy.deepcopy(self.data)
        del treenode['info']
        del treenode['myconf']

#        pdb.set_trace()
        return obj_hash.hash_tree(treenode)

    def lock(self):
        """After locking setting attributes through the interface
           will raise an error.
        """

        self.is_locked = True

    def __getitem__(self, conf_key):
        """Fetch one configuration key."""

        assert hasattr(conf_key, 'split')

        if conf_key == 'fid': raise

        treenode = self.data
        for key in conf_key.split('.'):
            assert key in treenode, self.msg_invalid.format(conf_key)
            treenode = treenode[key]

        return treenode

    def adv_setitem(self, key, val):
        """Setting configuration items using wild cards."""

        nodes = key.split('.')
        treenodes = [self.data]
        for node in nodes[:-1]:
            if node == '*':
                treenodes = sum([X.values() for X in treenodes], [])
            else:
                treenodes = [X[node] for X in treenodes]

        for treenode in treenodes:
            treenode[nodes[-1]] = val


    def __setitem__(self, key, val):
#        assert not self.is_locked

        if '*' in key:
            self.adv_setitem(key, val)
            return

        nodes = key.split('.')
        treenode = self.data
        try:
            for node in nodes[:-1]:
                treenode = treenode[node]
            treenode[nodes[-1]] = val
        except KeyError:
            print('Can not set', key, val)

    def hasitem(self, key):
        """Check if one configuration exists."""

        try:
            self[key]
            return True
        except KeyError:
            return False

    def add_myconf(self, myconf):
        """Add configurations to the defaults. Includes an
           option of wildcard adding.
        """

        for key, val in myconf.items():
            self[key] = val


    def copy(self):
        """Create a new and independent instance of the object."""
 
        new_conf = copy.deepcopy(self.data) 

        return self.__class__(new_conf, False)

    def schema(self):
        """JSON schema Tallada-style."""

        global tojson 
        tojson = {str: 'string', int: 'integer', float: 'float',
                  list: 'list', bool: 'bolean', unicode: 'str'}

        def flatten(d, pre=[]):
            """Flatten a dictionary tree with parameters."""

            if isinstance(d, dict):
                res = {}
                for key,val in d.iteritems():
                    res.update(flatten(val,pre+[key]))

                return res
            else:
                return {'.'.join(pre): tojson[type(d)]}

        # Main part of the Schema.
        defconf = copy.deepcopy(self.data)
        del defconf['myconf']
        injson = ["'" + key + "': { 'type': '"+val+"'}" for \
                  key,val in flatten(defconf).iteritems()]

        # Produce a string with the schema.
        txt = """{'title': 'Sci Schema', 'type': 'object', 'properties': {"""
        txt += ',\n'.join(injson)+'}'

        pdb.set_trace()
        return txt

    def adjustments(self):
        """Small adjustments to the configuration."""

        # Small adjustments.
        data = self.data
        deg_sky = 4*np.pi*(180./np.pi)**2
        data['fid']['f_sky'] = data['fid']['area'] / deg_sky
        exp_type = data['opts']['exp_type']
        if not isinstance(exp_type, list):
            data['opts']['exp_type'] = [exp_type]

    def expand_assume(self, data):
        """The assume option is to change parts of the population
           configuration without changing everything. That is in
           particular used to specify n(z) for different depths.
        """

        def satisfy_assume(pop_data, assume):
            """Check if all the conditions are fullfilled."""

            for if_key, if_val in assume['if'].items():
                if not pop_data[if_key] == if_val:
                    return False

            return True

        def update_values(pop_data, assume):
            """In the case where the if condition evaluate to
               True.
            """

            # Works on references.
            for key,val in assume['then'].items():
                pop_data[key] = val

        for pop_name, pop_data in data['fid']['pop'].items():
            for assume_part in pop_data['assume']:
                if satisfy_assume(pop_data, assume_part):
                    update_values(pop_data, assume_part)
                    break


    @property
    def fid(self):
        return pardict(self.data['fid'])

#        def flatten(d, pre=()):
#            res = {}
#            for key, val in d.iteritems():
##                newkey = '{0}.{1}'.format(pre, key) if pre else key
#                newkey = pre+(key,)
#                if isinstance(val, dict):
#                    res.update(flatten(val, newkey))
#                else:
#                    res[newkey] = val
#                    res['.'.join(newkey)] = val # BACKWARDS COMPATABILITY!!!
#
#            return res
#
#        return flatten(self.data['fid'])

    def set_info(self, myinfo):
        """Sets only the info part. Used when loading from
           cache.
        """

        # Since info can be a tree.     
        for key,val in myinfo.items():
            self[key] = val 

    def test_consistency(self):
        """Basic tests if the configuration is consistent."""

        params = self.data['opts']['params']
        assert len(params) == len(set(params)), 'Repeated params'

    def create_data(self, myconf):
        """Create the configuration data structure."""

        data = {}
        data['fid'] = copy.deepcopy(config.cosmo)
        data['opts'] = copy.deepcopy(config.opts)
        data['info'] = copy.deepcopy(config.info)

        # Joining in the populations data.
        if 'opts.pop' in myconf:
            data['opts']['pop'] = myconf['opts.pop']

        # HACK. 
        popdata = {}
        for pop_name in ['faint', 'bright', 'gold']:  #data['opts']['pop']:
            popdata[pop_name] = copy.deepcopy(getattr(config.pop, pop_name))

        data['fid']['pop'] = popdata

        # Joining in the survey data.
        if 'opts.survey' in myconf:
            data['opts']['survey'] = myconf['opts.survey']

        # To upgrade the popolation values before using the assume
        # facility.
        data['myconf'] = copy.deepcopy(myconf)
        self.expand_assume(data)

        data['fid'].update(copy.deepcopy(getattr(config.survey, data['opts']['survey'])))

        return data


    def __init__(self, myconf={}, add_defaults=True):

        """If you see some paranoid copying here, you can 
           understand which problems I had.
        """

        self.is_locked = False
        myconf = copy.deepcopy(myconf)
        keys = myconf.keys()
        
        self.data = self.create_data(myconf)
        self.add_myconf(myconf)
        self.adjustments()

        self.data['fid']['hash'] = 'none' # HACK

        self.test_consistency()
#        self.lock()
