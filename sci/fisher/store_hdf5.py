#!/usr/bin/env python
# encoding: UTF8

# Store the Fisher matrices in a HDF5 file.

import os
import pdb
import sys
import tables

import numpy as np
from sci import config

from sci.lib.libh5attr import h5getattr, h5setattr

def load(file_path):
    """Load fisher matrix from HDF5 format."""

    fb = tables.openFile(file_path)

    meta = fb.getNode('/fisher/meta')
    conf_data = h5getattr(meta, 'conf')

    def fromkey(key):
        return tuple(key.split('.')) if '.' in key else key

    data = {}
    for name, F in fb.getNode('/fisher')._v_children.iteritems():
        if name == 'meta': continue

        b1 = fromkey(h5getattr(F, 'b1'))
        b2 = fromkey(h5getattr(F, 'b2'))

        data[b1, b2] = F.read()

    fb.close()

    return conf_data, data

class store_hdf5(object):
    def save(self, file_path=''):
        """Save fisher matrix in HDF5 format."""

        # Later this should fail when a path is not passed. The external
        # frameworks are responsible for locating the files.
        if not file_path:
            file_name = self.conf.hashid()
            file_path = os.path.join(config.paths['cachedir'], 'fisher' ,file_name)

        fb = tables.openFile(file_path, 'w')
        fb.createGroup('/', 'fisher')

        meta = fb.createArray('/fisher', 'meta', np.zeros(1))
        h5setattr(meta, 'conf', self.conf.data)

        def tokey(key):
            return '.'.join(key) if isinstance(key, tuple) else key

        for i,item in enumerate(self._data.iteritems()):
            (b1,b2), F = item

            Fobj = fb.createArray('/fisher', 'fisher'+str(i), F)
            h5setattr(Fobj, 'b1', tokey(b1))
            h5setattr(Fobj, 'b2', tokey(b2))

        fb.close()
