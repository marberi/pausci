#!/usr/bin/env python
from __future__ import print_function, unicode_literals

## String handeling. ##
def nr_str(n):
    """Convert integer to text string."""

    nr_dict = {1: 'one', 2: 'two'}

    if n in nr_dict:
        return nr_dict[n].capitalize()

def to_plural(noun, n):
    """Add plural s to nouns."""

    if n > 1:
        return "%ss" % noun
    else:
        return noun

def to_str(noun, n):
    """Construct e.g. Two populations."""

    nr = nr_str(n)
    text = to_plural(noun, n)
    return "%s %s" % (nr, text)
