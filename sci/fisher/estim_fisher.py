#!/usr/bin/env python
# encoding: UTF8

import ipdb
import itertools as it
import sys
import time
import numpy as np
from numpy.linalg import pinv
from scipy.linalg import cho_factor, cho_solve

import sci
from sci.fisher.fisher_cosmo import fisher_cosmo as fisher_standard

class estim_fisher(object):
    """Fisher matrix forecast for find the uncertainties
       in parameters. Used to quickly forecast how well
       cosmological surveys can measure quanities like
       the equation of state for dark energy.
    """

    def calc_values(self, fid, diffs, to_calc=['all']):

        self.cosmo.gen_cache(diffs, to_calc, self.power)
        self.corr.lens.gen_cache(diffs, to_calc)

    def _calc_fpart(self, deriv, cov_mat):

        inv_type = self.conf['opts.cov_inv']
        assert inv_type == 'chol', 'Only invert using the cholsky method..'

        # Here I could consider optimizing.
        keys = deriv.keys()
        F = {}
        for i,key1 in enumerate(keys):
            x = cho_solve(cho_factor(cov_mat), deriv[key1].T) 
            for j,key2 in enumerate(keys):
                # Here key2 is the first index.
                F[key2,key1] = np.dot(deriv[key2], x)

        return F

    def _fpart(self, fid, diffs):
        """Contribution to the fisher matrix from l=l."""


        deriv_iter = self.deriv.deriv_iter(diffs)
        for u,(corrD,deriv) in zip(self.u_mean, deriv_iter):
            observD = self.observS.observD(u)
            nl = self.cosmo.N(u)
            cov_mat = self.cov(fid, corrD['corr_obs'], observD, nl)

            # In case no observables are selected.
            if not len(cov_mat):
                raise NotImplementedError('Should return a dictionary..')
                yield 0 #return 0

            f_part = self._calc_fpart(deriv, cov_mat)

            # Sometimes we scale with area since calculating many observables
            # take time. TODO: This scaling could as well be folded into the
            # covariance... 
#            if self.conf['opts.scale_area']:
#                r = fid['area'] / self.conf['opts.scale_to']

#                for key in f_part.keys():
#                    f_part[key] *= r

    #        pdb.set_trace()

            msg_sym = 'A fisher matrix should be symmetric'
            msg_neg = 'Each f_part should have positive values on the diagonal.'

            for (key1,key2),F in f_part.iteritems():
                if key1 == key2:
                    assert (0 <= np.diag(F)).all(), msg_neg

            yield f_part

    def _calc_fisher(self, fid):
        """The fisher matrix for the cross-correlation measurements."""

        verbose = self.conf['opts.verbose']

        ndim = len(self.conf['opts.params'])
        diffs = self.deriv.fid_diffs(fid)
        self.calc_values(self.conf.fid, diffs)

        # Using a totally new name. Reusing observD would only lead to hard to
        # detect errors. 
        observS = self.observ._find_partypack(fid)
        self.u_mean, _ = observS.ubins()

        # Not exactly elegant exposing this interface twice..
        self.observS = observS
        self.deriv.observS = observS

        name = 'l' if self.pop['meta']['fourier'] else 'theta'

        fpart_iter = self._fpart(fid, diffs)
        f_parts = []
        for u,fpart in it.izip(self.u_mean, fpart_iter):
            print('u', u)
            if verbose:
                print('%s = %s' % (name, str(u))) #z

            f_parts.append(fpart)

        # Sums over angles/l-values.
        fisher = {}
        for key in f_parts[0].keys():
            fisher[key] = sum([F_u[key] for F_u in f_parts])

        return fisher


    def fisher_mat(self):
        self.test_config()

        if self.conf['opts.mes_trans']:
            raise NotImplementedError('We have to fix this...')

        data = self._calc_fisher(self.conf.fid)

        # One need the redshift when plotting error on bias versus redshift.
        for popid in self.pop['meta']['pop']:
            key = 'meta', ('z', popid)
            data[key] = self.pop[popid]['z_mean']

        F_obj = fisher_standard(self.conf, data)
        F_obj.save()

        return F_obj
