#!/usr/bin/env python
# encoding: UTF8

import pdb

import numpy as np

class Finslices:
    """Fisher matrix using slices."""

    # The reason for using it here and not elsewhere is this
    # feature is considered experimental.

    def __init__(self, F, slices):
        self.F = F
        self.slices = slices

    def _create_slices(self, data):
        """Create slice objects for easier accessing different parts of
           the large array.
        """

        all_keys = [key[0] for key in data.keys()]
        keys = list(set(filter(lambda key: key != 'meta', all_keys)))

#        pdb.set_trace()

        nelemsD = dict((key[0],F_part.shape[0]) for key,F_part in data.items())
        inds = [nelemsD[x] for x in keys]

        nelem = np.sum(inds)

        edges = np.hstack([[0], np.cumsum(inds)])

        fslices = dict(((key,slice(start,stop,None)) for key,start,stop in \
                   zip(keys, edges[:-1], edges[1:])))

        return keys, nelem, fslices

    def _combine_parts(self, data):
        """Combine all the different parts of a Fisher matrix to a
           combined one.
        """

        keys, nelem, fslices = self._create_slices(data)

#        pdb.set_trace()
        F = np.zeros((nelem, nelem))

        for key1 in keys:
            s1 = fslices[key1]
            for key2 in keys:
                s2 = fslices[key2]

                F[s1, s2] = data[key1, key2]

        return F, fslices

    def to_data(self):
        res = {}
        for name1, s1 in self.slices.iteritems():
            for name2, s2 in self.slices.iteritems():
                res[name1, name2] = self.F[s1,s2]

        return res

    def select(self, inds):
        """Construct a new matrix only using selected slices."""

        missing_inds = set(inds) - set(self.slices.keys())
        assert not missing_inds

        data = self.to_data()

        for ind1,ind2 in data.keys():
            if not (ind1 in inds and ind2 in inds):
                del data[ind1,ind2]

        new_F, new_slices = self._combine_parts(data)    

        return Finslices(new_F, new_slices)

    def cov_part(self, key):
        s = self.slices[key]
        cov = np.linalg.inv(self.F)

        return cov[s,s]


class fisher_block:
    """Code for showing the finding the error on the blocks."""

    def _use_inds(self, block_type, ftype, pop):
        bkey = 'bias', ftype, pop
        # Instead of selecting which elements to use, we
        # remove sections. This allows for adding more
        # blocks of parameters later.

        inds = []
        marg_bias = self.conf['info.marg_{0}'.format(block_type)]
        marg_cosmo = self.conf['info.marg_cosmo']

        for ind in self._fslices.keys():
            if ind == 'cosmo' and not marg_cosmo:
                continue
            elif ('stoch' in ind and not marg_stoch) and \
                not ind == bkey:
                continue
            elif ('bias' in ind and not marg_bias) and \
                  not ind == bkey:
                continue

            inds.append(ind)

        return bkey, inds

    def err_block(self, block_type, ftype, pop):
        """Error on the bias parameters."""


        F = self.F

        bkey, inds = self._use_inds(block_type, ftype, pop)
        zkey = 'z.{0}'.format(pop)

        A = Finslices(self.F, self._fslices)
#        pdb.set_trace()
        B = A.select(inds)
        cov = B.cov_part(bkey)

        err = np.sqrt(np.diag(cov))
        z_all = self._data['meta', ('z', pop)]

#        pdb.set_trace()
        abs_err = np.sqrt(np.diag(cov))

        return z_all, abs_err
