#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function, unicode_literals
import sys
import copy
import itertools as it
import os
import pickle
import ipdb
import numpy as np
import scipy

import sci
from sci.fisher import store_hdf5

class F_invert(Exception, object): pass

class fisher_basic(store_hdf5.store_hdf5, object):
    """Fisher matrix object."""

    msg_square = 'data should be a square matrix.'
    msg_params = 'Inconsitent dimension of data and parameters.'
    msg_sym = 'A fisher matrix should be symmetrix.'
    msg_neg = 'The diagonal on a fisher matrix should be positive'
    msg_invert = 'Problems with inversion'
    msg_multi_define = 'Either use to_keep or to_rem, not both.'

    # Used to distinguish between Fisher matrices and chains.
    has_trace = False

    def __init__(self, conf=None, data={}, _fisher=None): #, add_priors=True):
        # So the user can pass myconf dictionaries.
        if not hasattr(conf, 'lock'):
            conf = sci.libconf(conf)

        conf.lock()
        self.conf = conf
        self._data = data

        assert isinstance(data, dict)

        if _fisher == None:
            self._fslices, F = self._combine_parts(data)
        else:
            assert len(self.conf['opts.params']) == _fisher.shape[0]

            self._fslices = {'cosmo': slice(0, _fisher.shape[0], None)}
            F = _fisher

        # Prevents recursively calling priors.mypriors. Testing for None
        # prevents from adding the priors multiple times e.g. when
        # copying.
        if self.conf['info.priors'] and isinstance(_fisher, type(None)):
            F = self._add_priors(F)

        _fisher = self._marginalize(F)

        ni, nj = np.shape(_fisher) 
        assert ni == nj, self.msg_square
        assert np.shape(_fisher)[0] == len(conf['opts.params']), self.msg_params
#        assert np.sum(np.abs(_fisher - _fisher.T)) < 1, msg_sym
        assert (0 <= np.diag(_fisher)).all(), self.msg_neg

        self.F = F
        self._fisher = _fisher

    def _create_slices(self, data):
        """Create slice objects for easier accessing different parts of
           the large array.
        """

        all_keys = [key[0] for key in data.keys()]
        keys = list(set(filter(lambda key: key != 'meta', all_keys)))

#        nelemsD = dict((key[0],F_part.shape[0]) for key,F_part in data.items())
#        inds = [nelemsD[x] for x in keys]

        if not self.conf['info.marg_bias']:
            keys = filter(lambda x: not 'bias' in x, keys)

        inds = [data[key,key].shape[0] for key in keys]

        nelem = np.sum(inds)

        edges = np.hstack([[0], np.cumsum(inds)])

        fslices = dict(((key,slice(start,stop,None)) for key,start,stop in \
                   zip(keys, edges[:-1], edges[1:])))

        return keys, nelem, fslices

    def _combine_parts(self, data):
        """Combine all the different parts of a Fisher matrix to a
           combined one.
        """

        keys, nelem, fslices = self._create_slices(data)

        F = np.zeros((nelem, nelem))

        for key1 in keys:
            s1 = fslices[key1]
            for key2 in keys:
                s2 = fslices[key2]

                F[s1, s2] = data[key1, key2]
            
        return fslices, F

    def _add_priors(self, fisher_in):
        """Add priors to fisher matrix."""

        fisher = fisher_in.copy()
        if 'cosmo' in self._fslices:
            mypriors = sci.priors.mypriors()
            pr_list = self.conf['info.priors']

            # At this point we know pr_list includes at least one
            # element.
            pr = [mypriors[x] for x in pr_list]
            F_priors = sum(pr[1:], pr[0])
            fisher_pr = F_priors.select(self.conf['opts.params'])

            scosmo = self._fslices['cosmo']
            fisher[scosmo, scosmo] += fisher_pr

        for key in self._fslices.keys():
            if 'key' == 'cosmo':
                continue

            block_type = key[0]
            if key[0] in ['bias', 'stoch']:
                fisher = sci.priors.block_priors(self.conf, self._data, fisher, self._fslices, key)

        assert not self.conf['opts.mes_trans'], 'Need to add back this functionality.'

        return fisher

    def _marginalize(self, F_in):
        """Remove everything else than the cosmology parameters."""

        if not set(self._fslices.keys()) - set(['cosmo']):
            return F_in

        if 'cosmo' in self._fslices:
            # Here pinv is used since some bias parameters might not be measured.
            scosmo = self._fslices['cosmo']
            cov = scipy.linalg.pinv(F_in)
            F = scipy.linalg.inv(cov[scosmo, scosmo])
        else:
            F = np.array([])

        return F

    def get_cov(self):
        """Return the covariance matrix."""

        if self.conf['opts.exp_type'] == ['priors']:
            raise F_other("Did you really want to invert the priors?")

#        ipdb.set_trace()

        try:
            cov = np.linalg.inv(self._fisher)
            cov_pinv = np.linalg.pinv(self._fisher)
        except np.linalg.linalg.LinAlgError:
            raise F_invert

        # Extra tests of invertibility.
        #inv_test = np.max(np.abs((cov-cov_pinv)/cov))
        inv_test = 0. # HACK
        if not inv_test < 10**-7:
            raise F_invert(self.msg_invert)

        if min(np.diag(cov)) < 0:
            raise F_invert('Negative values on the diagonal')

        #return cov
        return cov_pinv

    cov = property(get_cov)

    def select(self, params):
        """Select the Fisher matrix for a set of parameters. Add rows
           and columns of zero when not included."""

        all_params = self.conf['opts.params']
        use_params = filter(lambda x: x in all_params, params)

        from_inds = map(all_params.index, use_params)
        to_inds = map(params.index, use_params)
        F = np.zeros(2*(len(params),))
        F[np.ix_(to_inds, to_inds)] = self._fisher[np.ix_(from_inds, from_inds)]

        return F

    def remove_zeros(self):
        """A fisher matrix can include rows with zeros in the case
           the probe is insensitive to those parameters.
        """

        to_keep = self.conf['opts.params']

        return self._rem_marg(to_keep, [], False, True)


    def error(self, param):
        """Percentage error in parameter."""

        cov = self.cov

        fiducial = self.fid[param]
        index = self.indxs()[param]

        ans = np.sqrt(cov[index, index])

        return (ans/fiducial)*100.

    def err_abs(self, param):
        """Percentage error in parameter."""

        cov = self.cov

        fiducial = self.fid[param]
        index = self.indxs()[param]

        return np.sqrt(cov[index, index])

    def errors(self, params): #, keys):
        """Percentage error in parameter."""

        # The translation inside here is UGLY. I have a plan of moving
        # to a different style of parameter names. When starting out
        # having hirarchical parameter names seemed like overkill. Now
        # the infrastructure is in place.
        
        # Updata: It support these parameter names, what remains is starting
        #         to use them.

        import re
        def trans_name(par):
            """Translate the name to one which can be used for looking
               up parameters.
            """
 
            isbias = par[:2] == 'xb' or (par[0] == 'b') and (par[2] == 'p')
            if not isbias:
                return 'fid.{0}'.format(par)

            # Almost legendary ugly.
            pop = self.conf['opts.pop']
            has_x = par[0] == 'x'
            last_part = re.sub('^x', '', par[:-1])
            name = 'fid.pop.{0}.{0}'.format(pop[1*has_x+int(par[-1])-1], last_part)

            return name


        all_params = self.conf['opts.params']
        indxs = map(all_params.index, params)

        abs_err = np.sqrt(np.diag(self.cov)[indxs])
        keys = map(trans_name, params)
        fiducial = [self.conf[x] for x in keys]

        txt = [(('%.3f%%' % np.abs(100*(x/y))) if y else '{:.3f}'.format(x)) \
               for x,y in zip(abs_err, fiducial)]

        return keys, txt

    def _rem_marg(self, to_keep, to_rem, marg_params, rem_zeros):
        """Basic function doing removal and marginalization. Either
           specify to_keep or to_rem. rem_zeros remove entries not measured
           even when included in to_kee."""

        assert not (to_keep and to_rem), self.msg_multi_define

        params = self.conf['opts.params'] 
        data = self._fisher.copy()

        # In the first stage select select which parameters to use.
        # The code here is just boring logic.

        is_zero = np.array(params)[np.diag(data) == 0.].tolist() if \
                  rem_zeros else []

        start_params = to_keep if to_keep else params

        use_params = [x for x in start_params if not \
                      (x in to_rem or x in is_zero)]

        # Creating a new object with only use_params. Uses infinite
        # priors when inverting because its easier.
        inds = map(params.index, use_params)
        if not marg_params:
            new_fisher = data[np.ix_(inds, inds)]
        else:
            inds_zero = map(params.index, is_zero)
            data[inds_zero, inds_zero] = 1e200
            F = np.linalg.inv(data)
            new_fisher = np.linalg.inv(F[np.ix_(inds, inds)])

        new_conf = self.conf.copy()
        new_conf['opts.params'] = use_params

        return self.__class__(new_conf, self._data, new_fisher)

    def remove(self, to_remove=[], to_keep=[], test_zero=False):
        # TODO: Change the call signature of this function...
        X2 = self._rem_marg(to_keep, to_remove, False, False)

        return X2

    def marg(self, to_change=[], to_keep=[]):
        """Marginalize the fisher matrix over the parameters
           to_change.
        """

        # TODO: Change the call signature of this function...
        X2 = self._rem_marg(to_keep, to_change, True, False)

        return X2

    def add_extended(self, F2):
        def find_params(): 
            params_lst = self.conf['opts.params'] + F2.conf['opts.params']
            params = []
            for x in params_lst:
                if not x in params:
                    params.append(x)

            return params

        # Combining Fisher matrices from the two objects.
        params = find_params()
        new_fisher = np.zeros(2*(len(params),))
        inds1 = map(params.index, self.conf['opts.params'])
        inds2 = map(params.index, F2.conf['opts.params']) 

        new_data = {}
        new_cosmo = np.zeros(2*(len(params),))
        cosmo_key = ('cosmo', 'cosmo')
        new_cosmo[np.ix_(inds1, inds1)] = self._data[cosmo_key]
        new_cosmo[np.ix_(inds2, inds2)] += F2._data[cosmo_key]
        new_data[cosmo_key] = new_cosmo
 
        # TODO: Move this code out in blocks...


        # For other blocks we assume they have the same size.
        data_keys = set(self._data.keys()).union(set(F2._data.keys()))
#        pdb.set_trace()
        for pair in data_keys:
            key1, key2 = pair
            from_data = self._data if pair in self._data else F2._data

            if key1 == 'cosmo' and key2 == 'cosmo':
                continue

            if key1 == 'meta':
                # It should be equal. If this is different then there is 
                # something very wrong.
                if pair in self._data and pair in F2._data:
                    assert (self._data[pair] == F2._data[pair]).all()

                new_data[pair] = from_data[pair]
                continue

            # HACK:
            if key1=='cosmo' and 'bias.' in key2:
#'bias.' in key1 or 'bias.' in key2:
                pdb.set_trace()

            A = []
            if pair in self._data:
                A.append(self._data[pair])
            if pair in F2._data:
                A.append(F2._data[pair])

            new_data[pair] = sum(A)


        new_conf = self.conf.copy()
        exp_type = self.conf['opts.exp_type'] + F2.conf['opts.exp_type']
        exp_type = sorted(set(exp_type))

        # Since later analysis might depend on the order.
        order = ['faint', 'bright'] # HACK
        pop = self.conf['opts.pop'] + F2.conf['opts.pop']
        pop = sorted(set(pop))
        pop.sort(cmp=lambda x,y: order.index(x) - order.index(y))

        new_conf['opts.exp_type'] = exp_type
        new_conf['opts.params'] = params
        new_conf['opts.pop'] = pop

        # Some extre work since not all terms are there when combining
        # over different skies.

        # Set of unique keys without 'meta' keys.
        keys = set(sum(filter(lambda x: not x[0] == 'meta', 
                              new_data.keys()),()))

        for key1 in keys:
            for key2 in keys:
                if (key1,key2) in new_data:
                    continue

                n1 = new_data[key1,key1].shape[0]
                n2 = new_data[key2,key2].shape[0]

                new_data[key1, key2] = np.zeros((n1,n2))

        return self.__class__(new_conf, new_data)

    __add__ = add_extended

    def copy(self):
        """Return return a copy of this fisher matrix object."""

        conf = self.conf.copy()
        new_fisher = self._fisher.copy() 

        return self.__class__(conf, self._data, new_fisher)

    def __repr__(self):
        # The Brownthrower framework require this.

        return 'fisher matrix'
