#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function, unicode_literals
import ipdb
import numpy as np

import sci
from sci import config
from sci.fisher import fisher_basic
import fisher_block

np.seterr(over='raise', divide='raise', invalid='raise', under='raise')
class F_invert(Exception, object): pass

class fisher_cosmo(fisher_basic.fisher_basic, fisher_block.fisher_block):
    def bias_params(self, only_evolution=False):
        """Parameters which are bias. Used when fixing the bias."""

        vectors = ['bias', 'dbdz', 'ddbdz', 
                   'xbias', 'xdbdz', 'xddbdz',
                   'b1p', 'b2p', 'b3p', 'b4p',
                   'xb1p', 'xb2p', 'xb3p', 'xb4p',
                   'm1p', 'm2p', 'm3p', 'm4p',
                   'xm1p', 'xm2p', 'xm3p', 'xm4p']


        res = []
        for param in self.conf['opts.params']:
            for vec in vectors:
                if param.startswith(vec):
                    res.append(param)

        return res

    def title(self):
        """Return a title for this fisher matrix."""

        raise "Is this in use???"

        exp = self.config['opts.exp_type']
        if not isinstance(exp, list): #HACK
            exp = [exp]

        exp = '+'.join(exp).upper()
        npop = len(self.conf['opts.pop'])

        pop_str = lang.to_str('population', npop)
        title = "%s - %s" % (exp, pop_str)
        if hasattr(self, 'add_title'):
            return "%s (%s)" % (title, self.add_title)
        else:
            return title

    def dxdyp(self, paramx, paramy):
        """Parameters for plotting contours."""

        assert paramx != paramy
        params = self.conf['opts.params']
        i = params.index(paramx)
        j = params.index(paramy)

        cov = np.linalg.pinv(self._fisher, rcond=1.0000000000000001e-80)


        dx = np.sqrt(cov[i,i])
        dy = np.sqrt(cov[j,j])
        dxy = cov[i,j]
        p = dxy / (dx*dy)

        if not (-1 <= p and p <= 1):
            msg_noninv = 'The matrix is not invertible..'

            raise Exception(msg_noninv)


        sin2ang = 2*dxy
        cos2ang = dx**2 - dy**2
        x = cos2ang
        y = sin2ang
                
        theta = np.arctan(y/x)
        # <copied code>
        theta = np.where(np.less(x, 0), theta + np.pi, theta)
        theta = np.where(np.logical_and(np.greater(x, 0), np.less(y, 0)), theta + \
                         2*np.pi, theta)
        # </copied code>

        # This line should be after the others. It is 2*theta which is the angle
        # being fixed.
        theta = theta/2.

        return dx, dy, p, theta

    def area(self, paramx, paramy):
        """Area of the contour of paramx and paramy."""

        dx, dy, p, theta = self.dxdyp(paramx, paramy)

        # arxiv: 0906.4123
        return 6.17*np.pi*dx*dy*np.sqrt(1 - p**2)

    def description(self):
        """Text string describing the Fisher matrix. A set of tags is
           used...
        """

        if not 'tags' in self.conf['info']: return None
        info_tags = self.conf['info']['tags']
        info_tags = [info_tags] if not isinstance(info_tags, list) else \
                    info_tags
#        val = self.conf[info_tag]
#        ans = "%s = %s" % (info_tag, val)
        ans = []
        for info_tag in info_tags: 
            try:
                ans.append("%s = %s" % (info_tag, self.conf[info_tag]))
            except KeyError:
                pass

        ans = '- '.join(ans) if ans else 'No avail'

        return ans

    def acronym(self):
        """Short name to describe the probes, area and if they are
           over the same part of the sky.
        """

        # These is no way the calculation framework should have this information.
        # This is a leftover from previous stages.
        pop = self.conf['opts.pop']
        exp_type = self.conf['opts.exp_type']
        same_field = self.conf['opts.same_field']

        # Map from population name to a simple letter.
        pop_letter = dict((y,x) for (x,y) in config.general['pop_name'].items())

        op = 'x' if same_field else '+'
        part1 = op.join(pop_letter[x].upper() for x in pop)
        part2 = '%'.join(exp_type)

        acro = '{0}-{1}'.format(part1, part2)

        return acro

    def info(self):
        """Dictionary with information which is used when outputting 
           the tables.
        """

        to_deg = lambda f_sky: int(41252.962*f_sky)

        res = {}
        res['survey'] = self.conf['opts.survey']
        res['exp_type'] = self.conf['opts.exp_type']
        res['area'] = to_deg(self.conf['fid.f_sky'])
        res['acronym'] = self.acronym()
        res['descr'] = self.description()

        res['probe'] = '+'.join(set(self.conf['opts.exp_type'])).upper()

        # Describes what photoz-transitions which are used.
        # TODO: Use a separate function.
        with_trans = 'trans' in self.conf['opts.syst']
#        use_data = (self.conf['opts.module.pop'] == 'pop_cat')
        use_data = self.conf['opts.pkg'] != ''
        if with_trans and use_data:
            res['trans_text'] = 'sim'
        elif with_trans and not use_data:
            res['trans_text'] = 'ana'
        else:
            res['trans_text'] = 'no'

        return res

    def fom(self, strict=False):
        """Ordinary figure of merit."""

        msg_params = 'w0 and wa is needed to calculate the FoM'
        assert set(['w0', 'wa']).issubset(set(self.conf['opts']['params'])),\
               msg_params

        to_keep = ['w0', 'wa']
        if self.conf['info.marg_cosmo']:
            F_fom = self.marg(to_keep=to_keep)
        else:
            F_fom = self.remove(to_keep=to_keep)

        cov = F_fom.cov
        ans = 1./np.sqrt(np.linalg.det(cov))

        return ans

    def fom_detf(self, strict=False):
        """Dark energy task force figure of merit."""

        if 'gamma' in self.conf['opts.params']:
            fom = self.remove(['gamma']).fom()
        else:
            fom = self.fom()

        return fom

    def fom_abs_err(self, param, excl_params=[]):
        """Gamma figure of merit. Introduced by the bcn group."""

        msg_param = '%s is not one of the parameters' % param
        assert param in self.conf['opts.params'], msg_param

        if self.conf['info.marg_cosmo']:
            to_keep = list(set(self.conf['opts.params']) - set(excl_params))
        else:
            to_keep = [param]

        F_gamma = self.remove(to_keep=to_keep)

        ind = F_gamma.conf['opts.params'].index(param)
        abs_error_gamma = np.sqrt(F_gamma.cov[ind, ind])

        return abs_error_gamma

    def fom_g(self):
        """Error in gamma when keeping many parameters free."""

        return 1./self.fom_abs_err('gamma', ['w0', 'wa'])

    def fom_ys(self):
        """Figure of merit to match what Y-S is sending."""

        return 1./self.fom_abs_err('w0')**2.

    def fom_fnl(self):
        """Absolute error in gamma."""
    
        return self.fom_abs_err('fnl')

    def fom_ga(self):
        """Absolute error in ga."""
    
        return self.fom_abs_err('ga')



    def fom_c(self):
        """Combined figure of merit. Introduced by the bcn group."""

        min_params = set(['w0', 'wa', 'gamma'])
        text = 'The fisher matrix should at least contain: %s' % min_params
        assert set(self.conf['opts.params']).issuperset(min_params), text

        to_keep = ['w0', 'wa', 'gamma']
        if self.conf['info.marg_cosmo']:
            F_c = self.marg(to_keep=to_keep)
        else:
            F_c = self.remove(to_keep=to_keep)

#        ipdb.set_trace()

        fom_c = 1./np.sqrt(np.linalg.det(F_c.cov))

        return fom_c

    def foms(self, fom_types, prior_list, mypriors='', suf=''):
        """Add several different priors and then calculate different
           figures of merits(FoMs). This is the most usual way of calculating
           the FoMs.
        """

#        F = self.add_priors(prior_list, mypriors)
        F = self.remove_zeros()

        foms = {}
        for fom_type in fom_types:
            try:
                fom = getattr(F, fom_type)()
            except np.linalg.linalg.LinAlgError:
                fom = 'na'
            except F_invert:
                fom = 'na'
            except AssertionError:
                fom = 'na'

           # if fom == 'na':
           #     import pdb; pdb.set_trace()
            if suf:
                name = "%s_%s" % (fom_type, suf)
            else:
                name = fom_type

            foms[name] = fom


        return foms

    def fix_bias(self, only_evolution=False):
        """Fix bias parameters."""

        raise NotImplementedError('Deprecated feature')
        toremove = self.bias_params(only_evolution)
        F_fixed = self.remove(toremove) if toremove \
                  else self.copy()

        F_fixed.priors = self.priors

        return F_fixed

        # Add infinite priors to endtries close to zero.
        inds_sub = 10. < np.diag(F_all[:n1,:n1])
        z = z_all[inds_sub]
        nz = len(z)

        inds = np.ones(F_all.shape[0], dtype=np.bool)
        inds[:n1] = inds_sub

        F = F_all[np.ix_(inds, inds)]

        cov = np.linalg.inv(F)[:nz, :nz]
        abs_err = np.sqrt(np.diag(cov))

        print('abs_err', abs_err)

        return z, abs_err

    def rename_bias(self):
        """Rename the bias parameters. Used when two parts of
           the sky has different bias.
        """

        params = self.conf['opts.params']
     
        def invec(param):
            for vec in ['bias', 'dbdz', 'ddbdz', 
                        'b1p', 'b2p', 'b3p', 'b4p']:
                if param.startswith(vec):
                    return True

            return False
           
        new_params = []
        for param in params:
            if invec(param):
#                pdb.set_trace()
                new_params.append('x'+param)
            else:
                new_params.append(param)

        new_fisher = self._fisher.copy()
        new_conf = self.conf.copy()
        new_conf['opts.params'] = new_params

        return self.__class__(new_conf, self._data)

    def degen(self, fom_type):
        """Find the degeneracies between parameters."""

        # Ok, I admit this is a bit risky.
        backup_fisher = self._fisher.copy()
        fom_func = getattr(self, fom_type)

        fisher_tmp = self._fisher
        d = np.diag(fisher_tmp)
        nparams = len(d)
        maxentry = 10000*np.max(d)

        fom_orig = fom_func()
        foms = np.zeros(nparams)
        for i in range(nparams):
            np.fill_diagonal(fisher_tmp, d)
            
            fisher_tmp[i,i] = maxentry
            self._fisher = fisher_tmp

            foms[i] = fom_func()

        self._fisher = backup_fisher

        foms[foms/fom_orig > 100] = np.nan

        return fom_orig, foms
