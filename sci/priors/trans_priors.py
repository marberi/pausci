#!/usr/bin/env python
# encoding: UTF8

# Photo-z priors on transitions between redhshift
# bins.
import pdb
import time
import itertools as it
import numpy as np
import scipy
 
class trans_priors(object):
    def static_priors(self, popid):
        """Static priors on photoz transitions."""
    
        ncross = self.pop[popid]['nbins']**2.
        err_trans = self.conf['fid.pop.%s.pr_r' % popid]
        pr_trans_pop = (1./err_trans**2) * np.ones(ncross) if \
                        err_trans else np.zeros(ncross)
        
        return np.diag(pr_trans_pop)

    def _spl_pairs(self, pairs):
        i, j = zip(*pairs)
        i = np.array(i)
        j = np.array(j)
        
        return i,j

    def outlier_priors(self, popid):
        """The priors on the outliers."""

#        err_T = self.conf['fid.pop.%s.err_T' % popid] 
        err_t = self.pop[popid]['err_t']
        nbins = self.pop[popid]['nbins']

        # Not completely correct. The ngal_act is not correctly
        # set.
        ngal_obs = self.pop[popid]['ngal_obs']
        compl = self.pop[popid]['compl']
        ngal_act = ngal_obs / compl

        pairs = list(it.product(range(nbins), range(nbins)))
        i,j = self._spl_pairs(pairs)

        p = (ngal_obs[i]/ngal_act[j])**2 * err_t**-2

        pr_R = np.diag(p)

        return pr_R

    def core_outlier(self, popid):
        """Combine priors from photo-z core and the outliers."""

        pr_trans_pz = self.pop[popid]['photoz_F']
        inds_pz = self.pop[popid]['r_elem']

        pr_trans = self.outlier_priors(popid)
        pr_trans[np.ix_(inds_pz, inds_pz)] = pr_trans_pz
        inds = range(pr_trans.shape[0])

        return pr_trans, inds

    def all_indxs_priors(self): 
        """This loop does two things. It construct the priors on
           photoz and also figure out which transitions which is
           happening.
        """
        
        nparams = len(self.conf['opts.params'])
        
        all_pr = [np.zeros((nparams,nparams))]
        all_indxs = [range(nparams)]
        for popid in self.pop['meta']['pop']:
            ncross = self.pop[popid]['nbins']**2
    
            pr_option = self.conf['fid.pop.%s.pr_trans' % popid]
            if not pr_option or pr_option == 'none':
                pr_trans = np.zeros((ncross, ncross))
                all_indxs.append(range(ncross))
            elif pr_option == 'static':
                pr_trans = self.static_priors(popid)
                all_indxs.append(range(ncross))
            elif pr_option == 'photoz':
                pr_trans = self.pop[popid]['photoz_F']
                all_indxs.append(self.pop[popid]['r_elem'])
            elif pr_option == 'core':
                pr_trans, inds = self.core_outlier(popid)
                all_indxs.append(inds)
            else:
                print('pr_option', pr_option) #z
                raise

#            pdb.set_trace()

            # Assumes priors are on r..
            if self.conf['opts.rev_trans']:
                J = self.pop[popid]['jacb_r_s']
                pr_trans = J.T*pr_trans*J

            all_pr.append(pr_trans)

        return all_pr, all_indxs

    def trans_priors(self):
        """Combine the results from all_indxs_priors."""

        all_pr, all_indxs = self.all_indxs_priors()

        # Which indices in the full fisher matrix that we should
        # use.
        s, use_inds = 0, []
        for elem in all_indxs:
            use_inds.extend(list(s + np.array(elem)))
            s += len(elem)

        # To be added after removing transitions variables.
        nparams = len(all_indxs[0])

        F_photoz = scipy.linalg.block_diag(*all_pr)

        return use_inds, F_photoz

