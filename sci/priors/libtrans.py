#!/usr/bin/env python
# encoding: UTF8

w0, wa, om_m, om_de, = sp.symbols(['w0', 'wa', 'om_m', 'om_de'])

om_b, h, ns, sigma8 = sp.symbols(['om_b', 'h', 'ns', 'sigma8'])
#sigma8 = sp.

tos = {'w0': w0, 'wa': wa, 'om_m': om_m, 'om_de': om_de, 'h': h, 'ns': ns,
       'om_b': om_b, 'sigma8': sigma8,
       'Omega_DE': om_de,
       'Omega_k': 1 - om_m - om_de,
       'omega_m': om_m*h**2,
       'omega_b': om_b*h**2,
       'lnP': 2*sp.ln(sigma8)}

def trans_symb(par, data, from_params, to_params):
    """Transform data from from_params to to_params based on
       symbolic calculations."""

    from_symbols = [tos[x] for x in from_params]
    to_symbols = [tos[x] for x in to_params]

    # ok, more python magic should be applied here..
    e = {w0: par['w0'], wa: par['wa'], om_m: par['om_m'], om_de: par['om_de'],
         om_b: par['om_b'], h: par['h'], sigma8: par['sigma8']}

    l1 = []
    for x in from_symbols:
        l2 = []
        for y in to_symbols:
            ans = sp.diff(x, y)
            ans = ans.subs(e)
            l2.append(ans)

        l1.append(l2)

    M = np.array(l1)
    M = np.matrix(M)
    data = np.matrix(data)

    return M.T*data*M

