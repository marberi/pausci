#!/usr/bin/env python
# encoding: UTF8

# Functions used to read in and transform the priors
# on cosmological parameters. Some of these functions
# are very old.

# TODO: Look over the code to see what still makes sense.
# Beffer function names, removal of ald code and some
# comments will go a long way.

import ipdb
import copy
import itertools as it
import os
import sys
import numpy as np

import sci
from sci.lib.read_fisher import get_data
from sci import config,fisher
data_dir = 'data'

def get_table(fid):
    """Table with the numerical derivatives."""

    num_der = [
        ('Omega_DE', 'om_de', 1),
        ('Omega_k', 'om_m', -1),
        ('Omega_k', 'om_de', -1),
        ('omega_m', 'om_m', fid['h']**2.),
        ('omega_m', 'h', 2.*fid['om_m']*fid['h']),
        ('omega_b', 'om_b', fid['h']**2.),
        ('omega_b', 'h', 2.*fid['om_b']*fid['h']),
        ('lnP', 'sigma8', 2./fid['sigma8'])]

    return num_der 

def trans_num(fid, data, from_params, to_params):
    """Use the numerical Jacobian to transfer from one set of
       parameters to a new one.
    """


    table = get_table(fid)
    def find_num_der(x, y):
        if x == y: return 1
        for par, der, val in table:
             if x == par and y == der:
                 return val
    
        return 0

    M = np.zeros((len(from_params), len(to_params)))
    for i, x in enumerate(from_params):
        for j, y in enumerate(to_params):
            M[i,j] = find_num_der(x, y)

    M = np.matrix(M)
    data = np.matrix(data)

    return M.T*data*M

def _load_priors(file_name):
    """Load the priors from a file."""

    from_params = ['w0', 'wa', 'om_de', 'Omega_k', 'omega_m', 'omega_b', 'ns', 'lnP']
    to_params = ['w0', 'wa', 'om_m', 'om_de', 'h', 'om_b', 'ns', 'sigma8']

    myconf = {'opts.params': to_params, 'opts.exp_type': file_name, 'info.priors': []}

    root = os.path.dirname(os.path.realpath(__file__))
    root = os.path.join(root, '..')

    data_dir = os.path.join(root, 'data') 
    file_path = os.path.join(data_dir, 'detf', file_name)

    _fisher = get_data(file_path, False, True)

    # Fiducial values are needed when convertig the values..
    fid_to_trans = copy.deepcopy(config.cosmo)
    _fisher = trans_num(fid_to_trans, _fisher, from_params, to_params)
    data = {('cosmo', 'cosmo'): _fisher}

    return fisher.standard(myconf, data) #, add_priors=False)

def _fixed_priors():
#    _fisher = np.array([[1e7]])
    X = 1e8
    params = ['om_de', 'om_m', 'sigma8', 'ns'] #]
    params = ['om_m', 'sigma8', 'ns']
    _fisher = np.diag(X*np.ones(len(params)))
    myconf = {'opts.params': params, 'opts.exp_type': 'fixed', 'info.priors': []}

    data = {('cosmo', 'cosmo'): _fisher}

    print('Fisher', _fisher[0,0])
    return fisher.standard(myconf, data) #, add_priors=False)

def mypriors():
    """Main function for loading priors."""

    # Find priors
    tmp_opts = {'bias_priors': False}
    mypriors = {}

    detf_priors = ['cl', 'sn', 'wl']
    stage = 'II'
    for pr in detf_priors:
        name = "%s-%s" % (pr.upper(), stage)
        mypriors[pr] = _load_priors(name)

    mypriors['pl'] = _load_priors('planckfish')
    mypriors['fix'] = _fixed_priors()

#    ipdb.set_trace()
    return mypriors
