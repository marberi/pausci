#!/usr/bin/env python
# encoding: UTF8

# Priors on the bias and stochasticity.

import ipdb
import time

import numpy as np
import scipy
from scipy.linalg import block_diag

### <to_replace>
# This code was fount at:
# - http://stackoverflow.com/questions/19491936/complex-symmetric-matrices-in-python
# 
# if the updated bias prior method is working, one should be replacing
# this function.

def takagi(A) :
    """Extremely simple and inefficient Takagi factorization of a 
    symmetric, complex matrix A.  Here we take this to mean A = U D U^T
    where D is a real, diagonal matrix and U is a unitary matrix.  There
    is no guarantee that it will always work. """
    # Construct a Hermitian matrix.
    H = np.dot(A.T.conj(),A)
    # Calculate the eigenvalue decomposition of the Hermitian matrix.
    # The diagonal matrix in the Takagi factorization is the square
    # root of the eigenvalues of this matrix.
    (lam, u) = scipy.linalg.eigh(H)
    # The "almost" Takagi factorization.  There is a conjugate here
    # so the final form is as given in the doc string.
    T = np.dot(u.T, np.dot(A,u)).conj()
    # T is diagonal but not real.  That is easy to fix by a
    # simple transformation which removes the complex phases
    # from the resulting diagonal matrix.
    c = np.diag(np.exp(-1j*np.angle(np.diag(T))/2))
    U = np.dot(u,c)
    # Now A = np.dot(U, np.dot(np.diag(np.sqrt(lam)),U.T))
    return (np.sqrt(lam), U)

### </to_replace>



def static_priors(conf, data, block_type, popid):

    z = data['meta', ('z', popid)]
    pr_err = 'info.{0}.pr_{1}_err'.format(popid, block_type)
    f = 1./conf[pr_err]**2.

    return np.diag(f*np.ones(len(z)))

def dist_priors(conf, data, block_type, popid):
    """Priors as a linear function of distance."""

    # The code here internally use two populations even
    # if the interface let you bin in one way.
    z_mean1 = data['meta', ('z', popid)]

    # The formula for calculating the width is not correct.
    # Ideally the stored Fisher matrix should also include
    # this information.
    tmp = z_mean1[1:] - z_mean1[:-1]
    z_width1 = np.hstack([tmp[0], tmp])

    za1 = z_mean1 - 0.5*z_width1
    zb1 = z_mean1 + 0.5*z_width1

    nbins1 = len(z_mean1)
    za2, zb2, nbins2 = za1, zb1, nbins1

    Dr = conf['info.{0}.pr_{1}_dist'.format(popid, block_type)]
    q = np.zeros((nbins1, nbins2))


    n = 25
    e = np.ones((n, n))
    t1 = time.time()

    for i, (zai,zbi) in enumerate(zip(za1, zb1)):
        for j, (zaj,zbj) in enumerate(zip(za2, zb2)):


            x = np.linspace(zai, zbi, n)
            y = np.linspace(zaj, zbj, n)

            diff = np.abs((e.T*x).T - e*y)
            q[i,j] = np.sum((diff < Dr)*(1 - diff/Dr)) / n**2

    t2 = time.time()

    print('time block priors', t2-t1)
    f = 1./np.sqrt(np.diag(q))
    q2 = (q.T*f).T*f
#    F = np.linalg.inv(q2)

    return q2
#    return F

def create_bins(zmin, zmax, width0):

    z = zmin
    edges = [zmin]
    while z < zmax:
       width = width0*(1+z)
       assert 10**-5 < width, 'Artificially narrow redshift bins.'

       z += width
       if not zmax < z:
           edges.append(z)

    return np.array(edges)

def create_zedges(z0, zmax, nbins):
    r = (1+zmax)/(1+z0)
    w = -1 + r**(1./nbins)
    n = np.arange(nbins+1)
    edges = (1+z0)*(1+w)**n - 1

    return edges

def block_priors(conf, data, fisher, fslices, key):
    """Add bias priors to the Fisher matrix."""

    sblock = fslices[key]

    block_type,ftype,popid = key
    pr_type = conf['info.{0}.pr_{1}_type'.format(popid, block_type)]

    print('sbloch', block_type)
    use_deriv = conf['opts.deriv_{0}'.format(block_type)]
    dblock = conf['opts.d{0}'.format(block_type)]
    onebin_par = use_deriv and dblock[popid]
    onebin_par = conf['opts.deriv_bias'] and conf['opts.dbias'][popid]

    if not onebin_par:
        return fisher

    if pr_type == 'static':
        block_pr = static_priors(conf, data, block_type, popid)
        fisher[sblock, sblock] += block_pr
    elif pr_type == 'dist':
        # This code received many iterations. The variable m is indicating
        # the version number. Most old ones are deleted and there should
        # only be one when publishing the excalc:bias paper.
        m = 10

        mL = [9, 10]
        if m in mL:
            # This implementation is badly abusing the different
            # interfaces.
#            dist_pr = dist_priors(conf, data, block_type, popid)

            z_mean = data['meta', ('z', key[-1])]
            Dr = conf['info.{0}.pr_{1}_dist'.format(popid, block_type)]

            zmin = conf['fid.pop.{0}.z0'.format(popid)]
            zmax = conf['fid.pop.{0}.zmax'.format(popid)]
#            zmin -= 0.5*Dr # HACK.

            npoints = Dr

            zedgesP = create_zedges(zmin, zmax, npoints-1)
#                in_bin = np.digitize(z_mean, zedgesP) - 1

        if m == 9:
            if not npoints == 0:
                in_bin = np.digitize(z_mean, zedgesP) - 1

                z0 = zedgesP[in_bin]
                dz = z_mean - z0
                zwidthP = zedgesP[1:] - zedgesP[:-1]
                dx = dz / zwidthP[in_bin]


            nbias = fisher[sblock].shape[0]
#                npoints = len(zedgesP)

            M = np.zeros((nbias, nbias))
            if npoints == 0:
                pass
            else:
                for i in range(npoints):
                    ip = i+1
                    inds0 = in_bin == i
                    inds1 = in_bin == ip
                    M[i] += (1-dx)*inds0

                    if not nbias -1 == i:
                        M[ip] += dx*inds0


        # This should not be specified here at all. I only discovered
        # the fiducial bias was needed very late...
        biasf = {('bias', 'counts', 'bright'): lambda z: 2.0 + 2.0*(z - 0.5),
                 ('bias', 'counts', 'faint'): lambda z: 1.2 + 0.4*(z - 0.5)}

        from scipy.interpolate import splrep, splev
        if m == 10:
            # Change the naming.
            zm_bins = z_mean
            ze_points = zedgesP

            y0 = biasf[key](zm_bins) #_mean)
            y0_points = biasf[key](ze_points)

            nbins = len(zm_bins)
            npoints = len(ze_points)

            nbias = fisher[sblock].shape[0]

            assert nbins == nbias, 'Something went wrong.'

            # We first calculate the derivative and later construct
            # the transformation. While related, this enable some
            # checks.
            der = np.zeros((nbins,npoints))

            # This needs to be an option..
            k = 1

            for i in range(npoints):
                y_tmp = y0_points.copy()
                diff = y_tmp[i]*0.05
                y_tmp[i] += diff

                spl = splrep(ze_points, y_tmp, k=k)
                y1 = splev(zm_bins, spl)

                der[:,i] = (y1-y0) / diff


            # The transformation matrix.
            M = np.zeros((nbias, nbias))
            M[:npoints,:] = der.T

        if m in mL:
            forig = fisher.copy()

            fisher[sblock] = np.dot(M, fisher[sblock])
            fisher[:,sblock] = np.dot(fisher[:,sblock], M.T)


            # Just for secutity.
            assert np.sum(np.abs(fisher - fisher.T)) < 1

            # Fixing the remainding parameters. The exact value does
            # not matter.
            avg_diag = np.diag(fisher).mean()
            fisher[np.diag(fisher) == 0] = 10000*avg_diag

            return fisher


    elif pr_type == 'none':
        # This can be dropped by now...
        block_pr = np.zeros(fisher[sblock, sblock].shape)
    else:
        raise ValueError('No such prior type: {0}'.format(pr_type))


    return fisher
