#!/usr/bin/env python
# encoding: UTF8
import pdb
import time
import numpy as np

class deriv_trans(object):
    """Analytical derivatives of the photo-z transitions."""

    def _split_inds(self, inds):
        """Split a list with pairs of x,y into arrays with
           x and y. Simply a helper function.
        """

        x, y = zip(*inds)
        x = np.array(x)
        y = np.array(y)

        return x, y

    def _trans_inds(self, nbins):
        A = np.arange(nbins)

        m = np.repeat(A, nbins)
        n = np.tile(A, nbins)

        return m, n

#    @profile
    def deriv_r(self, clD, observD):
        """Derivative of the migrations matrix. See the paper
           arXiv:1109.4852 for a definition.
        """

        keys = clD['cl_act'].keys()
        obs = set(sum(zip(*keys), ()))

        # Formulas and definition is also given in the paper.
        # The notation is similar.
        Es = observD['all_pop']
        columns = []
        for obs1, obs2, _ in observD['obs_cov']:
            obs = (obs1, obs2)
            R = np.array(clD['R'][obs])
            L = np.array(clD['L'][obs])

            cl_shape = clD['cl_obs'][obs].shape

            inds_cl = observD['obs_exp'][obs]
            if not inds_cl:
                continue

            i,j = self.split_inds(inds_cl)

            tmp = []
            for E in Es:
                nbins_E = self.pop[E]['nbins'] 
                m,n = self.trans_inds(nbins_E)

                part = np.zeros((nbins_E**2, len(inds_cl)))
                if E == obs1[1]:
                    delta1 = np.add.outer(m,-i) == 0
                    part += delta1*R[np.ix_(n,j)]
                if E == obs2[1]:
                    delta2 = np.add.outer(m,-j) == 0
                    part += delta2*L[np.ix_(i,n)].T

                tmp.append(part)

            columns.append(np.vstack(tmp))

        return np.hstack(columns)
