#!/usr/bin/env python
# encoding: UTF8

import itertools as it
import pdb
import time
import numpy as np

class deriv_stoch(object):
    def deriv_stoch(self, corrD):
        """Derivative of the stochasticity."""

        observD = self.observS.observD(corrD['u'])

        A = list(it.product(observD['all_fluct'], observD['all_pop']))
        nbins = dict([(pop,self.pop[pop]['nbins']) for pop in observD['all_pop']])
        obs_cov = observD['obs_cov']

        deriv_all = []
        dbias = self.conf['opts.dstoch']
        for obs1,obs2,_ in obs_cov:
            ftype1, pop1 = obs1
            ftype2, pop2 = obs2

            obs_exp = observD['obs_exp'][(obs1,obs2)]
            nobs = len(obs_exp)

            deriv_obs = {}
            for fluct,pop in A:
                key = 'stoch',fluct,pop
                deriv_obs[key] = np.zeros((nbins[pop], nobs))

            if not (obs1 == obs2):
                deriv_all.append(deriv_obs)
                continue

            x,y = zip(*obs_exp)
            inds = np.array(x) == np.array(y)
            binnr = np.array(x)[inds]

            G = np.zeros((nbins[pop1], nobs))
            G[binnr, inds] = 1.
            deriv_obs[('stoch',)+obs1] = G

            deriv_all.append(deriv_obs)

        # Repeating code from deriv_bias... not good.
        deriv = {}
        deriv_fluct = self.conf['opts.deriv_fluct']
        for ftype,pop in A:
            if not dbias[pop]:
                continue

            if not deriv_fluct[ftype]:
                continue

            key = 'stoch',ftype,pop
            deriv[key] = np.hstack([X[key] for X in deriv_all])

        return deriv

