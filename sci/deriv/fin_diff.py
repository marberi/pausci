#!/usr/bin/env python
# encoding: UTF8

import ipdb
import copy
import functools
import sys
import time
import itertools as it
import numpy as np

import sci
from sci import config
from sci.deriv import deriv_bias, deriv_stoch, deriv_trans
import sci.probes

def compare_dicts(d1, d2):
    """Function to compare dictionaries. Only included for testing some new code."""

    keys1 = d1.keys()
    keys2 = d2.keys()

    assert set(keys1) == set(keys2)
    for key in keys1:
        v1 = d1[key]
        v2 = d2[key]

        if isinstance(v1, dict):
            compare_dicts(v1, v2)
        else:
            assert v1 == v2
#            ipdb.set_trace()


class fin_diff(deriv_bias.deriv_bias, 
               deriv_stoch.deriv_stoch,
               deriv_trans.deriv_trans,
               object):
    """Numberical derivative of the parameters based on
       a finite difference algorithm.
    """

    def __init__(self, conf, corr, pop, observ, syst):
        self.conf = conf
        self.corr = corr
        self.pop = pop
        self.observ = observ
        self.syst = syst

        self.paramsB = self._bias_params()
        self.paramsB['cosmo'] = self.conf['opts.params']

    def _bias_params(self):
        """Apply the macros to one configuration."""

        if not self.conf['opts.nuis.bias']:
            return {}

        conf = self.conf
        exp_types = conf['opts.exp_type']
        pop = conf['opts.pop']
        nrbias_params = config.general['nrbias_params']

        # Only coded for the bias by now...
        bparams = {}

        def find_params(popid, pre):
            res = ["pop.{0}.{1}{2}".format(popid, pre, i) for \
                   i in range(nrbias_params)]

            return res

#        allconf = sci.libconf(conf)
        dbias = conf['opts.dbias']
        deriv_bias = conf['opts.deriv_bias']

        if set(exp_types) & set(sci.probes.count_probes):
            for popid in pop:
                if deriv_bias and dbias[popid]:
                    continue

                bparams['bias','counts',popid] = find_params(popid, 'b')

        if set(exp_types) & set(sci.probes.mag_probes):
            for popid in pop:
                if deriv_bias and dbias[popid]:
                    continue

                bparams['bias','mag',popid] = find_params(popid, 'm')

        return bparams

    def _find_fid_diff(self, fid, param):
        """Step for the derivative of one parameter."""

        # TODO: Consider copy this to the module for the derivatives.

        fid_diff = copy.deepcopy(fid)
        DELTA_abs = self.conf['opts.delta_abs']
        DELTA_rel = self.conf['opts.delta_rel']

        fid_value = fid[param]
        if fid_value == 0:
            delta = DELTA_abs
        else:
            delta = fid_value*DELTA_rel

        fid_diff[param] += delta
        fid_diff['changed_value'] = fid_diff[param]

        # Not good if one wants another algorithm for finding the
        # derivative. It works for the time beeing.
        fid_diff['hash'] = param

        return fid_diff, delta

    def fid_diffs(self, fid):
        """Steps for the derivatives."""

        res = {}

        # A bit ugly, I have not decided quite on the format yet.
        params = sum(self.paramsB.values(), [])
        for param in params:
            fid_diff, delta = self._find_fid_diff(fid, param)
            res[param] = (fid_diff, delta)

        fid_copy = copy.deepcopy(fid)
        res['none'] = (fid_copy, 0)

        return res

    def _obs_vector(self, par):
        # Ok, testing passing the dataframe.
        observD = self.observS.static_observD()
        observD['df'] = self.observS['df']
        corr_iter = self.corr.create_iter(par, observD)

        for X in corr_iter:
            u = X['u']
            corrD = self.syst(par, u, X)

            observD = self.observS.observD(u)
            corr_vec = self.observ.sel_obs(corrD, observD)

            yield corrD, corr_vec

    def _deriv_numerical(self, diffs):
        """Numerical derivatives of parameters."""

        # There might be better ways of doing this...
        vector_iter = {}
        params = sum(self.paramsB.values(), [])
        for param in params+['none']:
            par, delta = diffs[param]
            vector_iter[param] = self._obs_vector(par)

        for corrD_fid, vec_fid in vector_iter['none']:
            derivB = {}
            for blockid, params in self.paramsB.iteritems():
                deriv = np.zeros((len(params), len(vec_fid)))

                for i,param in enumerate(params):
                    corrD_param, vec_param = vector_iter[param].next()
                    deriv[i] = (vec_param - vec_fid) /diffs[param][1]

                derivB[blockid] = deriv

            yield corrD_fid, derivB

    def deriv_iter(self, diffs):
        """Derivatives used when calculating the Fisher matrix."""

        num_iter = self._deriv_numerical(diffs)


        for corrD_fid, deriv in num_iter: #cosmo_iter:

            if self.conf['opts.mes_trans']:
                deriv.update(self.deriv_r(corrD_fid))

            if self.conf['opts.nuis.bias'] and self.conf['opts.deriv_bias']:
                deriv.update(self.deriv_bias(corrD_fid))

            if self.conf['opts.deriv_stoch']:
                deriv.update(self.deriv_stoch(corrD_fid))

            # Yields also the correlations, since these are used to
            # estimated the covariance.
            yield corrD_fid, deriv
