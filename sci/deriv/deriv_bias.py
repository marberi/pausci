#!/usr/bin/env python
# encoding: UTF8

import ipdb
import itertools as it
import time

import numpy as np

class deriv_bias(object):
    def _core_overlap(self, za1, zb1, za2, zb2):
        A = np.zeros((len(za1), len(za2)))

        w1 = zb1 - za1
        for i in range(len(za1)):
            for j in range(len(za2)):
                if zb1[i] <= za2[j]+1e-4:
                    A[i,j] = 1.
                elif zb2[j] < za1[i]+1e-4:
                    A[i,j] = 0.
                else:
                    A[i,j] = 0.

        return A

    def _deriv_overlap(self, observD):

        # Overlap 
        deriv_overD = {}
        all_pop = observD['all_pop']
        npop = len(all_pop)
        for i,j in it.product(range(npop), range(npop)):
#            if i < j: continue

            pop_name1 = all_pop[i]
            pop_name2 = all_pop[j]
            pop1 = self.pop[pop_name1]
            pop2 = self.pop[pop_name2]
            width1 = pop1['z_width']
            width2 = pop2['z_width']

            A = self._core_overlap(pop1['za'], pop1['zb'], pop2['za'], pop2['zb'])

            deriv_overD[pop_name1, pop_name2] = A

        return deriv_overD


    def deriv_bias(self, corrD):
        """Derivative of the bias."""

        observD = self.observS.observD(corrD['u'])

        obs_cov = observD['obs_cov']

        # Intialize the values to zero.
        A = list(it.product(observD['all_fluct'], observD['all_pop']))
        nbins = dict([(pop,self.pop[pop]['nbins']) for pop in observD['all_pop']])

        t1 = time.time()
        deriv_all = []

        dbias = self.conf['opts.dbias']
        for obs1,obs2,_ in obs_cov:
            # At high l, not all the observables (e.g. clustering) is included.
            if not (obs1,obs2) in observD['obs_exp']:
                continue

            ftype1, pop1 = obs1
            ftype2, pop2 = obs2

            I1 = corrD['I1'][(obs1, obs2)]
            I2 = corrD['I2'][(obs1, obs2)]
            L1 = corrD['L1'][(obs1, obs2)]
            L2 = corrD['L2'][(obs1, obs2)]

            r1 = self.pop[pop1]['rel_trans']
            r2 = self.pop[pop2]['rel_trans']

            obs_exp = observD['obs_exp'][(obs1,obs2)]
            nobs = len(obs_exp)

            deriv_obs = {}
            for fluct,pop in A:
                key = 'bias',fluct,pop
                deriv_obs[key] = np.zeros((nbins[pop], nobs))

            bder1 = np.zeros((nbins[pop1], nobs))
            bder2 = np.zeros((nbins[pop2], nobs))

            for x,(i,j) in enumerate(obs_exp):
                for n in range(nbins[pop1]):
#                    bder1[n,x] += r1[i,n]*I1[n,j] + r1[i,n]*L1[n,j]
                    bder1[n,x] += r1[i,n]*I1[n,j]

            for x,(i,j) in enumerate(obs_exp):
                for n in range(nbins[pop2]):
                    #bder2[n,x] += I2[i,n]*r2[j,n] + L2[i,n]*r2[j,n]
                    bder2[n,x] += I2[i,n]*r2[j,n] # + L2[i,n]*r2[j,n]

            deriv_obs[('bias',)+obs1] += bder1
            deriv_obs[('bias',)+obs2] += bder2

            deriv_all.append(deriv_obs)

        deriv = {}
        deriv_fluct = self.conf['opts.deriv_fluct']
        for ftype,pop in A:
            if not dbias[pop]:
                continue

            if not deriv_fluct[ftype]:
                continue

            key = 'bias',ftype,pop
            deriv[key] = np.hstack([X[key] for X in deriv_all])

#        import pdb
#        pdb.set_trace()

        t2 = time.time()
        print('time', t2-t1)

        return deriv
