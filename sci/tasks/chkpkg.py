#!/usr/bin/env python
# encoding: UTF8

import pdb
import sci

import taskbase

@sci.setup
class chkpkg(taskbase.base, object):
    """Test if the input package with observations gives problems
       when loading.
    """

    msg_filename = 'No input file name specified.'

    # Start of gluing together the parts..
    def __init__(self, myconf):
        self.conf = sci.libconf(myconf)

    __brownthrower_name__ = 'chkpkg'

    # Fields to properly implement.
    config_schema = """{}"""
    input_schema = """{}"""
    output_schema = """{}"""
    config_sample = """{}"""
    input_sample =  """{}"""
    output_sample = """{}"""

    @classmethod
    def check_config(cls, inp): pass

    @classmethod
    def check_input(cls, inp): pass

    @classmethod
    def check_output(cls, inp): pass

    @property
    def hashid(self):
        return self.conf.hashid()

    def run(self):
        file_name = self.conf['opts.pkg']
        assert file_name, self.msg_filename

        try:
            inst = sci.pkg.PkgRead(file_name, only_open=True)
            inst.read()
        except AssertionError, details:
            print('The loading failed with the error:') #z
            print(details) #z

            inst.h5file.close()
