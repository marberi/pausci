#!/usr/bin/env python
# encoding: UTF8

import pdb
import sci

import taskbase

@sci.setup
class fisher(taskbase.base, object):
    """Calculates one Fisher matrix.
       Using the built in model and priors it computes on Fisher matrix.
       A Fisher matrix is a quick way to estimate errors.
    """

    def __init__(self, config):
        self.myconf = config
        self.conf = sci.libconf(config)

    @property
    def hashid(self):
        return self.conf.hashid()

    __brownthrower_name__ = 'fisher'

    # Fields to properly implement.
    config_schema = """{}"""
    input_schema = """{}"""
    output_schema = """{}"""
    config_sample = """{}"""
    input_sample =  """{}"""
    output_sample = """{}"""

    @classmethod
    def check_config(cls, inp): pass
 
    @classmethod
    def check_input(cls, inp): pass

    @classmethod
    def check_output(cls, inp): pass

    def prolog(self, tasks, inp):
        from sci import probes
        subjobs = probes.comb_probes.subjobs(self.myconf)

        return subjobs

    def run(self, runner=False, inp=False, obj=True):
        from sci import probes
        exp_type = self.conf['opts.exp_type']
        assert not isinstance(exp_type, str), 'exp_type should not be a string'

        # This assumes the jobs has been splitted in different Fisher 
        # matrices in the prolog.
        assert len(exp_type) == 1
        x = exp_type[0]
        inst = probes.probes[x](self.conf)
        F = inst.fisher_mat()

        return F

    def epilog(self, tasks, out):
        from sci import probes

        return probes.comb_probes.add_parts(out)

    def load(self, file_path, info={}):
        """Load a previously store Fisher matrix and add the
           info dictionary.
        """

        conf_data, data = sci.fisher.store_hdf5.load(file_path)
        del conf_data['info']

        conf = sci.libconf(conf_data, False)
        conf.set_info(info)

        return sci.fisher.fisher_cosmo.fisher_cosmo(conf, data)


