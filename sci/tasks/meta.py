#!/usr/bin/env python
# encoding: UTF8

import pdb
import sci

import taskbase

@sci.setup
class meta(taskbase.base, object):
    """Package with only the metadata.
       Contains information on binning, angles/l and redshifts.
    """

    # Start of gluing together the parts..
    def __init__(self, myconf):
        self.conf = sci.libconf(myconf)

    __brownthrower_name__ = 'meta'

    # Fields to properly implement.
    config_schema = """{}"""
    input_schema = """{}"""
    output_schema = """{}"""
    config_sample = """{}"""
    input_sample =  """{}"""
    output_sample = """{}"""

    @classmethod
    def check_config(cls, inp): pass

    @classmethod
    def check_input(cls, inp): pass

    @classmethod
    def check_output(cls, inp): pass

    @property
    def hashid(self):
        return self.conf.hashid()

    def run(self):
        inst = sci.pkg.meta_write(self.conf)
        inst()
