#!/usr/bin/env python

class base(object):
    @property
    def config_schema(self):
        """Defines the allowed input."""

        return self.conf.schema()

    @property
    def hashid(self):
        return self.conf.hashid()
