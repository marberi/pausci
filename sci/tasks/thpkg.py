#!/usr/bin/env python
# encoding: UTF8

import pdb

import sci
import sci.pkg

import taskbase

@sci.setup
class thpkg(taskbase.base, object):
    """Package with theoretical values.
       Includes meta data, values of correlations, covariance and the transitions.
    """

    # Start of gluing together the parts..
    def __init__(self, myconf):
        self.conf = sci.libconf(myconf)

    __brownthrower_name__ = 'thpkg'

    # Fields to properly implement.
    config_schema = """{}"""
    input_schema = """{}"""
    output_schema = """{}"""
    config_sample = """{}"""
    input_sample =  """{}"""
    output_sample = """{}"""

    @classmethod
    def check_config(cls, inp): pass

    @classmethod
    def check_input(cls, inp): pass

    @classmethod
    def check_output(cls, inp): pass

    @property
    def hashid(self):
        return self.conf.hashid()

    def run(self):
        if self.conf['opts.use_data']:
            return sci.pkg.pkg_read.PkgRead(self.conf['opts.pkg'])
        else:
            return sci.pkg.pkg_write(self.conf)()

    def load(self, file_path, info):
        pkg = sci.pkg.PkgRead(file_path, info)

#        conf = sci.libconf(conf_data, False)
#        conf.set_info(info)

        return pkg
