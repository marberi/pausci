#!/usr/bin/env python
# encoding: UTF8

import pdb
import sci

import taskbase

@sci.setup
class detect(taskbase.base, object):
    """Estimates the chi2 values of a detection."""

    def __init__(self, config):
        self.myconf = config
        self.conf = sci.libconf(config)

    @property
    def hashid(self):
        return self.conf.hashid()

    __brownthrower_name__ = 'detect'

    # Fields to properly implement.
    config_schema = """{}"""
    input_schema = """{}"""
    output_schema = """{}"""
    config_sample = """{}"""
    input_sample =  """{}"""
    output_sample = """{}"""

    @classmethod
    def check_config(cls, inp): pass
 
    @classmethod
    def check_input(cls, inp): pass

    @classmethod
    def check_output(cls, inp): pass

    def run(self, runner=False, inp=False, obj=False):

        import sci.detect

        inst = sci.detect.estim_detect.estim_detect(self.myconf)
        D = inst.detect()

        return D

    def load(self, file_path, info={}):
        """Load a previously store Fisher matrix and add the
           info dictionary.
        """

        import sci.detect        
        conf_data, data = sci.detect.store_hdf5.load(file_path)
        del conf_data['info']

        conf = sci.libconf(conf_data, False)
        conf.set_info(info)

        return sci.detect.estim_detect.detect_cosmo(conf, data)
