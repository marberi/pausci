#!/usr/bin/env python
# encoding: UTF8

from chain import chain
from fisher import fisher
from mcmc import mcmc
from detect import detect

from chkpkg import chkpkg
from meta import meta
from thpkg import thpkg

pkg = thpkg
from regress import regress
