#!/usr/bin/env python
# encoding: UTF8

import os
import pdb

import sci
import taskbase

@sci.setup
class mcmc(taskbase.base, object):
    """Primitive way of encoding the dependencies. Just to get
       started.
    """

    __brownthrower_name__ = 'mcmc'

    def __init__(self, config):

        config['info.mcmc.nburn'] = 2
        config['info.npoints'] = 2
        config['fid.h'] = 0.7003
#        npoints = self.conf['info.npoints']
#        nburn = self.conf['info.mcmc.nburn']

        self.myconf = config
        self.conf = sci.libconf(config)


    # Fields to properly implement.
    config_schema = """{}"""
    input_schema = """{}"""
    output_schema = """{}"""
    config_sample = """{}"""
    input_sample =  """{}"""
    output_sample = """{}"""

    @classmethod
    def check_config(cls, inp): pass

    @classmethod
    def check_input(cls, inp): pass

    @classmethod
    def check_output(cls, inp): pass

    def run(self, runner=False, inp=False, obj=True):
        # Duplicate code. Should be handled by a separate layer.
        def find_info(myconf):
            res = {}
            for key,val in myconf.iteritems():
                if key.startswith('info.'):
                    new_key = key.replace('info.', '')
                    res[new_key] = val

            return res


        print('In mcmc wrapper...')

        # This should not be needed. We are still missing the layer
        # of integration between tasks.
        cachedir = sci.config.paths['cachedir']
        file_path = os.path.join(cachedir, 'pkg', self.hashid)
        pkg_exists = os.path.exists(file_path)

        if (not 'opts.pkg' in self.myconf) and (not pkg_exists):
            import copy
            myconf = copy.deepcopy(self.myconf)
            myconf['info.estim'] = 'HACK' 
            myconf['opts.pkg'] = '' 
            pkgtask = sci.tasks.thpkg(myconf)
            pkgtask.run()

        chain = sci.tasks.chain(self.myconf).run()
        if obj:
            return chain
