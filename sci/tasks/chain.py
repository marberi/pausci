#!/usr/bin/env python
# encoding: UTF8

import copy
import pdb

import sci
import taskbase

@sci.setup
class chain(taskbase.base, object):
    """Chain with mcmc values.
       Sample the parameter space to find the posterior distribution.
    """

    def __init__(self, config):
        myconf = config
        myconf['info.estim'] = 'mcmc'
        self.conf = sci.libconf(myconf)
        self.myconf = myconf

    __brownthrower_name__ = 'chain'

    # Fields to properly implement.
    config_schema = """{}"""
    input_schema = """{}"""
    output_schema = """{}"""
    config_sample = """{}"""
    input_sample =  """{}"""
    output_sample = """{}"""

    @classmethod
    def check_config(cls, inp): pass

    @classmethod
    def check_input(cls, inp): pass

    @classmethod
    def check_output(cls, inp): pass

    def load(self, file_path, info={}):
        """Load a previously stores chain and add the info dictionary.
        """

        conf_data, data = sci.chain.store_chain.load(file_path)
        conf = sci.libconf(conf_data, False)
        conf.set_info(info)

        return sci.chain.cosmo_chain(conf, data)

    def _run_chain(self, input_pkg, output_chain):
        myconf = copy.deepcopy(self.myconf)
        myconf['opts.pkg'] = input_pkg

        # Caching the Bessel functions speeds up calculating
        # the Fisher matrices. For chains it is less significant
        # since they are running longer. We can worry about this
        # later.
        sci.config.general['cache_bessel'] = False
        sci.config.general['use_cache'] = False

        # Estimated the chain. Only output can be found
        # in output_chain.
        conf = sci.libconf(myconf)
        inst = sci.probes.probes['magn'](conf) # HACK
        inst.get_chain(output_chain)

    def run(self, runner=False, inp=False, obj=True):
        conf = sci.libconf(self.myconf)
        inst = sci.probes.probes['magn'](conf) # HACK

        return inst.get_chain()
