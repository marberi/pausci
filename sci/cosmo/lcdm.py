#!/usr/bin/env python
# encoding: UTF8

# Defines the LCDM cosmology.

from __future__ import print_function, unicode_literals
import copy
import pdb
import numpy as np
import scipy
from scipy.integrate import quad
from scipy.special import hyp2f1
from scipy.interpolate import interp1d, splrep, splev, bisplrep, bisplev

#, RectBivariateSpline

def OLD_rect_spline(x, y, z):
    xi = np.repeat(x, len(y))
    yi = np.tile(y, len(x))

    return bisplrep(xi, yi, z.flatten())

class lcdm(object):
    def __init__(self, conf, par, power):
        self.conf = conf
        self.power = power
        self.power.cosmo = self # HACK. A bad one.
        self.cache = {}

    def cache_param(self, par, keep_values=False): 
        """Create splines for different cosmological functions
           for one parameter combination.
        """

        power_mod = self.conf['opts.module.power']
 
        cache = {}

        z = np.linspace(0., 6., 100)
        if self.conf['opts.spl_bug']:
            k = np.logspace(-5., 1., 100)
        else:
            k = np.logspace(-5., 3., 1000)

        H = self.H(z, par)
        D = self.D(par, z)
        chi = self._chi_z_core(par, z)
        r = self.f_K(par, chi)
        r_der = self._r_der(par, chi, H) 
        om_m = self.om_m(par, z)
        om_de = self.om_de_exact(par, z)

        gamma = self.gamma(par, z)

        cache['H'] = splrep(z, H)
        cache['D'] = splrep(z, D)
        cache['chi'] = splrep(z, chi)
        cache['r'] = splrep(z, r)
        cache['r_der'] = splrep(z, r_der)
        cache['om_m'] = splrep(z, om_m)
        cache['om_de'] = splrep(z, om_de)
        cache['gamma'] = splrep(z, gamma)

        # Ok, it has to be done in two steps. Later this
        # should be moved elsewhere.
        par_hash = par['hash']
        self.cache[par_hash] = cache
        if keep_values:
            self.cache[par_hash].update(cache)
        else:
            self.cache = {par_hash: cache}

        cache = {}
        T = self.power.tk_eh98(par, k)
        cache['T'] = splrep(k, T)


#        if power_mod in ['eishu', 'const']:
#            plin = self.power(par, k, z)
#            cache['plin'] = splrep(k, plin)
#        elif power_mod == 'camb':
#            pcamb_spl = self.power(par, k, z)
#            cache['pcamb'] = pcamb_spl

        if self.conf['opts.use_nl']:
            pk_lin, pk_nl = self.power(par, k, z)
            from scipy.interpolate import RectBivariateSpline

            # NOTE: THE Nonlinear power spectrum includes the growth.
            cache['plin'] = splrep(k, pk_lin)
            cache['pk_nl'] = RectBivariateSpline(k, z, pk_nl) 
#            pdb.set_trace()
        else:
            pk_lin = self.power(par, k, z)
            cache['plin'] = splrep(k, pk_lin)


        if self.conf['opts.module.bias'] == 'hod':
            # Using a special binning for the sigma spline is only because
            # the sigma implementation is slow...
            M = np.logspace(9, 15, 10)
#            M = np.logspace(6, 16, 20)
#            z_hod = np.linspace(0.01, 2., 20)
            z_hod = np.arange(0.01, 2., 0.1)
#            z_hod = np.array([0.5])
            R = self.mass_radius(par, z_hod, M)
            D_hod = self.D(par, z_hod)

            # TEASTING::::
            import sys
            from matplotlib import pyplot as plt

            sys.path.append('/Users/marberi/source/chomp')
            import cosmology as cpy
            A = cpy.MultiEpoch(0.2, 2.)
        
            R2 = A.mass_r(M, 0.5) 

            if False:
                plt.plot(M, R[0], label='Mine')
                plt.plot(M, R2, label='Chomp')
                plt.loglog() #xscale('log')
                plt.legend()
                plt.show()


            sigma_flat = self.power.sigma_r(cache['plin'], R.flatten())
            sigma_z0 = sigma_flat.reshape(R.shape)
            sigma = (sigma_z0.T*D_hod).T

            sr0 = np.array([A.sigma_r(Ri, 0.) for Ri in R[0]])
            sr = np.array([A.sigma_r(Ri, 0.5) for Ri in R[0]])
            sr_clust = self.power.cluster(par, R[0])
            from matplotlib import pyplot as plt
            if False:
                plt.plot(R[0], sigma[0], label='Mine')
                plt.plot(R[0], sr, label='Chomp')
                plt.plot(R[0], sr0, label='Chomp, z=0')
                plt.plot(R[0], sr_clust, label='Earlier try.')
                #plt.xlabel('R')
                #plt.yscale('log')
                plt.loglog()
                plt.legend()
                plt.show()

                pdb.set_trace()

            if False:
                myk = np.logspace(-3,1, 200)
                G1 = self.power.toint(myk, .01)
                G2 = A.epoch0._sigma_integrand(myk, .01)

                plt.plot(myk, G1, label='Mine')
                plt.plot(myk, G2, label='Chomp')
                plt.legend()
                plt.loglog()
                plt.show()

            def test_int(lnk, R):
                k = np.exp(lnk)
                kR = k*R
                W = 3.*(np.sin(kR)/kR**3. - np.cos(kR)/kR**2.)

                return k**3*1.*W**2.

            if False: #True:
                myk = np.logspace(-3,1, 10)
                myk_log = np.log(myk)
                G1 = test_int(myk_log, .01)
                G2 = A.epoch0._test_integrand(myk_log, .01)

                plt.plot(myk, G1, 'o-', label='Mine')
                plt.plot(myk, G2, 'o-', label='Chomp')
                plt.legend()
                plt.loglog()
                plt.show()


               # G2 = 
#                pdb.set_trace()

            sm = np.array([A.sigma_m(Mi, 0.5) for Mi in M])

            if False: #True:
                wind = self.power.wind(k, 0.1)
                plt.plot(k, wind)
                plt.xscale('log')
                plt.show()

#            pdb.set_trace()
            if False: #True:
                plt.plot(M, sigma[0], label='Mine')
                plt.plot(M, sm, label='Chomp')
                plt.xlabel('M')
                plt.xscale('log')
                plt.show()

            if False:
                p2 = A.linear_power(k)
                plt.plot(k, pk_lin, label='Mine')
                plt.plot(k, p2, label='Chomp')
#                plt.xlim(np.min(k), 10)
                plt.loglog()
                plt.legend()
                plt.show()
 
                pdb.set_trace()

            if False: #True:
                p2 = A.linear_power(k)
                plt.plot(k, pk_lin/p2, label='Ratio pk')
                plt.xlim(np.min(k), 10)
                plt.xscale('log')
                plt.show()
            # sigma is not represented as a spline since we currently
            # don't use this quantity.

            from scipy.interpolate import RectBivariateSpline

            def f_core(nu):
                a = 1./np.sqrt(2.)
                p = 0.3

                nupr = a*nu

                return (1.+nupr**(-p))*np.sqrt(0.5*nupr)*np.exp(-0.5*nupr)/nu

#                return np.sqrt(2.*anu2/np.pi)*(1. + anu2**(-p))*np.exp(-0.5*anu2)/nu

            # Sheth-Tormen mass function.
            from scipy.integrate import quad
            nu_int = np.logspace(-2, 2.5)
            amp = quad(f_core, 0.105, 50.)[0]
            st99_mf = f_core(nu_int)/amp

            from mass_function import MassFunction
            if False:
                MF = MassFunction()
                mf2 = MF.fdebug(nu_int)

                pdb.set_trace()
                plt.plot(nu_int, st99_mf, label='Mine')
                plt.plot(nu_int, mf2, label='Chomp')
                plt.xlabel('nu')
                plt.title('Mass function')
                plt.loglog()
                plt.legend()
                plt.show()


            delta_c = self.delta_c(par, z)
            cache['delta_c'] = splrep(z, delta_c)
            cache['st_mf'] = splrep(nu_int, st99_mf)

            # Mass to nu.
            delta_c_hod = self.delta_c(par, z_hod)
            nu = (delta_c_hod/sigma.T).T**2
            cache['nu'] = RectBivariateSpline(z_hod, np.log10(M), nu, s=1)
#            cache['nu'] = rect_spline(z_hod, np.log10(M), nu)

#            np.savetxt('fisk', nu)
#            sys.exit()

            nu2 = np.array([A.nu_m(Mi, 0.5) for Mi in M])
            self.cache[par_hash].update(cache)
            nu3 = self.cache_ev2dgrid(par, 'nu', z_hod, np.log10(M))

            if False:
                plt.plot(M, nu[5], 'o--', label='Mine')
                plt.plot(M, nu2, label='Chomp')
                plt.plot(M, nu3[5], 'o--', label='From spline')
                plt.xlabel('M')
                plt.ylabel('nu')
                plt.loglog() #xscale('log')
                plt.legend()
                plt.show()

#            pdb.set_trace()
#            from matplotlib import pyplot as plt
#            plt.plot(nu_int, st99_mf, 'o-')
#            plt.show()
#            pdb.set_trace()


        self.cache[par_hash].update(cache)

        if False:
            M2 = np.logspace(10, 15)
            M2 = np.log10(M2)
            nu3 = self.cache_ev2dgrid(par, 'nu', np.array([0.5]), M2)
            plt.plot(M2, nu3)
            plt.show()

#        pdb.set_trace()

    def gen_cache(self, par_derivs, to_calc, power):
        if not set(['cosmo', 'all']) & set(to_calc):
            return

        for par,delta in par_derivs.values():
            self.cache_param(par, True)

    def find_spl(self, par, f):

        return self.cache[par['hash']][f]
 

    def cache_ev(self, par, f, x_array):
        """Evaluate cache for the 1D function f."""

        par_hash = par['hash']
        try:
            spl = self.cache[par_hash][f]
        except KeyError:
            msg_store = 'No values stored for: %s,%s' % (par_hash, f)
            raise msg_store

        assert x_array.ndim <= 2
        if x_array.ndim == 1:
            return splev(x_array, spl, ext=2)
        else:
            shape = x_array.shape
            return splev(x_array.flatten(), spl, ext=2).reshape(shape)

    def cache_ev2dgrid(self, par, f, x_array, y_array):
        """Evaluate cache for the 2D function f."""

        spl = self.cache[par['hash']][f]

        xi = np.repeat(x_array, len(y_array))
        yi = np.tile(y_array, len(x_array))
        shape = (len(x_array), len(y_array))

        return spl.ev(xi, yi).reshape(shape)

#        pdb.set_trace()
#        ans = bisplev(xi, yi, spl).reshape(shape)
#        ans = bisplev(x_array, y_array, spl)

#        return ans
    
    def N(self, l):
        """Number of measured modes."""

        return 2*(2*l + 1)

   
    def f_K(self, par, chi, deriv=False):
        """Convert between radial and transverse distances."""

        om_k = 1. - par['om_m'] - par['om_de']
        A = np.sqrt(np.abs(om_k))*par['H_0']/par['c']

        if self.conf['opts.h_bug']:
            #chi = chi / par['h']
            A = A * par['h']

        if om_k < 0.:
            f_K = np.sin(A*chi)/A
            der = np.cos(A*chi)
        elif om_k == 0.:
            f_K = chi
            der = np.ones(len(chi))
        elif om_k > 0.:
            f_K = np.sinh(A*chi)/A
            der = np.cosh(A*chi)

        if not deriv:
            return f_K
        else:
            return f_K, der

    def f_K_rdep(self, par, chi, z):
        om_de = self.om_de_exact(par, z)
        om_m = self.om_m(par, z)

#        om_k = 1. - par['om_m'] - par['om_de']
        om_k = 1. - om_m - om_de
        A = np.sqrt(np.abs(om_k))*par['h']*par['H_0']/par['c']

        z_c1 = om_k < 0.
        z_flat = om_k == 0.
        z_c3 = om_k > 0.

        res = []
        for i in range(len(z)):
            if om_k[i] < 0.:
                res.append(np.sin(A[i]*chi[i])/A[i])
            elif om_k[i] == 0.:
                res.append(chi[i])
            elif om_k[i] > 0.:
                res.append(np.sinh(A[i]*chi[i])/A[i])


        return np.array(res)

        res = z_c1 * np.sin(A*chi)/A + \
              z_flat * chi + \
              z_c3 * np.sinh(A*chi)/A

        return res

        if om_k < 0.:
            return np.sin(A*chi)/A
        elif om_k == 0.:
            return chi
        elif om_k > 0.:
            return np.sinh(A*chi)/A



    def chi_H(self, par, z):
        """Def. follows after eq.43."""

        return par['c'] / self.H(z, par)

    def _r_der(self, par, chi, H):
        # Only available through the cache since chi and H is passed.

        _, der = self.f_K(par, chi, True)

        return der*par['c'] / H


         
    def de_eos(self, z, par):
        """Dark energy equation of state."""

        return par['w0'] + par['wa']*z/(1.+z)

    def om_de_exact(self, par, z):
        """Om_de for w(z) = w0 + wa*z/(1+z)."""

        w0 = par['w0']
        wa = par['wa']
                
        return par['om_de']*(1+z)**(3.*(1.+w0+wa))*np.exp(-3.*wa*z/(1.+z))

    def om_m(self, par, z):
        """Redshift dependent matter density."""

        H = self.H(z, par)

        fac = 2. if not self.conf['opts.d_bug'] else 1.
        om_m = par['om_m'] * (1.+z)**3 * (par['H_0']/H)**fac
        if self.conf['opts.h_bug']:
            om_m = om_m *par['h']**fac

        return om_m

    def delta_c(self, par, z):
        delta_c = 1.6865*np.ones(len(z))
        om_m = self.om_m(par, z)

        om_k = 1. - par['om_m'] - par['om_de']
        if om_k == 0:
            delta_c *= om_m**0.0055
        else:
            raise NotImplementedError

        # Only for testing...
        D = self.D(par, z)
        return delta_c/D
#        return delta_c
 
    def rho_crit(self, par, z):
        """Critical density of the universe."""

        H = self.H(z, par)

        return 3.*H**2/(8*np.pi*4.302e-9)

    def mass_radius(self, par, z, M):
        # NOTE: Check these expressions...
        rho_crit = self.rho_crit(par, z)
        om_m = self.om_m(par, z)
        rho_bar = rho_crit*om_m

        pre = 3./(4.*np.pi)
        radius = (pre*np.outer(1./rho_bar, M))**(1./3.)
    
        return radius

    def standard_growth_index(self, par, z):
    #def growth_index(self, par, z):
        """Eq.11. Growth index assuming general relativity."""

        w = self.de_eos(z, par)
        return 3.*(w - 1.)/(6.*w - 5.)

    def growth_index(self, par, z):

        w_z1 = self.de_eos(np.array([1.]), par)[0]

        # Here I don't think k is the scale.
        if -1 <= par['w0']:
            k = .55 + .05*(1+w_z1)
        else:
            k = .55 + .02*(1+w_z1)

        if isinstance(z, float):
            return k
        else:
            return k*np.ones(len(z))

    def g0_ga_form(self, par, z):
        # Since I don't know if this can happen..
#        assert not isinstance(z, float)

#        g0 = par['gamma']
#        ga = par['ga'] 

        return par['gamma'] + par['ga']*z/(1.+z)

        pdb.set_trace()

    def gamma(self, par, z, k=False, use_diag=False):
        """Growth index."""

        scale_dep = True if isinstance(k, np.ndarray) else False

        params = self.conf['opts.params']
        if 'ga' in self.conf['opts.params']:
            gi = self.g0_ga_form(par, z)
        elif 'gamma' in params:
            gi = par['gamma'] if isinstance(z, float) else par['gamma']*np.ones(len(z))
        else:
            gi = self.growth_index(par, z)

        if scale_dep and not use_diag:
            return np.tile(gi,(len(k),1)).T
        else:
            return gi


    def H(self, z, par):
        """Eq.6. The Hubble parameter for a spesific time over the same parameter
        for time 0."""

        # Note: The integration routine requires z as the first param.
        a = 1./(1. + z)

        om_de_z = self.om_de_exact(par, z)

        ans = par['H_0'] * np.sqrt(par['om_m']*a**-3. + om_de_z + \
                                   (1.-par['om_m']-par['om_de'])*a**-2.)

        if self.conf['opts.h_bug']:
            ans = ans * par['h']

        return ans

    def E(self, par, z):
        """Note: Don't use this code."""

        return self.H(z, par) / par['H_0']

    def D_albert_inner(self, par, zi, zmax):
        """DO NOT USE."""

        def to_int(x):
            return (1+x)/self.E(par, x)**3.

        pre = (5./2.)*par['om_m']*self.E(par, zi)
        
        return pre*quad(to_int, zi, zmax)[0]

    def D_albert(self, par, z, zmax):
        ans = np.array([self.D_albert_inner(par, zi, zmax) \
                        for zi in z])

        norm = self.D_albert_inner(par, 0., zmax)

        return ans / norm

    def D_exact(self, par, z):
        """Do not use.."""
        assert par['w0'] == -1

        Om0 = par['om_m']
        return hyp2f1(1./3, 1., 11./6, (Om0 - 1.)/Om0 *(1./(1. + z))**3)/ \
               (hyp2f1(1./3, 1., 11./6 , (Om0 - 1.)/Om0)*(1. + z))



    def f_modified(self, z, par):
        """Eq.9. Modified to include extra term to ease integration."""


        gi = self.gamma(par, z)
        om_m = self.om_m(par, z)

        return (1+z)**(-1.)*om_m**gi



    def D(self, par, z):
        """Linear growth function."""

        if isinstance(z, float):
            z = [z]

        # Note: The function f changed from the paper.
        if isinstance(z, list):
            z = np.array(z)


        res = np.exp(-np.array([quad(self.f_modified, 0, end_pos, par)[0] for end_pos in z]))

        return res


    def _chi_z_core(self, par, z):
        """Eq.43. The radial coordinate in term of redshift."""

        # Note: In practice this does not increase the speed considerably.
        n = len(z)
        pairs = [(0,z[0])] + [(z[i], z[i+1]) for i in range(n-1)]
        pair_int = [quad(lambda x, y: 1./self.H(x,y), a, b, par)[0] for a, b \
                    in pairs]

        return par['c']*np.cumsum(pair_int)

    def k(self, par, z, l):
        """Note that this equation is not used when doing the exact calculations."""

        l = float(l)
        r = self.cache_ev(par, 'r', z)

        return (l + .5)/ r

    def P_funk(self, par, l, popid, z):
        """Defined in Enriques notes2 version4."""

        k = self.k(par, z, l)

        power_mod = self.conf['opts.module.power'] 
        if power_mod in ['eishu', 'lin', 'trans', 'const']:
#            D = self.D(par_copy, z)
#            plin = D**2*power_inst(par_copy, k, raise_error=False)
            D = self.cache_ev(par, 'D', z)
            plin = D**2*self.cache_ev(par, 'plin', k)
            power_spec = plin
        elif power_mod == 'nonlin':
            pdb.set_trace()
            raise ValueError 
        elif power_mod == 'camb':
            power_spec = self.cache_ev2(par, 'pcamb', k, z)
        else:
            raise ConfigError

        r = self.cache_ev(par, 'r', z)

        if self.conf['opts.fs_bug']:
            r_der = self.chi_H(par, z)
        else:
            r_der = self.cache_ev(par, 'r_der', z)

        return power_spec/(r**2*r_der)

