#!/usr/bin/env python
# encoding: UTF8

__all__ = ['rsd', 'wl']

count_probes = ['magn', 'mag_all', 'gm', 'rsd', 'gs_mod', 'gs_cross', 'gs', 'gs_vol', 'des',
                'gs_minus', 'gs_minus2', 'gs_minus3', 'gs_nocc', 'gs_mini', 'ys', 'magn_nox', 'magn_nox2',
                'gg_auto', 'gg_cross', 'counts_vol'] 

mag_probes = ['mm', 'mag_all', 'gm', 'gm_red']
other_probes = ['shear']

import sci.probes.comb_probes
#from sci.probes.new_bao import bao
from sci.probes.bao import bao
from sci.probes.rsd.rsd import rsd

# Yes, this is definitively not the best way...
from sci.probes.wl import wl
probes = {
    'bao': bao,
    'rsd': rsd}

for x in set(count_probes+mag_probes+other_probes):
    probes[x] = wl

