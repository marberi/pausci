#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np
from scipy.linalg import block_diag

import sci

class bao(object):
    def __init__(self, conf):
        self.conf = conf
        par = conf['fid'] # HACK

#        power = sci.power.eishu(conf)
#        self.cosmo = sci.cosmo.lcdm(conf, par, power)
#        self.deriv A

        meta = {}
        to_calc = ['all']
        modules = sci.lib.libmodules.import_modules(conf, meta, to_calc)
        for key, val in modules.items():
            setattr(self, key, val)

    def jacobi(self, diffs, z):
        fid = diffs['none'][0]
        Da_none = self.cosmo.cache_ev(fid, 'chi', z) / (1.+z) / fid['h']
        H_none = fid['h']*self.cosmo.cache_ev(fid, 'H', z)

        res = []
        for param in self.conf['opts.params']:
            par, delta = diffs[param]

#        for key, (par, delta) in diffs.iteritems():
#            if key == 'none': continue

            Da = self.cosmo.cache_ev(par, 'chi', z) / (1.+z) / par['h']
            H = par['h']*self.cosmo.cache_ev(par, 'H', z)

            Da_deriv = (Da - Da_none) / delta
            H_deriv = (H - H_none) / delta

#            if key == 'om_m':
#                pdb.set_trace()

            # Trick for interleaving two arrays. Preferred since the correlations
            # are inside of each redshift bin.
            res.append(np.dstack((Da_deriv, H_deriv)).flatten())

        return np.vstack(res)

    def cov_inv(self, z, err_Da, err_H, R):
        fid = self.conf['fid']
        Da_none = self.cosmo.cache_ev(fid, 'chi', z)/(1.+z) / fid['h']
        H_none = fid['h']*self.cosmo.cache_ev(fid, 'H', z)

        abs_Da = Da_none*err_Da 
        abs_H = H_none*err_H
        cross = err_Da*err_H*R

        res = []
        for i in range(len(err_Da)):
            cov_block = np.array([[abs_Da[i]**2, cross[i]], [cross[i], abs_H[i]**2]])
            res.append(np.linalg.inv(cov_block))

#            res.append(np.array([[abs_Da[i]**2, cross[i]], [cross[i], abs_H[i]**2]]))

        return block_diag(*res)


    def fisher_mat(self):

        in_file = '/Users/marberi/sci/sci/data/bao/bbbao.txt'

        # Input errors are in percentage.
        z,err_Da,err_H,R = np.loadtxt(in_file, usecols=[0,1,2,3], unpack=True)
        err_Da /= 100. 
        err_H /= 100.

        diffs = self.deriv.fid_diffs(self.conf['fid'])
        self.cosmo.gen_cache(diffs, ['all'], self.power)
        jacobi = self.jacobi(diffs, z)

#        cov = self.xcov(z, err_Da, err_H, R)
#        cov_inv = np.linalg.inv(cov)
        cov_inv = self.cov_inv(z, err_Da, err_H, R)

        fisher = np.dot(jacobi, np.dot(cov_inv, jacobi.T))
        data = {('cosmo', 'cosmo'): fisher}

        return sci.fisher.standard(self.conf, data)
#        return None
#        pdb.set_trace()
