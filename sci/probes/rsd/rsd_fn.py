#!/usr/bin/env python
# encoding: UTF8

import os
import pdb

cosmo_params = ['om_m', 'om_de', 'h', 'sigma8', 'om_b', 'w0', 'wa', 'gamma', 'ns']
bias_params = ['b1p1', 'b2p1', 'b3p1', 'b4p1']

def file_name(conf, opts, fid):
    """Match RSD configuration to a specific file name."""


    from_area = 200
    params = cosmo_params + bias_params

    assert len(opts['pop']) == 1
    pop = opts['pop'][0]
    file_fmt = "radial/FisherMat_RD_PAU200_{0}_dz0p3_AP.{1}.dat"

    file_name = file_fmt.format(pop, opts['rsd_suf'])


#    file_name, params, from_area = rsd_fn1.file_name(opts, fid)
#    suf = '_'+opts['rsd_suf']+'.dat'
#
#    file_name = file_name.replace('_case_fiducial.dat', suf)
#
#
#    file_name = file_name.replace('dzvar0p1', 'dz0p2')
#    file_name = os.path.join('rsdwa_bug', file_name) 

    return file_name, params, from_area
