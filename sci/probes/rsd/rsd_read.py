#!/usr/bin/env python2.6
import numpy as np

def get_data(file_name):
    """Read fisher matrix from .dat file."""

    tmp = []
    for line in open(file_name).readlines():
        h = line.strip().split('\t')
        h = [float(x) for x in h]

        tmp.append(h)

    return np.matrix(tmp)
