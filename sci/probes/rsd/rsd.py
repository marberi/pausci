#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals
import copy
import os
import pdb
import numpy as np

import sci
from sci import fisher
from sci.probes.rsd import rsd_fn
from sci.probes.rsd import rsd_read

def conv_old(myopts, myfid, myinfo):
     myopts = copy.deepcopy(myopts)
     myfid = copy.deepcopy(myfid)
     myinfo = copy.deepcopy(myinfo)

     myconf = {}
     dict_list = [('opts', myopts), ('fid', myfid), ('info', myinfo)]

     #d = {'opts': myopts, 'fid': myfid, 'info': myinfo}
     #res = keys(d)

     for name, d in dict_list:
        if not d: continue
        for key, val in d.items():
            if key in ['myfid']: continue
            myconf['%s.%s' % (name,key)] = val

     keys = ['opts.approx', 'opts.dir', 'opts.module', 'opts.source', 'fid.pop',
             'fid.hash', 'fid.changed_param', 'info.errors', 'info.width']

     for key in keys:
         if key in myconf:
             del myconf[key]

#     del myconf['opts.dir']
     return myconf


class rsd(object):
    """3D redhist space distortions."""
    def __init__(self, conf):
        self.conf = conf

    def zero_fisher_matrix(self, conf):
        nparams = len(conf['opts.params'])
        data = {('cosmo', 'cosmo'): np.zeros((nparams, nparams))}

        return fisher.standard(conf=conf, data=data)

  
    def fisher_mat(self):
        conf = self.conf.copy() #copy.deepcopy(conf)

        opts, fid = conf['opts'], conf['fid']

        if 'mm' in conf['opts.exp_type']:
            return self.zero_fisher_matrix(conf)

        file_name, params, from_area = rsd_fn.file_name(conf, opts, fid)

        file_path = os.path.join('data/rsd', file_name)

        scale_area = fid['area'] / from_area

        if os.path.exists(file_path):
            data = rsd_read.get_data(file_path)
        else:
            print('ga..', file_path)
            return self.zero_fisher_matrix(conf)

        data = scale_area*data

        opts_tmp = copy.deepcopy(opts)
        opts_tmp['params'] = params
        opts_tmp['exp_type'] = 'rsd'

        del fid['f_sky']
        conf_data = {'opts': opts_tmp, 'fid': fid, 'info': conf['info']}
        conf_data = conv_old(opts_tmp, fid, conf['info'])
        conf_tmp = sci.libconf(conf_data)
       
        F_tmp = fisher.standard(conf=conf_tmp, data=data)

        excl_params = ['m1p1','m2p1','m3p1','m4p1','m1p2','m2p2','m3p2','m4p2','fnl']
        params = [x for x in opts['params'] if not x in excl_params]
        F = F_tmp.remove(to_keep=params)

        return F

