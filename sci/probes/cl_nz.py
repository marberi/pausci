#!/usr/bin/env python
from __future__ import print_function

# Code for measuring the n(z) when doing correlations.
# This was a bit cool, but I have not used it for anything
# serious yet.
# TODO: Consider including this in the paper.
import itertools as it
import numpy as np

class meassure_nz(object):
    def nz_trans_pop(self, popid):
        """The real N(z) variables for one population."""
        nbins = self.pop[popid]['nbins']
        ngal_obs = self.pop[popid]['ngal_obs']
        r_mask_1d = self.pop[popid]['r_mask_1d']
        T = self.pop[popid]['bin_trans']

        r_vars = list(zip(*r_mask_1d))
        nz_jacb = np.zeros((len(r_vars), nbins))
        toiter = it.product(enumerate(r_vars), range(nbins))

        for (nr,(k,l)),i in toiter:
            nz_jacb[nr,i] = T[k,i]/ngal_obs[k] if i==l else 0.

        nz_jacb = np.matrix(nz_jacb)
        if self.conf['opts.rev_trans']:
            jacb_s_r = self.pop[popid]['jacb_s_r']
            nz_jacb = jacb_s_r * nz_jacb

        return nz_jacb

    def nz_trans(self):
        """Jacobi transformation to find the real n(z)."""

        assert len(self.pop['meta']['pop']) == 1
        popid = self.pop['meta']['pop'][0]

        nparams = len(self.conf['opts.params'])
        tot_nbins = sum(self.pop[popid]['nbins'] for popid in\
                        self.pop['meta']['pop'])

        # vertical - old params, horizonal - new params
        res = [np.zeros((nparams, tot_nbins))]

        for popid in self.pop['meta']['pop']:
            res.append(self.nz_trans_pop(popid))

        res = np.vstack(res)

        return np.matrix(res)

    def messure_nz(self, fisher):
        """Meassure nz from data."""

        # Neede to find the true n(z).
        assert self.conf['opts.mes_trans']
        all_nz_jacb = self.nz_trans()

        g1 = all_nz_jacb.T*fisher
        g2 = g1*all_nz_jacb

        ext_fisher = np.vstack([np.hstack([fisher, g1.T]),
                                np.hstack([g1, g2])])

        ext_cov = np.linalg.pinv(ext_fisher)
        diag = np.diag(ext_cov)
        pop = self.conf['opts.pop']
        s = len(self.conf['opts.params'])
        res = {}
        for popid in pop:
            nbins = self.pop[popid]['nbins']
            res[popid] = np.sqrt(diag[s:s+nbins])
            s += nbins

        z_faint = self.pop['faint']['z']
        ngal_act_faint = self.pop['faint']['ngal_act']
        err_faint = res['faint']

        for x in zip(z_faint, ngal_act_faint, err_faint):
            print(*x) #z

