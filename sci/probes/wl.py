#!/usr/bin/env python
# encoding: UTF8

# Weak Lensing and Large scale structure predictions.
from __future__ import print_function, unicode_literals
import os
import pdb
import numpy as np

import sci
import sci.lib.libmodules
from sci import fisher,chain,priors

class wl(fisher.estim_fisher,  chain.estim_chain,  priors.trans_priors, 
         object): 
    """Either Fisher forecast or MCMC predictions for WL/
       LSS.
    """

    def __init__(self, conf, to_calc=['all']):
        self.conf = conf

        pkg_name = self.conf['opts.pkg']
        if pkg_name:
            self.pkg = sci.pkg.PkgRead(pkg_name)
        elif self.conf['info.estim'] == 'mcmc':
            cachedir = sci.config.paths['cachedir']
            file_path = os.path.join(cachedir, 'pkg', conf.hashid())
            self.pkg = sci.pkg.PkgRead(file_path)
        else:
            self.pkg = False

        modules = sci.lib.libmodules.import_modules(conf, self.pkg, to_calc)
        for key, val in modules.items():
            setattr(self, key, val)

    def test_config(self):
        conf = self.conf
        if conf['opts.mes_trans']:
            assert conf['opts.pop_type'] == 'pop_cat'
