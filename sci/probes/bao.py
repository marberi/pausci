#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals

# The BAO prediction only read in files which FJC made
# with predictions.
import os
import pdb

import sci
from sci.lib import read_fisher

class bao(object):
    def __init__(self, conf):
        self.conf = conf

    def area_scaling(self, fid, area):
#        area = 200
        hack_factor = fid['area'] / float(area)

        return hack_factor

    def find_data(self, opts, fid):
        data = 0.
        pop_to_ind = {'faint': 'F', 'bright': 'B'}
        d = os.path.join(opts['dir']['data'], 'bao')

        area = 14000 # HACK
        hack_factor = self.area_scaling(fid, area)

        assert len(opts['pop']) == 1
        for pop_name in opts['pop']:
            pop_name = 'bright' if pop_name == 'spec' else pop_name # HACK
            pop_ind = pop_to_ind[pop_name]

            file_name = 'new_bao.out'
            file_name = 'msdesi_k0p1_v1.out' # HACK

            file_path = os.path.join(d, file_name)
            data_pop = read_fisher.get_data(file_path, False, True)
            data += hack_factor*data_pop

            # Yes, a bit hackerish..
            data = data[:-2,:-2]


        return data

    def fisher_mat(self):
        fid = self.conf['fid']
        opts = self.conf['opts']

        params = ['om_m', 'om_de', 'h', 'sigma8', 'om_b', 'w0', 'wa']
        assert set(params).issubset(opts['params'])

        cosmo_data = self.find_data(opts, fid)
        data = {('cosmo', 'cosmo'): cosmo_data}

        # Ignoring other parameters...
        conf = self.conf.copy()
        conf['opts']['params'] = params

        F_tmp = sci.fisher.standard(conf=conf, data=data)

        return F_tmp
