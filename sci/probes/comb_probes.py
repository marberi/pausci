#!/usr/bin/env python
# encoding: UTF8

import copy

def subjobs(orig_myconf):
    """Split the configuration into separate Fisher matrices."""
 
    myconf = copy.deepcopy(orig_myconf)
    if len(myconf['opts.pop']) == 1 or (myconf['info.op'] == 'x'):
        myconf1 = copy.deepcopy(orig_myconf)
        return (myconf1,)

    else:
        myconf1 = copy.deepcopy(orig_myconf)
        myconf2 = copy.deepcopy(orig_myconf)

        pop = myconf1['opts.pop']
        myconf1['opts.pop'] = [pop[0]]
        myconf2['opts.pop'] = [pop[1]]

        return (myconf1, myconf2)

def add_parts(Fs):
    """Combine the results from different Fisher matrices."""

    if len(Fs) == 1:
        return Fs[0]
    elif len(Fs) == 2:
        F = Fs[0]
        return F.add_extended(Fs[1])
    else:
        raise NotImplementedError('blah...')

#def combine_fisher_matrices(myconfL):
#    """For multiple non-overlapping populations, split the
#       request and perform the calculations separately.
#    """
#
#    assert isinstance(myconfL, list)
#
#    # OK, this has to be written better...
#    intup = map(to_tuple, myconfL)
#    nr = map(len, intup)
#    edges = [0]+np.cumsum(nr).tolist()
#
#    E = sum(intup, ())
#    F = [('fisher', x) for x in E]
#    G = reqobj.getObjs(F)
#
#    H = [G[i:j] for (i,j) in zip(edges[:-1], edges[1:])]
#    J = map(add_parts, H)
#
#    priors = sci.priors.mypriors()
#    for x in J:
#        x.priors = priors
#
#    return J
