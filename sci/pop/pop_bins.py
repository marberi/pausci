#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals

import copy
import ipdb
import numpy as np

from scipy.integrate import quad
from scipy.interpolate import splev, splrep, splint

class pop_bins(object):
    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, val):
        self._data[key] = val

    def read_bindata(self, meta, popid):
        """Redshift binning coming from observations."""

        # I will adapt to the convensions of "z_mean",
        # "z_width" gradually.

        res = meta[popid]
        z_mean, z_width = res['z_mean'], res['z_width']
        res['za'] = z_mean - 0.5*z_width
        res['zb'] = z_mean + 0.5*z_width

        return res

    def _width_uni(par, z, popid):
        """Bins with uniform width."""

        return par['pop', popid, 'zwidth']

    def _width_pz(par, z, popid):
        """Bins scaled with (1+z)."""

        return par['pop', popid, 'zwidth']*(1 + z)

    zbin_types = {'uniform': _width_uni, 'pz': _width_pz}

    def zedges_ordinary(self, par, pop_data, popid, f_width):
        """Edges of the z-bin array."""

        zmin = par['pop', popid, 'z0']
        zmax = par['pop', popid, 'zmax']

        tmp = []
        z = zmin

        edges = [zmin]
        while z < zmax:
           width = f_width(par, z, popid)
           assert 10**-5 < width, 'Artificially narrow redshift bins.'

           z += width
#           print(popid, z, width)
           if not zmax < z:
               edges.append(z)

        return np.array(edges)

    def zedges_onebin(self, par, pop_data, popid):
        """One redshift bin. Usefull when doing plots."""

        zmean = par['pop', popid, 'onebin_mean']
        zwidth = par['pop', popid, 'onebin_width']

        zedges = np.array([zmean-0.5*zwidth, zmean+0.5*zwidth])

        return zedges

    def OLD_zedges_nbins(self, par, pop_data, popid):
        """Calculate so nbins is falling exactly within the
           range.
        """

        # This code included a minor bug. The redshift bins was slightly shifted
        # with respect to their real position. Affected results in the thesis, but
        # was found before paper-II was finished.

        nbins = par['pop', popid, 'nbins']
        assert 1 <= nbins, 'nbins {0} is not a valid number of bins'.format(nbins)

        z0 = par['pop', popid, 'z0']
        zmax = par['pop', popid, 'zmax']

        wmax = (zmax - z0)/nbins
        winterp = np.linspace(0., wmax)
        zn = (1+winterp)**nbins*z0 -1 + (1+winterp)**(nbins-1)

        spl = splrep(zn, winterp)

        widthz0 = splev(zmax, spl)

        n = np.arange(nbins)
        zedges = (1+widthz0)**n*z0 - 1 + (1+widthz0)**(n-1)
        zedges[0] = z0

        return zedges


    def zedges_nbins(self, par, pop_data, popid):
        """Calculate so nbins is falling exactly within the
           range.
        """

        nbins = par['pop', popid, 'nbins']
        assert 1 <= nbins, 'nbins {0} is not a valid number of bins'.format(nbins)

        z0 = par['pop', popid, 'z0']
        zmax = par['pop', popid, 'zmax']


        r = (1+zmax)/(1+z0)
        w = -1 + r**(1./nbins)
        n = np.arange(nbins+1)
        edges = (1+z0)*(1+w)**n - 1

        return edges



    def create_bins(self, par, pop_data, popid):
        """Create the redshift bins."""

        if self.pkg:
            return self.read_bindata(self.pkg.meta, popid)

        zbin_type = self.conf['fid.pop.{0}.zbin_type'.format(popid)]
        if zbin_type in self.zbin_types:
            f_width = self.zbin_types[zbin_type]
            zedges = self.zedges_ordinary(par, pop_data, popid, f_width)
        elif zbin_type == 'onebin':
            zedges = self.zedges_onebin(par, pop_data, popid)
        elif zbin_type == 'nbins':
            zedges = self.zedges_nbins(par, pop_data, popid)
        else:
            raise ValueError('No such zbin type:', zbin_type)

        zbin_data = {\
          'z_width': zedges[1:] - zedges[:-1],
          'z_mean': 0.5*(zedges[1:] + zedges[:-1]),
          'za': zedges[:-1],
          'zb': zedges[1:],
          'nbins': len(zedges) - 1,
          'edges': zedges}

        return zbin_data

    def pop_photoz_standard(self, par, popid):
        """Splines to use when no photoz data is available."""

        z_interp = np.linspace(0., 2.)

        raise #photoz0 without population index???????
        photoz0 = par['photoz0']*np.ones(len(z_interp))
        photoz = par['photoz0']*(1.+z_interp)

        compl = np.ones(len(z_interp))

        pop_photoz_data = {}
        pop_photoz_data['photoz'] = splrep(z_interp, photoz)
        pop_photoz_data['photoz0'] = splrep(z_interp, photoz0)
        pop_photoz_data['compl'] = splrep(z_interp, compl)

        return pop_photoz_data
