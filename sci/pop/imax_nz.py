#!/usr/bin/env python
# encoding: UTF8

import pdb

import numpy as np
import scipy
from scipy.integrate import romberg
#from matplotlib import pyplot as plt

scipy.seterr(under='ignore')

def find_z0(i):
    if 22. <= i <= 23:
        z0 = 0.446*(i-23.) + 1.235
    elif 23 < i < 27:
        a = np.array([1.237, 1.691, -12.167, 43.591, -76.076, 72.567, -35.959, 7.289])
        j = np.arange(8)
        z0 = np.sum(a*((i-23.)/4.)**j)
    else:
        raise ValueError('The fit is only valid for 22. < i < 27.')

    return z0

def nz_i(z, i):
    # z0
    # u
    z0 = np.array(map(find_z0, i))
    u = np.clip(i-23., 0., np.inf)

    alpha,beta,c,d,gamma = 0.678, 5.606,0.581,1.851,1.464

    zp = np.outer(z, 1./z0)

#    u = 0
    T1 = zp**alpha
    T2 = np.exp(-zp**beta)
    T3 = c*u**d*np.exp(-zp**gamma)

    return T1*(T2 + T3)

def lum(i):
    A, ms, alpha =  1.48584027,  20.2952231 ,  -1.76230166
#    A = 4404.78
#    ms = 20.3
#    alpha = -1.76

    return A*(10**(-0.4*(i-ms)))**(alpha+1)*np.exp(-10**(-0.4*(i-ms)))


def Nz_lowi(z):
    """The Nz for bright magnitudes. Separate fit to the data."""

    A,z0,alpha,beta = 2.24,0.7254,0.604,2.63

    def n(x):
        zp = x/z0
        return zp**alpha*np.exp(-zp**beta)

    dens = romberg(lum, 18,22)/romberg(n, 0.01, 2.)

    return dens*n(z) 

def nz(z, imin, imax):
    i = np.linspace(imin, imax)

    z_int = np.linspace(0.,4., 40)
    dz = (np.max(z_int) - np.min(z_int))/len(z_int)
    amp = (nz_i(z_int, i).sum(axis=0)*dz)**-1.

    di = (imax-imin)/len(i)
    B = amp*nz_i(z,i)*lum(i)*di
    part1 = B.sum(axis=1)

    if False:
        pdb.set_trace()
        plt.plot(z, ans)
        plt.show()
        pdb.set_trace()

    part2 = Nz_lowi(z)

    if False:
        plt.plot(z, part1, label='Normal')
        plt.plot(z, part2, label='Low i')
        plt.legend()
        plt.show()

    return part1+part2
