#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np
from scipy.interpolate import splrep, interp1d
from scipy.integrate import simps
from scipy.stats import gaussian_kde

import sci
from sci.pop import bin_trans, pop_bins

class pop_pkg(pop_bins.pop_bins, bin_trans.thr_bintrans,
              object):
    """Population data based on the input in a package."""

    def __init__(self, conf, pkg, to_calc):
        self.conf = conf
        self.pkg = pkg

        assert self.pkg, 'Set opts.pkg to input a package.'
        par = conf.fid
        self._data = self.pop_data(par, to_calc)

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, val):
        self._data[key] = val

    def alpha(self, par, popid):
        """Magnification slopes."""

        slopes = self.pkg.slopes[popid]
        z = slopes['z']

        slope_spls = {}
        for ftype in self.pkg.meta['ftype']:
            y = slopes[ftype]

            slope_spls['{0}_spl'.format(ftype)] = splrep(z, y)

        return slope_spls

    def photoz_cat(self, par, popid):
        """N(z) splines for each redshift bin."""

        catnr = self.conf['fid.pop.{0}.catnr'.format(popid)] 
        cat = self.pkg.photoz['catalogs'][popid][catnr]
        z_s = cat['z_s']
        z_p = cat['z_p']

        meta = self.pkg.meta[popid]
        kde = []
        for za,zb in zip(meta['za'], meta['zb']):
            inds = np.logical_and(za < z_p, z_p < zb)
            kde.append(gaussian_kde(z_s[inds]))

        return kde

    def photoz_nz(self, par, popid):
        """Construct splines for the photoz nz."""

        catnr = self.conf['fid.pop.{0}.catnr'.format(popid)] 
        photoz_nz = self.pkg.photoz['nz'][popid]
        assert catnr < len(photoz_nz), 'catalog number out of range.'
        photoz_nz = photoz_nz[catnr]

        z = photoz_nz['z']
        nz_input = photoz_nz['nz']

        A = np.array([simps(x, z) for x in nz_input])
        assert not (A == 0).any(), 'Empty nz array.'
        nz = (nz_input.T / A).T

        # Conference hack.
#        pdb.set_trace()
        nz_spls = [interp1d(z, x, bounds_error=False, fill_value=0.) for x in nz]

        return nz_spls

    def photoz(self, par, popid):
        """Photoz which can be specified in different ways."""

        photoz = {}
        if 'catalogs' in self.pkg.photoz:
            photoz['cat_kde'] = self.photoz_cat(par, popid)
        elif 'nz' in self.pkg.photoz:
            photoz['nz_spls'] = self.photoz_nz(par, popid)

        return photoz

 
    def pop_data(self, par, to_calc):

        # When adjusting theoretical expectations to input data, one need to
        # write the values. If these are set it will fail.
        meta = self.pkg.meta.copy()
        for popid in meta['pop']:
            del meta[popid]

        res = {'meta': meta}
        for popid in res['meta']['pop']:
            pop_data = {}
            pop_data['popid'] = popid
            pop_data['popname'] = popid

            pop_data.update(self.create_bins(par, pop_data, popid))
            if self.conf['opts.use_lens']:
                pop_data['alpha'] = self.alpha(par, popid)

            if hasattr(self.pkg, 'photoz'):
                pop_data.update(self.photoz(par, popid))

            res[popid] = pop_data

        if hasattr(self.pkg, 'noise'):
            res['noise'] = self.pkg.noise

        if 'fid' in self.pkg.meta:
            res['fid'] = self.pkg.meta['fid']

        return res
