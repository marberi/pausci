#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals
import numpy as np
np.seterr(over='raise', divide='raise', invalid='raise', under='raise')

import os
import pdb
import sys
from scipy.interpolate import splev, splrep, splint, interp1d
from scipy.integrate import quad
import scipy

from sci.lib import libinterp
from sci.pop import bin_trans, pop_bins

import nz_theory

class pop_theory(pop_bins.pop_bins, bin_trans.thr_bintrans, 
                 nz_theory.nz_theory):
    """Defining populalation data for multiple populations.
       Theoretical photo-z results are used.
    """

    def __init__(self, conf, pkg, to_calc):
        self.conf = conf
        self.pkg = pkg
        par = self.conf.fid
       
        assert not self.pkg, 'Use pop_pkg instead when using input packages.'
        self._data = self.pop_data(par, to_calc)

        # A bit HACKerish..
        all_pop = 'faint' if 'faint' in self._data else 'bright' # HACK
        self._data['all'] = self._data[all_pop]


    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, val):
        self._data[key] = val

    def alpha(self, par, pop_data, popid):
        """Slope of the lensing signal."""

        z = pop_data['z_mean']

        alpha = {}
        for ftype in ['counts', 'mag', 'shear']:
            obs = (ftype, popid)

            spl = libinterp.find_spl(par, obs, 'a', 'slope_z')
            alpha[ftype] = splev(z, spl) 
            alpha["%s_spl" % ftype] = spl

        return {'alpha': alpha}

    def pop_photoz(self, par, popid):
        """Splines for the theoretical photo-z and the completeness."""
   
        z_interp = np.linspace(0., 2.)

#        pre = par['pop'][popid]['photoz_k']*par['pop'][popid]['photoz0']
#        photoz0 = pre*np.ones(len(z_interp))

        photoz0 = par['pop', popid, 'photoz_k']
        photozk = par['pop', popid, 'photoz0']
        photoz = photoz0*photozk*(1.+z_interp)

        A = par['pop',popid,'compl_frac']
        compl = A*np.ones(len(z_interp)) 

#        import pdb; pdb.set_trace()
        pop_photoz_data = {}
        pop_photoz_data['photoz'] = splrep(z_interp, photoz)
        pop_photoz_data['compl'] = splrep(z_interp, compl)

        return pop_photoz_data

    def dndz_spls(self, par, pop_data, popid):
        """Splines of dndz data."""

        spls = {}
        z_interp = np.linspace(0., 2.)
        res0 = self.dndz_all(par, z_interp, popid)
        spls['dndz_all'] = splrep(z_interp, res0)

        res1 = splev(z_interp, pop_data['compl'])*res0
        spls['dndz'] = splrep(z_interp, res1)

        return spls

    def ngal(self, par, pop_data):
        """Number of galaxies in each bin."""

        def int_nbin(zi):
            return splev(zi, dndz_spl)

        za = pop_data['za']
        zb = pop_data['zb']
        dndz_spl = pop_data['dndz']

        ngal_bin = []
        for zai, zbi in zip(za, zb):
            ngal_bin.append(quad(int_nbin, zai, zbi)[0])

        return np.array(ngal_bin)/(4*np.pi*par['f_sky'])


    def pop_avg_photoz(self, pop_data):
        """Average photoz in each bin."""

        za = pop_data['za']
        zb = pop_data['zb']

        avg_photoz = []

#        photoz0_spl = pop_data['photoz0']

        photoz_spl = pop_data['photoz']
        dndz_spl = pop_data['dndz']

        def int_avg_photoz(zi):
            photoz = splev(zi, photoz_spl)

            return splev(zi, dndz_spl)*photoz

        def int_nbin(zi):
            return splev(zi, dndz_spl)

        tot_photoz = []
        ngal_bin = []
        for zai, zbi in zip(za, zb):
            tot_photoz.append(quad(int_avg_photoz, zai, zbi)[0])
            ngal_bin.append(quad(int_nbin, zai, zbi)[0])

        tot_photoz = np.array(tot_photoz)
        ngal_bin = np.array(ngal_bin)

        return tot_photoz/ngal_bin

    def noise_pop(self, par, pop_data, popid):
        """Noise term to add to the correlations."""

        # Shape-noise expressions is divided on four instead of scaling 
        # convergence to shear.
        ngal = pop_data['ngal_obs']
        cgg_noise = np.diag(1./ngal)
        ckk_noise = par['err_ellip']**2*np.diag(1./(par['nie_fac']*ngal))
        ckk_noise = ckk_noise / 4.
        cmm_noise = par['pop', popid, 'err_mag']**2.*cgg_noise

        noise = {(('counts', popid), ('counts', popid)): cgg_noise,
                 (('shear', popid), ('shear', popid)): ckk_noise,
                 (('mag', popid), ('mag', popid)): cmm_noise}

        return noise

    def meta(self):
        meta = {'fourier': self.conf['opts.fourier'],
                'pop': self.conf['opts.pop'],
                'fid': self.conf.fid.export(),
                'isthr': True}

        return meta

    def nz(self, par, pop_data, popid):
        """Construct nz including change in density and photoz."""

        dndz_spl = pop_data['dndz']
        photoz_spl = pop_data['photoz']
        za,zb = pop_data['za'], pop_data['zb']

        scipy.seterr(under='ignore')

        # A bit random upper edge. Should be fine.
        z = np.linspace(0., zb[-1], 1000)
        h = z[1]-z[0]

        nz_spls = []
        for iza,izb in zip(za, zb):
            z_sub = np.linspace(iza, izb)
            nz_sub = splev(z_sub, dndz_spl)
            pz_sub = splev(z_sub, photoz_spl) 

            A = np.exp(-np.add.outer(z, -z_sub)**2/pz_sub**2)*nz_sub
            B = A.sum(axis=1)
            B = B/(h*np.sum(B))

            nz_spls.append(interp1d(z, B))

        return {'nz_spls': nz_spls}

    def pop_data(self, par, to_calc):
        """Find data for populations."""

        res = {'meta': self.meta()}
        for popid in res['meta']['pop']:

            pop_data = {}
            pop_data['popid'] = popid
            pop_data['popname'] = popid

            pop_data.update(self.pop_photoz(par, popid))
            pop_data.update(self.create_bins(par, pop_data, popid))
            pop_data.update(self.alpha(par, pop_data, popid))
            pop_data.update(self.dndz_spls(par, pop_data, popid))

            if not self.conf['opts.trans_mat']:
                pop_data.update(self.nz(par, pop_data, popid))

            # TODO: Move some of this into a function..
            pop_data['ngal_act'] = self.ngal(par, pop_data)
            pop_data['avg_photoz'] = self.pop_avg_photoz(pop_data)
            calc_trans = set(['trans', 'all']) & set(to_calc)
            use_trans = 'trans' in self.conf['opts.syst']

            # At times calculating transition takes more times, so I was
            # tired of waiting.            
            if calc_trans:
                pop_data['rel_trans'] = self.rel_trans(par, pop_data)
                pop_data['bin_trans'] = self.bin_trans(par, pop_data)

            if calc_trans and use_trans:
                ngal_act = pop_data['ngal_act']             
                ngal_obs = pop_data['bin_trans']*np.matrix(ngal_act).T
                pop_data['ngal_obs'] = np.array(ngal_obs.T)[0]

            if not use_trans:
                pop_data['ngal_obs'] = pop_data['ngal_act']

            res[popid] = pop_data

   #         pdb.set_trace()

        # TODO: To another function..
        noise = {}
        for popid in self.conf['opts.pop']:
            pop_data = res[popid]
            noise.update(self.noise_pop(par, pop_data, popid))

        res['noise'] = noise


        # TODO: To another function?
        if self.conf['opts.verbose']:
            for popid in self.conf['opts.pop']:
                print('\nz - %s:' % popid) #z
                print(res[popid]['z_mean'].tolist()) #z

        return res 

def main():
    pass

if __name__ == '__main__':
    main()
