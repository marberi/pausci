#!/usr/bin/env python
# encoding: UTF8

# Theoretical expressions for the migration matrix.
from __future__ import print_function, unicode_literals
import os
import sys
import numpy as np
import scipy

from scipy.integrate import quad
np.seterr(divide='raise')

class basic_bintrans(object):
    def rel_trans(self, par, pop_data):
        """Relative transition rate between bins."""

        # The only transition is within the populations.
        trans = self.bin_trans(par, pop_data)

        N = pop_data['ngal_act']
        z = pop_data['z_mean']
        nbins = len(z)

        r = np.zeros((nbins, nbins))
        d = np.array((trans*np.matrix(N).T).T)[0]

        assert (d != 0).all()
        for i in range(nbins):
            for j in range(nbins):
                r[i,j] = (trans[i,j] * N[j]) / d[i]

        return np.matrix(r)

class thr_bintrans(basic_bintrans, object):
    """Theoretical values of the transition matrix."""

    def EXPL_bin_trans_ana(self, par, pop_data):
        """Transfer probability between one bin and another."""

        # Keeping the explicit integration around both to test and
        # since it might be needed for non-gaussian transitions.
        from scipy.integrate import dblquad
        width = pop_data['width']
        za = pop_data['za']
        zb = pop_data['zb']

        z = pop_data['z']
        nbins = len(z)
        trans = np.zeros((nbins, nbins))

        photo_z = pop_data['avg_photoz']

        for i in range(nbins):
            a_i = 1./(np.sqrt(2*np.pi)*photo_z[i])
            f_i = -1./(2.*photo_z[i]**2.)
            for j in range(nbins):
                trans[i][j] = (a_i/width[j])*dblquad(lambda x, mu: np.exp(f_i*(x-mu)**2), za[i], zb[i], \
                                                        lambda _: za[j], lambda _: zb[j])[0]

        return trans

    def bin_trans(self, par, pop_data):
        """Transfer probability between one bin and another."""

        width = pop_data['z_width']
        z = pop_data['z_mean']

        nbins = len(z)
        trans = np.zeros((nbins, nbins))

        photo_z = pop_data['avg_photoz']

        if (photo_z == 0).all():
            return np.diag(np.ones(nbins))

        msg_zero = 'photoz equal 0 in some of the bins are not supported'
        assert (photo_z != 0).all(), msg_zero
        fact = 1. / (photo_z * np.sqrt(8.))

        np.seterr(under='ignore')
        erf = scipy.special.erf
        for i in range(nbins):
            for j in range(nbins):
                test = 15 < np.abs(z[j]-z[i])/photo_z[j]
                if test: continue

                def integrand(x):
                    return erf((2*x + width[i])*fact[j]) - \
                           erf((2*x - width[i])*fact[j])

                delta_ij = z[j] - z[i]

                lim_min = delta_ij - width[j] / 2.
                lim_max = delta_ij + width[j] / 2.
                pre = 1./(2.*width[j])


                trans[i][j] = pre*quad(integrand, lim_min, lim_max)[0]

        return np.matrix(trans)


