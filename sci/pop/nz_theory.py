#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np
from scipy.integrate import quad

import imax_nz

class nz_theory(object):
    def dndz_all_onefit(self, par, z, popid):

        A_for_frac = par['pop', popid, 'A_for_frac']
        z0_gal = par['pop', popid, 'z0_gal']
        alpha_gal = par['pop', popid, 'alpha_gal']
        beta_gal = par['pop', popid, 'beta_gal']


        def n(x):
            return (x/z0_gal)**alpha_gal * np.exp(-(x/z0_gal)**beta_gal)

#        pre2 = A_gal*par['f_sky']/A_for_frac #/(4*np.pi*par['f_sky'])

        # Intergrating for too high redshifts leads to underflow
        # in exp.
        dens_rad2 = quad(n, 0., 7.)[0]

        # I don't particulary like this prefactor. To be removed..
        blah = 4*np.pi*par['f_sky']
        if not self.conf['opts.use_galdens']:
            A_gal = par['pop', popid, 'A_gal']
            pre = A_gal*par['f_sky']/A_for_frac
            dens_gal = (np.pi/10800.)**2*pre*dens_rad2
            dens_gal = dens_gal / blah
        else:
            dens_gal = par['pop', popid, 'dens_gal']
            dens_gal *= blah
            pre = dens_gal*(10800./np.pi)**2 / dens_rad2

        return pre*n(z)

    def dndz_all(self, par, z, popid):
        if self.conf['opts.nz_onefit']:
            return self.dndz_all_onefit(par, z, popid)
        else:
            imin = 22.01
            imax = par['pop',popid,'imax']

            blah = 4*np.pi*par['f_sky']
            pre = blah*(10800./np.pi)**2
            ans = pre*imax_nz.nz(z, imin, imax)

#            pdb.set_trace()
            return ans
#            pdb.set_trace()
