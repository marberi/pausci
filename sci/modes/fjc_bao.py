#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np
from scipy.interpolate import splrep, splev, splint

import sci

def find_nz(za, zb):
    file_nz = '/Users/marberi/Downloads/bb0.2z.txt'
    zin, nlrg, nelg, zqso = np.loadtxt(file_nz, unpack=True)

    spl = splrep(zin, nlrg+nelg)

    ans = np.array([splint(za[i], zb[i], spl) for i in range(len(za))])
    ans = (180./np.pi)**2 * ans

    return ans

def fjc_bao(myconf):

    myconf['opts.pop'] = ['bright']
    myconf['fid.pop.bright.dens_gal'] = 1000. / 3600.
    conf = sci.libconf(myconf)
    wl = sci.probes.wl(conf)

    pop = conf['opts.pop']
    assert len(pop) == 1
    pop_name = pop[0]

    f = lambda x: x.format(pop_name)
    # Assign some values
    zmean = wl.pop[pop_name]['z_mean']
    za = wl.pop[pop_name]['za']
    zb = wl.pop[pop_name]['zb']
    photoz0 = conf[f('fid.pop.{0}.photoz0')]*np.ones(len(zmean))

    # Use the fiducial cosmology..
    par = conf['fid']

    bias = wl.bias.mean(par, ('counts', pop_name), zmean, False, True)

    wl.cosmo.cache_param(par)
    chiA = wl.cosmo.cache_ev(par, 'chi', za)
    chiB = wl.cosmo.cache_ev(par, 'chi', zb)

    # First, volume in a shell. Galaxies are in one stereo radian..
    vol = (4*np.pi/3)*(chiB**3 - chiA**3)
#    ngal = wl.pop[pop_name]['ngal_act']

    ngal = find_nz(za, zb)

    dens = 4.*np.pi*ngal/vol
#    kmax = wl.observ.kmax(zmean)
    kmax = 0.1*np.ones(len(zmean))


    txt = 'zmean  zmin  zmax den(mpc/h)  bias  kmax dz/(1+z)\n'
    fmt = 3*'%.3f '+'%.4e '+2*'%.3f '+'%.4f'
    res = [zmean, za, zb, dens, bias, kmax, photoz0]
    res = np.vstack(res).T
    f = open('cosmo_bao.input', 'w')
    f.write('nbins {0}\n'.format(str(len(zmean))))
    f.write('area {0}\n'.format(str(conf['fid.area'])))
    f.write(txt)

    np.savetxt(f, res, fmt=fmt)
    np.vstack(res)
