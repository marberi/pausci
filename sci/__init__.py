#!/usr/bin/enc pytohn
# encoding: UTF8

import sci.lib

def libconf(myconf={}, add_defaults=True):
    """Shorter name."""

    return sci.lib.libconf.conf(myconf,add_defaults)

setup = sci.lib.setup.setup
