#!/usr/bin/env python
# encoding: UTF8

# For only storing the chi2 values the HDF5 format borders
# being overkill. The format is already extensively user for
# Fisher matrices, Correlation functions and MCMC chaings. Also
# storing detections in HDF5 makes things more consistent, which
# further allows storing these together in one files. Further,
# the HDF5 is better when having to include meta data.

import os
import pdb
import tables

import numpy as np
from sci import config

from sci.lib.libh5attr import h5getattr, h5setattr

def load(file_name):
    """Load detections from HDF5 format."""

    try:
        fb = tables.openFile(file_name)
    except IOError:
        file_path = os.path.join(config.paths['cachedir'], 'detect', file_name)
        fb = tables.openFile(file_path)

    meta = fb.getNode('/detect/meta')
    conf_data = h5getattr(meta, 'conf')

    # The code from here differs from the one dealing with Fisher
    # matrices.
    data = {}
    for name, F in fb.getNode('/detect')._v_children.iteritems():
        if name == 'meta': continue

        data[name] = np.array(F.read())

    data['fourier'] = conf_data['opts']['fourier']
    fb.close()

    return conf_data, data

class store_hdf5(object):
    def save(self, file_name=''):
        """Save detection chi2 values in HDF5 format."""

        if not config.general['use_cache']:
            return

        file_name = file_name if file_name else self.conf.hashid()
        file_path = os.path.join(config.paths['cachedir'], 'detect' ,file_name)
        fb = tables.openFile(file_path, 'w')
        fb.createGroup('/', 'detect')

        meta = fb.createArray('/detect', 'meta', np.zeros(1))
        h5setattr(meta, 'conf', self.conf.data)

        fb.createArray('/detect', 'uvals', self.uvals)
        fb.createArray('/detect', 'chi2', self.chi2)

        fb.close()
