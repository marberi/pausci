#!/usr/bin/env python
# encoding: UTF8

import copy
import ipdb
import numpy as np

import sci
from sci.detect import store_hdf5

# Part of this could be considered overkill. Using a separate
# object for returning the data follow the same pattern as
# using the Fisher matrices.

class detect_cosmo(store_hdf5.store_hdf5):
    def __init__(self, conf, data):
        self.conf = conf

        for key, val in data.iteritems():
            setattr(self, key, val)

class estim_detect(store_hdf5.store_hdf5, object):
    def __init__(self, myconf):
        self.myconf = myconf
        self.conf = sci.libconf(myconf)

    def detect(self):
        data = self._estim_chi2(self.myconf)

        D_obj = detect_cosmo(self.conf, data)
        D_obj.save()

        return D_obj

    def _find_conf(self, myconf_base):
        assert 'opts.detect' in myconf_base
        detect = myconf_base['opts.detect']
        myconf_fid = copy.deepcopy(myconf_base)
        myconf_diff = copy.deepcopy(myconf_base)
        del myconf_fid['opts.detect']
        myconf_diff.update(myconf_diff['opts.detect'])
        del myconf_diff['opts.detect']

        conf_fid = sci.libconf(myconf_fid)
        conf_diff = sci.libconf(myconf_diff)

        return conf_fid, conf_diff

    def _estim_chi2(self, myconf_base):

        conf_fid, conf_diff = self._find_conf(myconf_base)
        fid_fid = conf_fid.fid
        fid_diff = conf_diff.fid

        # A bit confusing names..
        wl_fid = sci.probes.wl(conf_fid)
        wl_diff = sci.probes.wl(conf_diff)
        diffs_fid = {'none': (fid_fid, None)}
        diffs_diff = {'none': (fid_diff, None)}

        wl_fid.calc_values(fid_fid, diffs_fid)
        wl_fid.observD_hack(fid_fid)
        wl_diff.calc_values(fid_diff, diffs_diff)
        wl_diff.observD_hack(fid_diff)

        vec_fidI = wl_fid.deriv._obs_vector(fid_fid)
        vec_diffI = wl_diff.deriv._obs_vector(fid_diff)

        uvals = []
        chi2 = []
        for u,(corrD,v1),(_,v2) in zip(wl_fid.u_mean, vec_fidI, vec_diffI):
            print('u')
            nl = wl_fid.cosmo.N(u)
            observD = wl_fid.XobservD[u]

            cov_mat = wl_fid.cov(fid_fid, corrD['corr_obs'], observD, nl)

            if not len(cov_mat):
                continue

            diff = v2 - v1

            # Ok, this functionality should actually be elsewhere...
            if self.conf['opts.cov_diag']:
                cov_mat = np.diag(np.diag(cov_mat))
#                ipdb.set_trace()


            covI = np.linalg.pinv(cov_mat)
            uvals.append(u)
            chi2.append(np.dot(diff, np.dot(covI, diff)))

            print(chi2)

        d = {'uvals': np.array(uvals),
             'chi2': np.array(chi2),
             'fourier': conf_fid['opts.fourier']}

        return d
