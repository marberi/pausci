#!/usr/bin/env python
# Generate power spectrum with top-hat smoothing.
# The code in tk_eh98 is a port of Martin Whites c-code
# to estimate Fisher matrix constraint for multiple
# populations. In the file he states:
#
# """If used in publications, please cite White, Song & Percival (2008)"""

import copy
import pdb
import sys
import time
import numpy as np
from numpy import power as pow
from numpy import cos, exp, log, pi, sin, sqrt
from numpy.testing import assert_almost_equal
from scipy.integrate import quad
from scipy.interpolate import interp1d, splev, splrep

from sci.power import halofit

class eishu(halofit.halofit, object):
    """The Eisenstein-Hu power spectrum. Uses halofit for the non-linear
       contribution.
    """

    def __init__(self, conf):
        self.conf = conf
#        if self.conf['opts.use_nl']:
#            self.halofit = halofit.halofit()

    def tk_eh98(self, par, k):
        """Transfer function of Eisenstein & Hu 1998 (Equation numbers 
           refer to this paper)"""
    
        # set up cosmology
        h = par['h']
        om_m = par['om_m']
        om_b = par['om_b']
    
        # convert k to Mpc^-1 rather than hMpc^-1
        rk=k*h;
        hsq=h*h;
        om_mhsq=om_m*hsq;
    
        # constants
        e=exp(1.);      
        thet=2.728/2.7;
        thetsq=thet*thet;
        thetpf=thetsq*thetsq;
    
        # Equation 4 - redshift of drag epoch
        b1=0.313*pow(om_mhsq,-0.419)*(1.+0.607*pow(om_mhsq,0.674));
        b2=0.238*pow(om_mhsq,0.223);
        zd=1291.*(1.+b1*pow(om_b*hsq,b2))*pow(om_mhsq,0.251) \
          /(1.+0.659*pow(om_mhsq,0.828));
    
        # Equation 2 - redshift of matter-radiation equality
        ze=2.50e4*om_mhsq/thetpf;
    
        # value of R=(ratio of baryon-photon momentum density) at drag epoch
        rd=31500.*om_b*hsq/(thetpf*zd);
    
        # value of R=(ratio of baryon-photon momentum density) at epoch of
        # matter-radiation equality
        re=31500.*om_b*hsq/(thetpf*ze);
    
        # Equation 3 - scale of ptcle horizon at matter-radiation equality
        rke=7.46e-2*om_mhsq/(thetsq);
    
        # Equation 6 - sound horizon at drag epoch
        s=(2./3./rke)*sqrt(6./re)*log((sqrt(1.+rd)+sqrt(rd+re))/(1.+sqrt(re)));
    
        # Equation 7 - silk damping scale
        rks=1.6*pow(om_b*hsq,0.52)*pow(om_mhsq,0.73)*(1.+pow(10.4*om_mhsq,-0.95));
    
        # Equation 10  - define q
        q=rk/13.41/rke;
            
        # Equations 11 - CDM transfer function fits
        a1=pow(46.9*om_mhsq,0.670)*(1.+pow(32.1*om_mhsq,-0.532));
        a2=pow(12.0*om_mhsq,0.424)*(1.+pow(45.0*om_mhsq,-0.582));
        ac=pow(a1,(-om_b/om_m))*pow(a2,pow(-(om_b/om_m),3.));
    
        # Equations 12 - CDM transfer function fits
        b1=0.944/(1.+pow(458.*om_mhsq,-0.708));
        b2=pow(0.395*om_mhsq,-0.0266);
        bc=1./(1.+b1*(pow(1.-om_b/om_m,b2)-1.));
    
        # Equation 18
        f=1./(1.+pow(rk*s/5.4,4.));
    
        # Equation 20
        c1=14.2 + 386./(1.+69.9*pow(q,1.08));
        c2=14.2/ac + 386./(1.+69.9*pow(q,1.08));
    
        # Equation 17 - CDM transfer function
        tc=f*log(e+1.8*bc*q)/(log(e+1.8*bc*q)+c1*q*q) + \
          (1.-f)*log(e+1.8*bc*q)/(log(e+1.8*bc*q)+c2*q*q);
    
        # Equation 15
        y=(1.+ze)/(1.+zd);
        g=y*(-6.*sqrt(1.+y)+(2.+3.*y)*log((sqrt(1.+y)+1.)/(sqrt(1.+y)-1.)));
    
        # Equation 14
        ab=g*2.07*rke*s/pow(1.+rd,0.75);
    
        # Equation 23
        bn=8.41*pow(om_mhsq,0.435);
    
        # Equation 22
        ss=s/pow(1.+pow(bn/rk/s,3.),1./3.);
    
        # Equation 24
        bb=0.5+(om_b/om_m) + (3.-2.*om_b/om_m)*sqrt(pow(17.2*om_mhsq,2.)+1.);
    
        # Equations 19 & 21
        tb=log(e+1.8*q)/(log(e+1.8*q)+c1*q*q)/(1+pow(rk*s/5.2,2.));
        if not self.conf['opts.no_wiggles']:
            wiggle_part = sin(rk*ss)/rk/ss
        else:
            wiggle_part = pow(1. + pow(rk*ss, 4.), -1./4.)

        float_high_pres = getattr(np, 'float128') if hasattr(np, 'float128') \
                          else np.float96

#        rk = getattr(np, 'float128')(rk) if hasattr(np, 'float128') else np.float96(rk)
        rk = float_high_pres(rk)
        test = pow(rk/rks,1.4)
        lim = 9000.
        if np.iterable(test):
            test[lim < test] = lim
        else:
            test = lim if lim < test else test
    #            test = np.float128(test)
            test = float_high_pres(test)
    
    #        tb=(tb+ab*exp(-pow(rk/rks,1.4))/(1.+pow(bb/rk/s,3.)))*wiggle_part
        tb=(tb+ab*exp(-test)/(1.+pow(bb/rk/s,3.)))*wiggle_part
       
        # Equation 8
        tk_eh=(om_b/om_m)*tb+(1.-om_b/om_m)*tc;
        
        return tk_eh;

    def wind(self, k, R):
        """Window function."""
    
        # Top hat
        return (sin(k*R)/(k*R)**3-cos(k*R)/(k*R)**2)
    
    def xibar2(self, par, R):
        """Normalization of the power spectrum."""
    
        def sxibar2(k):
            #return (k**2)*self._power(par,k)*exp(-(k/par['wcut'])**2)*(self.wind(k, R))**2
            return (k**2)*self._power(par,k)*(self.wind(k, R))**2
    
        cons =  9./2./pi**2
        epsabs = 1.4899999999999999e-18
    
        ans = cons*quad(sxibar2, par['wmin'], par['wmax'], epsabs=epsabs)[0]
    
        return ans
    
    def cluster(self, par, R, use_pow=False):
#        file_name = "wmap5baosn_max_likelihood_matterpower.dat" 

        maxp1 = 200
        limit = 400
        from scipy.interpolate import splev, splrep

        if not use_pow:
            epsabs = 1.4899999999999999e-18
            k_intr = np.logspace(-5.,2,1000)
            tf_full = self.tk_eh98(par, k_intr)
            plin_intr = tf_full**2*k_intr**par['ns']*\
                        self.wind(k_intr, par['rf'])**2

            plin_spl = splrep(k_intr, plin_intr)

        else:
            k_intr, P_intr = np.loadtxt(use_pow, unpack=True)
            plin_spl = splrep(k_intr, P_intr)
            norm = 1.

        def sxibar2(k, Ri):
            x = k*Ri
            wind = (3./x)*(np.sin(x)/x**2. - np.cos(x)/x)

            return k**2.*(splev(k, plin_spl,ext=2)*wind**2.)/(2.*np.pi**2.)

#  Note: k^3 -> k^2 because Komatsu uses dlog(k).
#  win= (3d0/x)*(sin(x)/x**2d0-cos(x)/x)
#  ds2dk = k**3d0*(linear_pk(k)*win**2d0)/(2d0*pi**2d0)

        if not use_pow:
            epsabs = 1.4899999999999999e-18
            norm = par['sigma8']**2./ \
                   quad(sxibar2, par['wmin'], par['wmax'], epsabs=epsabs,\
                        args=(8.), maxp1=maxp1, limit=limit)[0]
 
        kmax = 20./R
        epsabs = 1.4899999999999999e-18

        ans = [quad(sxibar2, 1e-4, 6., args=(y), maxp1=maxp1,limit=limit)[0]\
               for x,y in zip(kmax,R)]

        ans = np.sqrt(norm*np.array(ans))

#        import pdb; pdb.set_trace()
        return ans

    def _power(self, par, k):
        """Power spectrum WITHOUT correct normalization."""
    
        #global amp
        if not hasattr(self, 'amp'):
            self.amp = 1.
 
        tf_full = self.tk_eh98(par, k)

        return tf_full**2*k**par['ns']*self.amp #*self.wind(k, par['rf'])**2

    def toint(self, lnk, R):
        k = np.exp(lnk)
        kR = k*R
        W = 3.*(np.sin(kR)/kR**3. - np.cos(kR)/kR**2.)

        return k**3*splev(k, self.pk_sigma)*W**2.

    def sigma_r(self, pk_spl, R):
        """NOTE: THIS IS USING A TOP-HAT FILTER."""

        # Not the best code in the world...

        t1 = time.time()
        self.pk_sigma = pk_spl # HACK
        pre = 1./(2*np.pi**2)
        sigma2 = []
        for ri in R:
            #sigma2.append(quad(self.toint, -4, 4, ri)[0]) 
            sigma2.append(quad(self.toint, -7, 10., ri)[0]) 

        sigma2 = pre*np.array(sigma2)

        t2 = time.time()

        print('time used integration', t2-t1)
        return np.sqrt(sigma2)


    amp_cache = {}
    def power(self, par, k, z):
        """Power spectrum with correct normalization."""

        #global amp
        key = (par['sigma8'], par['h'], par['om_m'], par['om_b'], par['wmin'], par['wmax'],
               par['wcut'])
    
        # Note: Side effects are used to find the amplitude.
        R = 8.
        if key in self.amp_cache:
            self.amp = self.amp_cache[key]
        else:
            self.amp = 1.
            self.amp = par['sigma8']**2/self.xibar2(par, R)
            self.amp_cache[key] = self.amp
   
        pk_lin = self._power(par, k)

        msg_passing = 'Currently it does not support using one and on value.'
        assert np.min(k) < 1e-4 and 1e2 < np.max(k), msg_passing


        if self.conf['opts.use_nl']:
            d2_spl = splrep(k, k**3*pk_lin/(2*np.pi**2))
            pk_lin, pk_nl = self.get_nl(par, pk_lin, k, z, d2_spl)

            return pk_lin, pk_nl
        else:
            return pk_lin

    __call__ = power
