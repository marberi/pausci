#!/usr/bin/env python
# encoding: UTF8

import pycamb
from scipy.interpolate import RectBivariateSpline
import numpy as np

# Interface for CAMB. Currently the code is not in
# use. Enabling it again require a good way to compile
# Fortran code.

# TODO: Find a better way to install Python programs.

class camb(object):
    nl_effects = False

    def __init__(self, opts, par):
        self.opts = opts

    def __call__(self, par, k, z, raise_error=True):
        input_camb = {'H0': par['h']*par['H_0'],
                      'omegab': par['om_b'],
                      'omegac':  par['om_m'] - par['om_b'],
                      'omegav': par['om_de'],
                      'scalar_index': par['ns'],
                      'scalar_amp': 2.1e-9}
#                      'w': par['w0']}
        
        # Setting the right amplitude.
        # http://cosmocoffee.info/viewtopic.php?t=475&sid=f1de7ce54932d23c46bf97866fa8fa35
        a = pycamb.transfers(**input_camb)
        input_camb['scalar_amp'] *= (par['sigma8']/a[2][0])**2
        a = pycamb.transfers(**input_camb)

        # CAMB expects highest redshift first.
        z_rev = z.copy()
        z_rev = list(z_rev)
        z_rev.reverse()
        input_camb['redshifts'] = z_rev

        a = pycamb.matter_power(**input_camb)
        kvals = a[0].T[0]
        ans = a[1]
        ans = np.fliplr(ans)

#        spl = scipy.interpolate.RectBivariateSpline(kvals, z, ans)
        spl = RectBivariateSpline(kvals, z, ans)

        if raise_error:
            raise ValueError

        return spl
