#!/usr/bin/env python
# encoding: UTF8

import numpy as np

class const(object):
    """Constant power spectrum. Only intended used for
       debugging.
    """

    nl_effects = False

    def __init__(self, conf):
        pass

    def __call__(self, par, k, raise_error=False):
        return np.ones(len(k))
