#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function, unicode_literals

import pdb
import sys
from scipy.interpolate import interp1d, splrep, splev, splint
import numpy as np

pre = 1./(2.*np.pi**2)

class halofit(object):
    """Non-linear contribution to the linear power spectrum."""

    def halofit_toint(self, lnk, R):
        k = np.exp(lnk)

        return splev(k, self.d2_spl)*np.exp(-(k*R)**2)

    def lnSig2(self,R):
        """Logarithm of sigma**2."""
        lnk = np.linspace(-4,4)
        k = np.exp(lnk)

        import scipy
        scipy.seterr(under='ignore')
        Y = splev(k, self.d2_spl)*np.exp(-(k*R)**2)

        from scipy.integrate import quad
        return np.log(quad(self.halofit_toint, -4, 4, R)[0])


    def find_spec(self, par, amp):
        """Find the effective spectral quantities."""
    
        lnR = np.linspace(np.log(0.1), np.log(10))

        lnSig2 = np.array(map(self.lnSig2, np.exp(lnR)))
        lnSig2_lnR_spl = splrep(lnR, lnSig2)
        lnR_lnSig2_spl = splrep(lnSig2[::-1], lnR[::-1])

        lnRopt = splev(-2*np.log(amp), lnR_lnSig2_spl)
        d1 = splev(lnRopt, lnSig2_lnR_spl, der=1)
        d2 = splev(lnRopt, lnSig2_lnR_spl, der=2)

        neff = -3 - d1
        C = -d2
        
        rknl = 1./ np.exp(lnRopt)

        return rknl, neff, C

    def halofit(self, y, C, ns, om_m, om_de, plin):
        """Halofit implementation."""
    
        gam = 0.86485+0.2989*ns+0.1631*C
        a = 1.4861+1.83693*ns+1.67618*ns*ns+0.7940*ns*ns*ns+ \
          0.1670756*ns*ns*ns*ns-0.620695*C
        a = 10**a
        b = 10**(0.9463+0.9466*ns+0.3084*ns*ns-0.940*C)
        c = 10**(-0.2807+0.6669*ns+0.3214*ns*ns-0.0793*C)
        xmu = 10**(-3.54419+0.19086*ns)
        xnu = 10**(0.95897+1.2857*ns)
        alpha = 1.38848+0.3701*ns-0.1452*ns*ns
        beta = 0.8291+0.9854*ns+0.3400*ns**2

        # 
        assert np.abs(1. - om_m).all() > .01
        if True:
#        if np.abs(1. - om_m) > .01:
            f1a = om_m**(-0.0732)
            f2a = om_m**(-0.1423)
            f3a = om_m**(0.0725)
            f1b = om_m**(-0.0307)
            f2b = om_m**(-0.0585)
            f3b = om_m**(0.0743)       
            frac = om_de/(1.-om_m) 
            f1 = frac*f1b + (1-frac)*f1a
            f2 = frac*f2b + (1-frac)*f2a
            f3 = frac*f3b + (1-frac)*f3a
        else:
            f1 = 1.0
            f2 = 1.
            f3 = 1.
    
        delta2_H_marked = a*y**(3*f1)/(1.+b*y**f2+(c*f3*y)**(3. - gam))
        delta2_H = delta2_H_marked / (1+xmu*y**-1+xnu*y**-2)
        delta2_Q = plin*(1.+plin)**beta/(1.+plin*alpha)*np.exp(-y/4.0-y**2/8.0)
    
        return delta2_H + delta2_Q

    def correct_nonlin(self, par, k, z):
        """Return the linear power spectrum and the non-linear halofit."""
  
        # Untill I manage to support arrays for k and z...
        assert isinstance(k, float) or len(k) == 1
        assert isinstance(z, float) or len(z) == 1

        om_m, om_de, HACK_fisk_er_godt = cosm(par, z)
        amp = self.D(par, z)
        ksigma, ns, C = self.find_spec(par, amp)
        y = k / ksigma
   
        raise 
        pk_lin = amp**2*self.power(par, k)
        delta2_lin = pre*k**3*pk_lin
        delta2_nl = self.halofit(y, C, ns, om_m, om_de, delta2_lin)
        pk_nl = delta2_nl / (pre*k**3)
    
        return pk_lin, pk_nl

    def delta2_Q(self, par, delta2_lin, y, n, C):
        w = par['w0']

        alpha_n = np.abs(6.0835 + 1.3373*n - 0.1959*n**2 - 5.5274*C)
        beta_n = 2.0379 - 0.7354*n + 0.3157*n**2 + 1.2490*n**3 + 0.3980*n**4 - 0.1682*C

        f_y = y/4. + y**2 / 8.

#        delta2_X = np.tile(delta2_lin, (len(n), 1)).T
#        pdb.set_trace()
        in_brackets = (1 + delta2_lin)**beta_n / (1 + alpha_n*delta2_lin)
        delta2_Q = delta2_lin * in_brackets * np.exp(-f_y)

        return delta2_Q

    def delta2_H(self, par, y, n, C, om_m, om_de):

        w = par['w0']

        # Equation A6 -> A13
        a_n = 10**(1.5222 + 2.8553*n + 2.3706*n**2 + 0.9903*n**3 + 0.2240*n**4 - \
                 0.6038*C + 0.1749*om_de*(1+w))

        b_n = 10**(-0.5642 + 0.5864*n + 0.5716*n**2 - 1.5474*C + 0.2279*om_de*(1+w))
        c_n = 10**(0.3698 + 2.0404*n + 0.8161*n**2 + 0.5869*C)
        gamma_n = 0.1971 - 0.0843*n + 0.8460*C
        mu_n = 0.
        nu_n = 10**(5.2105 + 3.6902*n)

        # Eq A14
        f1 = om_m**(-0.0307)
        f2 = om_m**(-0.0585)
        f3 = om_m**(0.0743)

        delta2_H_mark = (a_n*y**(3*f1))/(1 + b_n*y**f2 + (c_n*f3*y)**(3-gamma_n))
        delta2_H = delta2_H_mark / (1 + mu_n*y**-1 + nu_n*y**-2)

        return delta2_H

    def get_nl(self, par, pk_lin, k, z, d2_spl):
        """Return the linear power spectrum and the non-linear halofit."""
        if isinstance(z, float):
           z = np.array([z])

        self.d2_spl = d2_spl
        om_m = self.cosmo.cache_ev(par, 'om_m', z)
        om_de = self.cosmo.cache_ev(par, 'om_de', z)

        amp = self.cosmo.cache_ev(par, 'D', z)
        ksigma, neff, C = self.find_spec(par, amp)
   
        y = np.outer(k, 1./ksigma)

        # TODO: Improve this...
        delta2_X = pre*k**3*pk_lin
        delta2_lin = np.outer(delta2_X, amp**2)
 
#        pdb.set_trace() 
        delta2_Q = self.delta2_Q(par, delta2_lin, y, neff, C)
        delta2_H = self.delta2_H(par, y, neff, C, om_m, om_de) 

        delta2_nl = delta2_Q + delta2_H
        pk_nl = (delta2_nl.T / (pre*k**3)).T
   
        return pk_lin, pk_nl


#        mypk = (delta2_lin.T / (pre*k**3)).T
#        mypk_nl = (delta2_nl.T / (pre*k**3)).T
#
#        from matplotlib import pyplot as plt
#        for i in [0,1]:
#            plt.plot(k, mypk[:,i], label='Linear p(k), z = {0}'.format(z[i]))
#            plt.plot(k, mypk_nl[:,i], label='Halofit p(k), z = {0}'.format(z[i]))
#
#        plt.xlim(1e-4, 10)
#        plt.ylim(1e-1, 1e5)
#        plt.legend(loc=3)
#        plt.loglog()
#        plt.show()
##        self.mytest()
#        pdb.set_trace()
#    
#        pk_lin = amp**2*self.power(par, k)
#        delta2_lin = pre*k**3*pk_lin
#        delta2_nl = self.halofit(y, C, ns, om_m, om_de, delta2_lin)
#
#        pdb.set_trace()
#        pk_nl = delta2_nl / (pre*k**3)
#    
#        return pk_lin, pk_nl

