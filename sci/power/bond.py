#!/usr/bin/env python
# encoding: UTF8

import numpy as np

class bond(object):
    """The Bond power spectrum. Implemented only because the Halofit
       code included this by default.
    """

    nl_effects = False

    def __init__(self, conf):
        self.conf = conf

    def power_bond(self, par, k):
        """Bond & Efstathiou (1984) approximation to the linear CDM power spectrum. 
           Shamelessly copied from Smiths halofit code.
        """

        p_index = 1.
        rkeff = 0.172 + 0.011*np.log(par['gams']/0.36)*np.log(par['gams']/0.36)
        q = 1.e-20 + k/par['gams']
        q8 = 1.e-20 + rkeff/par['gams']
        tk = 1/(1 + (6.4*q + (3.0*q)**1.5 + (1.7*q)**2)**1.13)**(1/1.13)
        tk8 = 1/(1 + (6.4*q8 + (3.0*q8)**1.5 + (1.7*q8)**2)**1.13)**(1/1.13)

        return par['sigma8']**2*((q/q8)**(3.+p_index))*tk*tk/tk8/tk8
