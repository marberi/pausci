
# CODE FROM THE INTERNET. 

__all__=["camb","age","angular_diameter","matter_power","cls", "transfers"]
from numpy import zeros,repeat, array, float64
import sys
import _pycamb
_getcls = _pycamb.pycamb_mod.getcls
_getage = _pycamb.pycamb_mod.getage
_gentransfers = _pycamb.pycamb_mod.gentransfers
_freetransfers = _pycamb.pycamb_mod.freetransfers
_getpower = _pycamb.pycamb_mod.getpower
_freepower = _pycamb.pycamb_mod.freepower
_freecls = _pycamb.pycamb_mod.freecls
_angulardiameter = _pycamb.pycamb_mod.angulardiameter
_angulardiametervector = _pycamb.pycamb_mod.angulardiametervector

numericalParameters=['omegab', 'omegac', 'omegav', 'omegan', 'H0', 'TCMB', 'yhe', 'Num_Nu_massless', 'Num_Nu_massive', 'omegak', 'reion__redshift', 'reion__optical_depth', 'reion__fraction', 'reion__delta_redshift', 'Scalar_initial_condition', 'scalar_index', 'scalar_amp', 'scalar_running', 'tensor_index', 'tensor_ratio', '@AccuracyBoost', '@lAccuracyBoost', '@lSampleBoost', '@w_lam', '@cs2_lam']
logicalParameters=['WantScalars', 'WantTensors', 'reion__reionization', 'reion__use_optical_depth', '@w_perturb']
defaultValues={'scalar_amp': 2.1e-09, '@AccuracyBoost': 1.0, '@cs2_lam': 1.0, '@w_perturb': False, '@lSampleBoost': 1.0, '@lAccuracyBoost': 1.0, '@w_lam': -1.0}
np=len(numericalParameters)+len(logicalParameters)
nn=len(numericalParameters)
nm=len(numericalParameters)+len(logicalParameters)

def build_pvec(**parameters):
    pvec=repeat(-1.6375e30,np)
    input_params=list(parameters.keys())
    for n,p in enumerate(numericalParameters):
        pin=p
        if p.startswith('@'): pin=p.lstrip('@')
        if pin in input_params:
            pvec[n]=parameters[pin]
            input_params.remove(pin)
    for n,p in enumerate(logicalParameters):
        pin=p
        if p.startswith('@'): pin=p.lstrip('@')
        if pin in input_params:        
            if parameters[pin]:
                pvec[n+nn]=1.0
            else:
                pvec[n+nn]=0.0
            input_params.remove(pin)

    if input_params:
        print("WARNING: Unrecognized parameters:")
        for p in input_params:
            print(p)
    return pvec
    

def camb(lmax,**parameters):
    """
    Run camb up to the given lmax, with the given parameters and return the Cls.  Parameter names are case-insensitive.
    
    Any parameters that are not specified in the input are left with camb's default values,as given by the CAMB_SetDefParams subroutine in camb.f90.
    
    You can either specify parameters as keywords:
        cl = camb(1000,H0=72.0,omegab=0.04)
        
    Or using a dictionary:
        camb_params={"H0":72.0,"omegab":0.04}
        cl=camb(1000,**camb_params)
    
    The latter method gives you more flexibilty when writing codes.
    
    Valid parameters are all specified in the script that generated this code.  Most are elements of the CAMBparams derived type specified in modules.f90 .
    Parameters that are not in CAMBparams can be set by pre-pending an underscore.
    
    Parameters that are members of a sub-typewithin the CAMBparams type, (that is, all those accessed in fortran using CP%(something)%(varname)  ) such as the reionization parameters, should be given with the percent symbols replaced with a double underscore, __.
    
    For example, to specify the redshift of reionization, given by CP%reion%redshift in camb use:
        cl=camb(1000,reion__redshift=11.0)
    
    Boolean (logical) parameters can be specified as you would expect:
        cl=camb(1000,reionization=False)
        
    You can let more parameters be passed into camb by modifiying the top of, and running, generatePyCamb.py

    In this code, valid normal parameters are:
    omegab, omegac, omegav, omegan, H0, TCMB, yhe, Num_Nu_massless, Num_Nu_massive, omegak, reion__redshift, reion__optical_depth, reion__fraction, reion__delta_redshift, Scalar_initial_condition, scalar_index, scalar_amp, scalar_running, tensor_index, tensor_ratio, AccuracyBoost, lAccuracyBoost, lSampleBoost, w_lam, cs2_lam, WantScalars, WantTensors, reion__reionization, reion__use_optical_depth, w_perturb

    
    And parameters with default values are:
scalar_amp (2.100000e-09)
AccuracyBoost (1.000000e+00)
cs2_lam (1.000000e+00)
w_perturb (0.000000e+00)
lSampleBoost (1.000000e+00)
lAccuracyBoost (1.000000e+00)
w_lam (-1.000000e+00)
    
    
    Parameters which take a vector (like the primordial power spectrum parameters) are not yet properly implemented here, 
    so only one spectral index, amplitude, etc., at a time is possible.  Keyword parameters that are mapped to camb names are:
        scalar_index ---> InitPower__an(1)
    tensor_index ---> InitPower__ant(1)
    scalar_amp ---> InitPower__ScalarPowerAmp(1)
    scalar_running ---> InitPower__n_run(1)
    tensor_ratio ---> InitPower__rat(1)
    
    """
    pvec=build_pvec(**parameters)
    cls=_getcls(pvec,lmax)
    return cls.transpose()
    
    

def age(**parameters):
    """
Get the age of the unverse with the given parameters.  See the docstring for pycamb.camb for more info on parameters.
    """
    pvec=build_pvec(**parameters)
    age=_getage(pvec)
    return age
    

def transfers(redshifts=[0],**parameters):
    """
Generate transfer functions for the cosmology.  Specify the redshifts in DECREASING order (ie in temporal order).
The cosmological parameters that can be varied in the code are the same as for the 'camb' function - see help on that function for details
    """
    lmax=1000
    pvec=build_pvec(**parameters)
    redshifts = array(redshifts,dtype=float64)
    ordered_redshifts = redshifts.copy()
    ordered_redshifts.sort()
    ordered_redshifts=ordered_redshifts[::-1]
    if not (redshifts == ordered_redshifts).all(): sys.stderr.write("WARNING:  Re-ordered redshift vector to be in temporal order.  Ouput will be similarly re-ordered.\n")
    if len(redshifts)>500: raise ValueError("At most 500 redshifts can be computed without changing the hardcoded camb value")
    
    _gentransfers(pvec,lmax,ordered_redshifts)
    T = _pycamb.pycamb_mod.transfers.copy()
    K = _pycamb.pycamb_mod.transfers_k.copy()
    S = _pycamb.pycamb_mod.transfers_sigma8.copy()
    _freetransfers()
    return K,T,S


def matter_power(redshifts=[0], width=[1], bias=[3.1415], maxk=1.,logk_spacing=0.02,**parameters):
    """
    Generate matter power spectra for the cosmology.  Specify the redshifts in DECREASING order (ie in temporal order).
    You can also set the maximum k value and the spacing in log(k)
    The cosmological parameters that can be varied in the code are the same as for the 'camb' function - see help on that function for details
    """
    pvec=build_pvec(**parameters)
    redshifts = array(redshifts,dtype=float64)
    ordered_redshifts = redshifts.copy()
    ordered_redshifts.sort()
    ordered_redshifts=ordered_redshifts[::-1]
    if not (redshifts == ordered_redshifts).all(): sys.stderr.write("WARNING:  Re-ordered redshift vector to be in temporal order.  Ouput will be similarly re-ordered.\n")
    if len(redshifts)>500: raise ValueError("At most 500 redshifts can be computed without changing the hardcoded camb value")
    _getpower(pvec,maxk,logk_spacing,ordered_redshifts, width, bias)
    power=_pycamb.pycamb_mod.matter_power.copy()
    kh=_pycamb.pycamb_mod.matter_power_kh.copy()
    _freepower()
    return kh.squeeze(),power.squeeze()

def cls(redshifts=[0], width=[1], bias=[3.1415], maxk=1.,logk_spacing=0.02,**parameters):
    """
    Generate matter power spectra for the cosmology.  Specify the redshifts in DECREASING order (ie in temporal order).
    You can also set the maximum k value and the spacing in log(k)
    The cosmological parameters that can be varied in the code are the same as for the 'camb' function - see help on that function for details
    """
    pvec=build_pvec(**parameters)
    redshifts = array(redshifts,dtype=float64)
    ordered_redshifts = redshifts.copy()
    ordered_redshifts.sort()
    ordered_redshifts=ordered_redshifts[::-1]
    if not (redshifts == ordered_redshifts).all(): sys.stderr.write("WARNING:  Re-ordered redshift vector to be in temporal order.  Ouput will be similarly re-ordered.\n")
    if len(redshifts)>500: raise ValueError("At most 500 redshifts can be computed without changing the hardcoded camb value")
    _getcls(pvec,maxk,logk_spacing,ordered_redshifts, width, bias)

    cls=_pycamb.pycamb_mod.cls.copy()
    _freecls()

    return cls

def angular_diameter(z,**parameters):
    """
    Compute the angular diameter distance to the given redshift for the specified cosmology.
    You can also set the maximum k value and the spacing in log(k)
    The cosmological parameters that can be varied in the code are the same as for the 'camb' function - see help on that function for details
        """    
    pvec=build_pvec(**parameters)
    if isinstance(z,float) or len(z)==1:
        return _angulardiameter(pvec,z)
    redshifts = array(z,dtype=float64)
    ordered_redshifts = redshifts.copy()
    ordered_redshifts.sort()
    ordered_redshifts=ordered_redshifts[::-1]
    if not (redshifts == ordered_redshifts).all(): sys.stderr.write("WARNING:  Re-ordered redshift vector to be in temporal order.  Ouput will be similarly re-ordered.\n")
    return _angulardiametervector(pvec,ordered_redshifts)
        
    
    
