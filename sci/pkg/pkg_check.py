#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pdb

import sci
import pkg_calc

def comp_corr(corr_in, corr_new):
    assert set(corr_in.keys()) == set(corr_new.keys())

    for u in corr_in.keys():
        p1 = corr_in[u]
        p2 = corr_new[u]

        assert set(p1.keys()) == set(p2.keys()) 
        for key in p1.keys():
            assert (p1[key] == p2[key]).all()

def comp_cov(cov_in, cov_new):
    assert set(cov_in.keys()) == set(cov_new.keys())

    for u in cov_in.keys():
        p1 = cov_in[u]
        p2 = cov_new[u]

        assert set(p1.keys()) == set(p2.keys()) 
        for key in p1.keys():
            assert (p1[key] == p2[key]).all()

class pkg_check(pkg_calc.pkg_calc, object):
    def __init__(self, conf):
        self.conf = conf

    def check(self):
        pkg_file = self.conf['opts.pkg']
        assert pkg_file, 'No package file specified in opts.pkg'

        pkg = sci.pkg.PkgRead(pkg_file)
        corr_in = pkg.corr
        cov_in = pkg.cov

        wl,observD = self._create_wl_observD()
        corr_new = self._corr_calc(wl, observD)
        cov_new = self._cov_calc(wl, observD)

        comp_corr(corr_in, corr_new)
        comp_cov(cov_in, cov_new)
