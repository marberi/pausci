#!/usr/bin/env python
# encoding: UTF8

# Module for reading in 2D correlations. The HDF5 format
# is defined in doc/corr_format.

import ipdb
import json
import os
import pdb
import numpy as np
import tables
import pandas as pd
import sci
from sci.lib.libh5attr import h5getattr, h5setattr

# Pandas complains all the time.
import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)

class PkgRead(object): 
    def __init__(self, file_name, info={}, root= '/', only_open=False):
        """Initializes the input of correlations, covariances and metadata
           from the data.
        """
 
        self._info = info 

        file_name = os.path.expanduser(file_name)
        assert file_name, 'No package file specified.'

        self.store = pd.HDFStore(file_name, rootUEP=root)
        self.h5file = self.store._handle

        # Knowing the file name is useful when debugging.
        self._file_name = file_name
        if not only_open:
            self.read()

    def _test_meta(self, meta):
        """Test meta data consistency."""

        for pop_name in meta['pop']:
            msg_mean = 'Negative redshift in: ' + pop_name
            msg_width = 'Negative width in: ' + pop_name

            assert (0 <= meta[pop_name]['z_mean']).all(), msg_mean
            if 'z_width' in meta[pop_name]:
                assert (0 <= meta[pop_name]['z_width']).all(), msg_width
            if 'z_sigma' in meta[pop_name]:
                assert (0 <= meta[pop_name]['z_sigma']).all(), msg_width

    def _get_metapop(self, mpopObject):
        """Reads the metadata from the HDF5 file and store
           them in a dictionary.
        """
        
        metapop = {}

        attr_list = mpopObject.attrs._f_list('user')
        for attr in attr_list:
            metapop[attr] = h5getattr(mpopObject, attr)
            
        for attr in  ['z_mean', 'z_width']:
            msg = 'Missing metadata: '+attr
            assert hasattr(mpopObject.attrs, attr), msg
          
        metapop['nbins'] = len(metapop['z_mean']) 

        return metapop

    def _get_meta(self):
        """Reads the metadata from the HDF5 file and store
           them in a dictionary.
        """

        meta = {}
        mObject = self.h5file.get_node('/meta', 'meta')
        attr_list = mObject.attrs._f_list('user')

        for attr in ['fourier', 'pop']: #, 'ftype']:
            meta[attr] = h5getattr(mObject, attr)

        meta['ftype'] = ['counts'] # HACK

        key = 'l' if meta['fourier'] else 'ang'
        for part in ['mean', 'width']:
            from_key = '{0}_{1}'.format(key, part)
            to_key = 'u_{1}'.format(key, part)

            meta[to_key] = h5getattr(mObject, from_key)

        # TODO: Looks redundant.
        for attr in ['pop', 'fourier']:
            msg = 'Missing metadata: '+attr
            assert hasattr(mObject.attrs, attr), msg

        if hasattr(mObject.attrs, 'fid'):
            meta['fid'] = h5getattr(mObject, 'fid')

        if hasattr(mObject.attrs, 'config'):
            meta['conf'] = h5getattr(mObject, 'config')

        for i, pop_name in enumerate(meta['pop']):
            test_key = '/meta/{0}/meta'.format(pop_name)

            assert test_key in self.h5file, 'Missing group: {0}'.format(test_key)
            metapopObject = self.h5file.get_node('/meta/'+pop_name, 'meta')
            metapop = self._get_metapop(metapopObject)
            meta[pop_name] = metapop
 
        self._test_meta(meta)

        return meta

    def _get_corr(self, meta):
        """Reads the correlations from the HDF5 file and store
           them in a dictionary.
        """

        corr = dict((u, {}) for u in meta['u_mean'])
        corr_grp = self.h5file.get_node('/', 'corr')
        for corr_name, corrObj in corr_grp._v_children.iteritems():

            y = lambda x: h5getattr(corrObj, x)
            A = ((y('ftype0'), y('pop0')), (y('ftype1'), y('pop1')))
            attr_corr_list = corrObj.attrs._f_list('user')
            corrArray = corrObj.read()
            
#            for i in range (0,len(meta['pop'])):
#                pop_name = y('pop' + str(i))
            aShape = (len(meta['u_mean']), meta[y('pop0')]['nbins'], meta[y('pop1')]['nbins']) 
            msg = 'Wrong dimension of the correlation: ',corr_name ,'Expected: '\
                  ,str(list(aShape)),'Shape input: ',str(list(corrArray.shape))
              
            assert aShape == corrArray.shape, msg

            for i,u in enumerate(meta['u_mean']):
                corr[u][A] = corrArray[i]
                    
        return corr


    def _get_cov_dict(self, meta):
        """Reads the covariance matrices from the HDF5 file and store
           them in a dictionary.
        """    

        cov = dict((u, {}) for u in meta['u_mean'])
        for cov_name, covObj in self.h5file.get_node('/', 'cov')._v_children.iteritems():
            
            y = lambda x: h5getattr(covObj, x)
            A = ((y('ftype0'), y('pop0')), (y('ftype1'), y('pop1')), \
                 (y('ftype2'), y('pop2')), (y('ftype3'), y('pop3')))
           
            z = lambda i: meta[h5getattr(covObj, 'pop'+str(i))]['nbins'] 
            nbins = tuple([z(i) for i in range(4)])

            # There can either be 5 or 6 indexes.
            expected = lambda i: i*(len(meta['u_mean']),) + nbins

            covArray = covObj.read()
            msg_covdim = 'Wrong dimension of the covariance: {0}\nExpected: {1} or {2} \nInput: {3}'\
                         .format(cov_name, expected(1), expected(2), covArray.shape)

            assert covArray.shape in [expected(1), expected(2)], msg_covdim

            for i,u in enumerate(meta['u_mean']):
                cov[u][A] = covArray[i]

        return cov

    def _get_cov_array(self, meta):
        covD = {}
        for covObj in self.h5file.get_node('/', 'cov'):
            y = lambda x: h5getattr(covObj, x)
            A = ((y('ftype0'), y('pop0')), (y('ftype1'), y('pop1')), \
                 (y('ftype2'), y('pop2')), (y('ftype3'), y('pop3')))

            z = lambda i: meta[h5getattr(covObj, 'pop'+str(i))]['nbins']
            nbins = tuple([z(i) for i in range(4)])

            # There can either be 5 or 6 indexes.
            expected = lambda i: i*(len(meta['u_mean']),) + nbins

            # HACK, not yet testing for the shape...

            print('Starting to read covariance')
            covD[A] = covObj.read()
            print('Finished reading the covariance')

        return covD


    def _get_cov(self, meta):
        if meta['fourier']:
            return self._get_cov_dict(meta)
        else:
            return self._get_cov_array(meta)
        

    def _get_ngal(self, meta):
        """Reads the galaxy ditribution for each population
           from the HDF5 file and store them in a dictionary
        """

        # Note: This functionality has not been officially documented or
        #       even used. How we are going to specify n(z) is still an
        #       open question.
        ngal = {}
        for ngal_name, ngalObj in self.h5file.get_node('/', 'ngal')._v_children.iteritems():
            A = h5getattr(ngalObj, 'pop')
            ngal[A] = ngalObj.read()
            
        return ngal

    def _get_noise(self, meta):
        """Noise which needs to be added to the theoretical models of the correlations."""


        noise = {}
        for name, noiseObj in self.h5file.get_node('/noise')._v_children.iteritems():
            y = lambda x: h5getattr(noiseObj, x)

            ftype0, pop0 = y('ftype0'), y('pop0')
            ftype1, pop1 = y('ftype1'), y('pop1')
            A = ((ftype0, pop0), (ftype1, pop1))

            part = noiseObj.read()
            meta_shape = meta[pop0]['nbins'], meta[pop1]['nbins']

            msg = 'The noise for {0}.{1} and {2}.{3} is expected to be {4}.\nInput shape {5}.'
            assert meta_shape == part.shape, \
                   msg.format(ftype0, pop0, ftype1, pop1, meta_shape, part.shape)


            noise[A] = part

        return noise

    def _get_catalogs(self, meta):
        """Read in photo-z tables used to determine the n(z) for each bin."""

        msg_nottable = 'Galaxy catalogs should be tables.'
        msg_cols = 'Catalogs require the fields z_s and z_p'

        photoz = self.h5file.get_node('/photoz/catalog')
        catalogs = dict(((x,[]) for x in meta['pop']))
        for cat_name, catObj in photoz._v_children.iteritems():
            # TODO; Check photo-z is specified for all the populations..

            assert isinstance(catObj, tables.table.Table), msg_nottable
            colset = set(catObj.cols._v_colnames)
            assert set(['z_s', 'z_p']).issubset(colset), msg_cols

            z_s = catObj.read(field='z_s')
            z_p = catObj.read(field='z_p')

            pop_name = h5getattr(catObj, 'pop')
            catalogs[pop_name].append({'z_s': z_s, 'z_p': z_p})

        return catalogs

    def _get_nz(self, meta):
        """Static n(z). Sometime people like to define those."""

        nz_grp = self.h5file.get_node('/photoz/nz')
        nz_info = dict(((x,[]) for x in meta['pop']))
        for nz_name, grp in nz_grp._v_children.iteritems():
            z_path = '/photoz/nz/{0}/z'.format(nz_name)
            nz_path = '/photoz/nz/{0}/val'.format(nz_name)

            z_node = self.h5file.get_node(z_path)
            z = z_node.read()
            nz = self.h5file.get_node(nz_path).read()

            assert z.ndim == 1, 'Redshifts array should be an 1D array.'
            assert nz.ndim == 2, 'Redshifts array should be an 2D array.'
            assert len(z) == nz.shape[1], 'Different length of the z and nz array.'

            # The solution here is not the optimal...
            popid = h5getattr(z_node, 'pop')
            nz_info[popid].append({'z': z, 'nz': nz})

        return nz_info

    def _get_photoz(self, meta):
        """Read in the photo-z informations which can be specified in different
           ways.
        """

        photoz = {}
        if '/photoz/catalog' in self.h5file:
            photoz['catalogs'] = self._get_catalogs(meta)
        if '/photoz/nz' in self.h5file:
            photoz['nz'] = self._get_nz(meta)

        return photoz

    def _get_slopes(self, meta):
        """Slopes used for magnification."""

        slopes = {}
        for slope_table in self.h5file.get_node('/slopes')._v_children.itervalues():
            pop = h5getattr(slope_table, 'pop')
            assert pop in meta['pop'], 'Not a population:{0}'.format(pop)

# TODO: Check that all the slopes are included....
#            assert hasattr(slope_table.cols, 'z'), 'Missing z information.'

            slopes[pop] = slope_table.read()

        return slopes

#    def _get_err(self, meta):
#        """Error on all the observables."""
#        assert self.cov, 'The covariance is not read.'
#
#        yerr = []
#        for u in self.u_mean:
#            pdb.set_trace()
#            A = self.pkg.cov[u][obs+obs]
#            if A.ndim == 5:
#                yerr.append(np.sqrt(np.abs(A[u,i,j,i,j])))
#            else:
#                yerr.append(np.sqrt(np.abs(A[u,u,i,j,i,j])))
#
#        return np.array(yerr)

    def _get_err(self, meta):
#        self.h5file.get_node('/err')

        err = dict((u, {}) for u in meta['u_mean'])
#        err = {}
        nbins = lambda p: meta[y(p)]['nbins']
        for err_name, errObj in self.h5file.get_node('/', 'err')._v_children.iteritems():

            y = lambda x: h5getattr(errObj, x)
            A = ((y('ftype0'), y('pop0')), (y('ftype1'), y('pop1')))

            err_array = errObj.read()

            shape = (len(meta['u_mean']), nbins('pop0'), nbins('pop1'))
            assert err_array.shape == shape, 'Wrong array shape. Expected: {0}. Found: {1}'.format(shape, err_array.shape)

            for i,u in enumerate(meta['u_mean']):
                err[u][A] = err_array[i]
#            err[A] = err_array

        return err

    @property
    def err(self):
        if hasattr(self, '_err'):
            return self._err

        def toerror(i, D):
            
            find_auto = lambda X: (X[0] == X[2]) and (X[1] == X[3])
            usekeys = filter(find_auto, D.keys())

            sub_err = {} 
            for X in usekeys:
                obs0,obs1,obs2,obs3 = X
                tmp = D[X]
                tmp = tmp[i] if tmp.ndim == 5 else tmp

                # HACK: The following line include a bad HACK. It includes an
                # absolute value since Anne is sending negative values in the
                # covariance. This is to test the other parts...
                sub_err[obs0,obs1] = np.sqrt(np.abs(np.einsum('ijij->ij', tmp)))

            return sub_err

        err = {}
        for i, (u, data) in enumerate(self.cov.iteritems()):
            err[u] = toerror(i, data)

        return err

    def _get_jk(self, meta):
        """Read in the Jack-Knife sample."""

        jkD = {}
        keys = ['pop0', 'ftype0', 'pop1', 'ftype1']
        for jk_part in self.h5file.get_node('/jk'):
            f = lambda name: json.loads(getattr(jk_part.attrs, name))

            key = map(f, keys)
            jkD[tuple(key)] = jk_part.read()

        return jkD

    def _get_jk(self, meta):

#        ipdb.set_trace()
        return self.store['/jk/jk']
#        ipdb.set_trace()

    def _jk_convert(self, jkD):
        def todense(X):
            return np.where(X.mask, np.nan, X.data)

        corrD = {}
        covD = {}
        for key, val in jkD.iteritems():
            val_masked = np.ma.masked_array(val, np.isnan(val))

#            ipdb.set_trace()
            
            # HACK, I should insert a factor in the covariance to account for
            # the difference in area.
            corrD[key] = todense(val_masked.mean(axis=0))
            covD[key] = todense(val_masked.std(axis=0))

        return corrD, covD
 
    def _jk_convert(self, jk):

        corrD = {}
        covD = {}
        key = 'HACK'

        X1,X2,X3 = np.vstack(jk.index.values).T
        nth = int(np.max(X1))+1
        ni = int(np.max(X2))+1
        nj = int(np.max(X3))+1


        err_df = np.sqrt(jk.T.var())
        err = np.nan*np.ones((nth, ni, nj))

        for t,i,j in zip(X1, X2, X3):
            err[t,i,j] = err_df.ix[t,i,j]

        return err

    def read(self, read_extra=True):
        """Read in the data."""


        meta = self._get_meta()
        if '/corr' in self.h5file:
            self.corr =  self._get_corr(meta)

        if '/cov' in self.h5file and read_extra:
            self.cov = self._get_cov(meta)

        if '/err' in self.h5file and read_extra:
            self._err = self._get_err(meta)
 
        if '/ngal' in self.h5file and read_extra:
            self.ngal = self._get_ngal(meta)

        if '/noise' in self.h5file and read_extra:
            self.noise = self._get_noise(meta)

        if '/photoz' in self.h5file and read_extra:
            self.photoz = self._get_photoz(meta)

        if '/slopes' in self.h5file and read_extra:
            self.slopes = self._get_slopes(meta)

        # Test both if the file includes Jack-Knife samples and
        # ignore the ones Anne is creating.
        if '/jk' in self.h5file and '/jk/jk' in self.h5file:
            # This is conflicting with the error property..
            self.jk = self._get_jk(meta)
            tmp_err = self._jk_convert(self.jk)

            if not hasattr(self, '_err'):
                self._err = tmp_err

 
#        self.conf = sci.libconf(meta['conf'], False)
        self.meta = meta
        self.h5file.close()

    def save(self):
        # Same interface as for the Fisher matrices. Not in use...
        pass

class MetaRead(PkgRead, object): 
    def __init__(self, file_name, root = '/'):
        """Initializes the metadata
           from the data
        """

        self.h5file = tables.open_file(file_name, title = 'Metadata',
                                      rootUEP=root)

    def read(self):
        """Read in the meta part."""

        meta = self._get_meta()
        self.corr =  self._get_corr(meta)

        # Note: Need to be improved to handle group names without ending slash...
        if '/cov' in self.h5file:
            self.cov = self._get_cov(meta)
            
        if '/ngal' in self.h5file:
            self.ngal = self._get_ngal(meta)

        self.meta = meta

        self.store.close() 
        #h5file.close()


if __name__ == '__main__':
    main()
    
