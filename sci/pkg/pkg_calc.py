#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np

import sci

class pkg_calc(object):
    def _create_wl_observD(self):
        """The observations to output."""

#        ipdb.set_trace()

        # Ok, it is to complex to set up.
        fid = self.conf.fid
        l = 0
        diffs = {'none': (fid, 0)}
        wl = sci.probes.wl(self.conf)
        wl.calc_values(fid, diffs)

        # This object should support the old interface..
        observS = wl.observ._find_partypack(fid, exp_type='all')

        return wl, observS


    def _corr_calc(self, wl, observD):
        """Corr from conf."""

        fid = self.conf.fid
        u_mean = observD['u_mean']
        corr_type = self.conf['info.corr_type']
        nbins = dict((x,wl.pop[x]['nbins']) for x in observD['all_pop'])

        corr_res = {}
        for u in u_mean:
            corrD = wl.corr(fid, u, observD)
            corrD = wl.syst(fid, u, corrD)
            corr_res[u] = corrD[corr_type]

        return corr_res

    def _cov_calc(self, wl, observD):
        fid = self.conf.fid
        u_mean = observD['u_mean']
        corr_type = self.conf['info.corr_type']

        corr_type = 'corr_obs'
        nbins = dict((x,wl.pop[x]['nbins']) for x in observD['all_pop'])

        cov_2d = {}
        cov_4d = {}
        corr_iter = wl.corr.create_iter(fid, observD)
        for corrD in corr_iter:
            u = corrD['u']
#            if not corr_type == 'corr_act':
            corrD = wl.syst(fid, u, corrD)

            nl = wl.cosmo.N(u)
            cov_2d_u = wl.cov(fid, corrD[corr_type], observD, nl, True)
            cov_2d[u] = cov_2d_u
            cov_4d[u] = sci.cov.convert.to4d(nbins, cov_2d_u)

        return cov_2d, cov_4d
