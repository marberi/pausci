#!/usr/bin/env python
# encoding: UTF8

import json
from collections import OrderedDict
import itertools as it
import numpy as np


def tojson(header):
    """Convert header entries to json."""

    res = OrderedDict()
    for key, val in header.items():
        if isinstance(val, np.ndarray):
            val = val.tolist()

        res[key] = json.dumps(val)

    return res

def output_header(wl, observD, field, lvals):
    """Correlation file header."""

    header = OrderedDict()
    obs_cov = observD['obs_cov']

    header['obs_cov'] = obs_cov
    header[field] = lvals

    for pop in observD['all_pop']:
        for entry in ['z', 'width']:
            key = '%s.%s' % (pop, entry)

            header[key] = wl.pop[pop][entry]

    jsh = tojson(header)
    for key, val in jsh.items():
        print('# %s: %s' % (key, val)) #z


def output_corr(wl, observD, corr_key, uvals, fid):
    """Output the correlations."""

    obs_cov = observD['obs_cov']
    for u in uvals:
        corrD = wl.corr(fid, u, observD)


        res = []

        for obs1, obs2, f in obs_cov:
            res.append(corrD[corr_key][(obs1, obs2)].flatten())

        res = np.hstack(res)
        print(res.tolist()) #z


def write_corr(conf, meta, data):
    """Theory predictions."""

    conf['opts.umax_type'] = 'fixed'

    l = 0
    fid = conf['fid']
    diffs = {'none': (fid, 0)}
    wl = probe.wl(conf)
    wl.calc_values(fid, diffs)

    observD = wl.observ(fid, l)
    corr_key = 'corr_act' if conf['info.th_corr'] else 'corr_obs'

    if conf['opts.fourier']:
        uvals = wl.observ.ubins(fid)
        field = 'lvals'
    else:
        uvals, _ = wl.observ.ubins(fid) # yes..
        field = 'angles'

    output_header(wl, observD, field, uvals)
    output_corr(wl, observD, corr_key, uvals, fid)
