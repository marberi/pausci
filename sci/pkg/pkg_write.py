#!/usr/bin/env python
# encoding: UTF8

import json
import os
import ipdb
import sys
import time
import numpy as np
import itertools as it
from scipy.interpolate import splrep,splev,splint
import tables

import sci
import sci.config
import sci.probes
from sci.lib.libh5attr import h5getattr, h5setattr
import sci.pkg
from sci.pkg import pkg_calc


class PhotozCat(tables.IsDescription, object):
    """Table to save the mock galaxy catalogs."""

    z_s = tables.Float64Col(pos=0)
    z_p = tables.Float64Col(pos=1)

class pkg_write(pkg_calc.pkg_calc, object):
    def __init__(self, conf, out_path=None):
        self.conf = conf

        if not out_path:
            cachedir = sci.config.paths['cachedir']
            out_path = os.path.join(cachedir, 'pkg', conf.hashid())

        self.out_path = out_path
        self._fb = tables.open_file(out_path, 'w')


    def _add_meta(self, wl, observD):
        """Add meta data to HDF5 files."""

        self._fb.create_group('/', 'meta')
        meta = self._fb.create_array('/meta', 'meta', np.zeros(1))
       
        h5setattr(meta, 'ftype', observD['all_fluct'])
        for key, val in wl.pop['meta'].iteritems():
            h5setattr(meta, key, val)

        key = 'l' if wl.pop['meta']['fourier'] else 'ang'
        h5setattr(meta, '{0}_mean'.format(key), observD['u_mean'])
        h5setattr(meta, '{0}_width'.format(key), observD['u_width'])
        h5setattr(meta, 'config', wl.conf.data)

    def _add_meta_pop(self, wl, observD):
        """Add meta data to HDF5 files."""

        for i, pop_name in enumerate(wl.pop['meta']['pop']):
            group_pop = '/meta/'+pop_name
#            self._fb.create_group('/meta/', pop_name)
            meta_pop = self._fb.create_array(group_pop, 'meta', np.zeros(1),\
                                            createparents=True)
       
            # Redshift binning..
            pop = wl.pop[pop_name]
            h5setattr(meta_pop, 'z_mean', pop['z_mean'])
            h5setattr(meta_pop, 'z_width', pop['z_width'])

    def _add_corr(self, wl, observD):
        """Add the correlations to the output."""

        self._fb.create_group('/', 'corr')
        obs_cov = observD['obs_cov']
        obs_cov = observD['obs_cov']
        fid = self.conf.fid
        corr_type = self.conf['info.corr_type']

        res = {}
        corr_iter = wl.corr.create_iter(fid, observD)
        for corrD in corr_iter:
            u = corrD['u']
            if not corr_type == 'corr_act':
                corrD = wl.syst(fid, u, corrD)

            for obs1, obs2, _ in obs_cov:
                key = (obs1, obs2)

                if not key in res:
                    res[key] = []            
        
                res[key].append(corrD[corr_type][key])

        for i,(obs1,obs2,_) in enumerate(obs_cov):
            key = obs1,obs2

            # If you think this code is retarded I completely
            # agree. It failed with strange memory errors in a
            # non-pythonic way..

            to_stack = res[key]
            A = np.zeros((len(to_stack),) + to_stack[0].shape)
            for j, val in enumerate(to_stack):
                A[j] = val

#            ipdb.set_trace()

            corr = self._fb.create_array('/corr', 'corr'+str(i), A) 
            corr.attrs.ftype0 = json.dumps(obs1[0])
            corr.attrs.pop0 = json.dumps(obs1[1])
            corr.attrs.ftype1 = json.dumps(obs2[0])
            corr.attrs.pop1 = json.dumps(obs2[1])

    def _add_cov(self, wl, observD, cov_4d):
        """Add sections of the covariance to the HDF5 file."""

        self._fb.create_group('/', 'cov')

        # Changing the format.
        u_mean = observD['u_mean']
        cov_parts = cov_4d[u_mean[0]].keys()
        for i, part in enumerate(cov_parts):
            # Basically stacking over the first axis.
            B = np.vstack(np.expand_dims(cov_4d[u][part],axis=0) for u in u_mean)

            cov_array = self._fb.create_array('/cov', 'cov'+str(i), B)

            for j,obs in enumerate(part):
                setattr(getattr(cov_array, 'attrs'), 'ftype'+str(j), json.dumps(obs[0]))
                setattr(getattr(cov_array, 'attrs'), 'pop'+str(j), json.dumps(obs[1]))

    def OLD_add_err(self, wl, observD, cov_4d):
        """Add a section with errors. Done to save space."""

        # We have not yet implemented the correct covariance for w(theta). The current
        # expressions used are wrong.
        if not self.conf['opts.fourier']:
            return

        self._fb.create_group('/', 'err')

        u_mean = observD['u_mean']
        err_keys = cov_4d[u_mean[0]].keys()

        for i,key in enumerate(err_keys):
            obs0,obs1,obs2,obs3 = key
            if not (obs0 == obs2 and obs1 == obs3):
                continue

            cov_shape = cov_4d[u_mean[0]][key].shape
            errors = np.zeros((len(u_mean), cov_shape[0], cov_shape[1]))
            for j,u in enumerate(u_mean):
                tmp = np.einsum('ijij->ij', cov_4d[u][key])
                errors[j] = np.sqrt(tmp)

            err_array = self._fb.create_array('/err', 'err'+str(i), errors)
            for j,obs in enumerate([obs0,obs1]):
                setattr(getattr(err_array, 'attrs'), 'ftype'+str(j), json.dumps(obs[0]))
                setattr(getattr(err_array, 'attrs'), 'pop'+str(j), json.dumps(obs[1]))

    def _add_err(self, wl, observD, cov_2d):
        """Add a section with errors. Done to save space."""

        self._fb.create_group('/', 'err')

        u_mean = observD['u_mean']
        err_keys = cov_2d[u_mean[0]].keys()

        for i,key in enumerate(err_keys):
            obs0,obs1,obs2,obs3 = key
            if not (obs0 == obs2 and obs1 == obs3):
                continue

            nbins0 = wl.pop[obs0[1]]['nbins']
            nbins1 = wl.pop[obs1[1]]['nbins']
            cov_shape = cov_2d[u_mean[0]][key].shape
            errors = np.zeros((len(u_mean), nbins0, nbins1)) #cov_shape[0])) #, cov_shape[1]))

            for j,u in enumerate(u_mean):
                errors[j] = np.sqrt(np.diag(cov_2d[u][key])).reshape((nbins0, nbins1))

            err_array = self._fb.create_array('/err', 'err'+str(i), errors)
            for j,obs in enumerate([obs0,obs1]):
                setattr(getattr(err_array, 'attrs'), 'ftype'+str(j), json.dumps(obs[0]))
                setattr(getattr(err_array, 'attrs'), 'pop'+str(j), json.dumps(obs[1]))

    def _find_S(self, observD, umax):

        # Previously the call structure was more complicated..
        th_mean = observD['u_mean']
        th_width = observD['u_width']
#        tha = th_mean - 0.5*th_width
#        thb = th_mean + 0.5*th_width
#        th_edges = np.array(list(tha)+[thb[-1]])

        return sci.corr.libclconv.find_S(th_mean, th_width, umax)

    def _add_err_wtheta(self, wl, observD):

        #The code here includes more calculations than I usually like..
        self._fb.create_group('/', 'err')

        fid = self.conf.fid
        corr_iter = wl.corr.create_iter(fid, observD, return_cl=True)
#        corr_type = self.conf['info.corr_type']

        # Ok, I should introduce some other variable for handling this.
        corr_type = 'corr_obs'
        var,uvals = [],[]
        for corrD in corr_iter:
            u = corrD['u']
            corrD = wl.syst(fid, u, corrD)
            nl = wl.cosmo.N(u)
            var.append(wl.cov.var(fid, corrD[corr_type], observD, nl))
            uvals.append(u)

        # Yes, this code is not exactly efficient..
        lvals, S = self._find_S(observD, max(uvals))
        S2 = S*S
        for i, (obs0,obs1,part) in enumerate(observD['obs_cov']):
            nbins0 = wl.pop[obs0[1]]['nbins']
            nbins1 = wl.pop[obs1[1]]['nbins']

            obsx = obs0,obs1 
            var_in = np.vstack([x[obsx] for x in var]).T
            var_intrp = np.zeros((var_in.shape[0], len(lvals)))
            for j in range(var_in.shape[0]):
                spl = splrep(uvals, var_in[j])
                var_intrp[j] = splev(lvals, spl)

            var_theta_flat = np.dot(S2, var_intrp.T)
            ntheta = var_theta_flat.shape[0]
            errors = np.zeros((ntheta, nbins0, nbins1))
            for j in range(ntheta):
                errors[j] = np.sqrt(var_theta_flat[j].reshape((nbins0,nbins1)))

            err_array = self._fb.create_array('/err', 'err'+str(i), errors)
            for j,obs in enumerate([obs0,obs1]):
                setattr(getattr(err_array, 'attrs'), 'ftype'+str(j), json.dumps(obs[0]))
                setattr(getattr(err_array, 'attrs'), 'pop'+str(j), json.dumps(obs[1]))

    def _add_cov_wtheta(self, wl, observD):

        # This code is shamelessly copied from the variance calculations and then
        # modified....
        self._fb.create_group('/', 'cov')

        fid = self.conf.fid
        corr_iter = wl.corr.create_iter(fid, observD, return_cl=True)

        # Creadint the covariances...
        corr_type = 'corr_obs'
        cov,uvals = [],[]
        for corrD in corr_iter:
            u = corrD['u']
            corrD = wl.syst(fid, u, corrD)
            nl = wl.cosmo.N(u)
            cov.append(wl.cov.cov(fid, corrD[corr_type], observD, nl, use_pieces=True))
            uvals.append(u)

        # Dealing with indices...
        indsD = {}
        for key, val in observD['obs_exp'].iteritems():
            indsD[key] = np.array(val)
#            ipdb.set_trace()


        # Yes, this code is not exactly efficient..
        lvals, S = self._find_S(observD, max(uvals))
        S2 = S*S

        cov_iter = it.product(observD['obs_cov'], observD['obs_cov'])
        for i, ((obs0,obs1,part1), (obs2,obs3,part2)) in enumerate(cov_iter):
            nbins0 = wl.pop[obs0[1]]['nbins']
            nbins1 = wl.pop[obs1[1]]['nbins']
            nbins2 = wl.pop[obs2[1]]['nbins']
            nbins3 = wl.pop[obs3[1]]['nbins']

            obsx = obs0,obs1,obs2,obs3 
            cov_in = np.dstack([x[obsx] for x in cov]).T

            nth = S.shape[0]
            M = np.zeros((nth, nth, nbins0, nbins1, nbins2, nbins3))

            for k1, (i,j) in enumerate(observD['obs_exp'][obs0,obs1]):
                t1 = time.time()
                for k2, (m,n) in enumerate(observD['obs_exp'][obs2,obs3]):
                    spl = splrep(uvals, cov_in[:,k1,k2])
                    x = splev(lvals, spl)
                    y = np.dot(S*x, S.T)

                    M[:,:,i,j,m,n] = y


                t2 = time.time()
                print('Time storing...', t2-t1)

            cov_array = self._fb.create_array('/cov', 'cov'+str(i), M)
            for j,obs in enumerate([obs0,obs1,obs2,obs3]):
                setattr(getattr(cov_array, 'attrs'), 'ftype'+str(j), json.dumps(obs[0]))
                setattr(getattr(cov_array, 'attrs'), 'pop'+str(j), json.dumps(obs[1]))

            print('Finished writing covariance matrix...')

    def _add_cov_err(self, wl, observD):
        """Add the covariance and error section."""

        if wl.pop['meta']['fourier']:
            cov_2d, cov_4d = self._cov_calc(wl, observD)
            self._add_err(wl, observD, cov_2d)

            if self.conf['opts.add_cov']:
                self._add_cov(wl, observD, cov_4d)
        else:
            self._add_err_wtheta(wl, observD)

            if self.conf['opts.add_cov']:
                self._add_cov_wtheta(wl, observD) #, cov_4d)

    def _add_nz(self, wl, observD):
        """Add the number of galaxies in each bin. Only output if provided."""

        # Test quite explicitly if one of the population includes the number
        # of galaxies.
        test = set(['ngal_obs' in wl.pop[X] for X in wl.pop['meta']['pop']])
        if test == set([False]):
            return

        self._fb.create_group('/', 'ngal')
        for i,pop in enumerate(wl.pop['meta']['pop']):
            if not 'ngal_obs' in wl.pop[pop]:
                continue

            ngal = json.dumps(list(wl.pop[pop]['ngal_obs']))

            ngal_array = self._fb.create_array('/ngal', 'ngal'+str(i), ngal)
            ngal_array.attrs.pop = json.dumps(pop) 

    def _add_noise(self, wl, observD):

        self._fb.create_group('/', 'noise')
        for i,(key,noise) in enumerate(wl.pop['noise'].iteritems()):
            (ftype0, pop0), (ftype1, pop1) = key

            noiseObj = self._fb.create_array('/noise', 'noise'+str(i), noise)
            for var, val in [('ftype0', ftype0), ('pop0', pop0), \
                             ('ftype1', ftype1), ('pop1', pop1)]:

                h5setattr(noiseObj, var, val)

    def _photoz_pop(self, wl, pop_name):
        """Photoz sample random generated for population."""

        ngal = 100000
        pop = wl.pop[pop_name]
        photoz0 = wl.conf['fid.pop.{0}.photoz0'.format(pop_name)]
        dndz_spl = pop['dndz']
        zmin = np.min(pop['za'])
        zmax = np.max(pop['zb'])
        z_highres = np.linspace(zmin, zmax, 200) 

        dndz_peak = np.max(splev(z_highres, dndz_spl))
        A = splint(zmin, zmax, dndz_spl)
        rfac = 2.*dndz_peak / A

        catalogs = []
        for i in range(1):
            # Sampling the True position.
            z_s = zmin + np.random.random(rfac*ngal)*(zmax-zmin)
            y = splev(z_s, dndz_spl) / dndz_peak
            r1 = np.random.random(rfac*ngal)
            inds = r1 < y
            assert ngal < np.sum(inds), 'Too few generated galaxies.'

            r_gauss = np.random.normal(size=rfac*ngal)
            z_p = z_s + photoz0*(1+z_s)*r_gauss
            z_p = np.clip(z_p, 0., np.inf)

            catalogs.append([z_s[inds], z_p[inds]])

        return catalogs

    def _add_photoz(self, wl, observD):
        """Generate random sample of galaxies drawn from the theoretical 
           photo-z distribution.
        """

        for popid in observD['all_pop']:
            catalogs = self._photoz_pop(wl, popid)
            for i, cat in enumerate(catalogs):
                grp_name = '/photoz/catalog'
                name = '{0}_cat{1}'.format(popid, str(i))
                table = self._fb.createTable(grp_name, name, PhotozCat, \
                                             createparents=True)

                table.append(np.vstack(cat).T)
                h5setattr(table, 'pop', popid)

    def _add_slopes(self, wl, observD):
        """Add high resolutions version of the magnification slope for counts,
           magnitudes and sizes.
        """

        self._fb.create_group('/', 'slopes')
        entries = ['z'] + observD['all_fluct']
        descr = dict((x,tables.Float64Col(pos=i)) for i,x in enumerate(entries))

        for pop_name in observD['all_pop']:
            pop = wl.pop[pop_name]
            zmin = np.min(pop['za'])
            zmax = np.min(pop['zb'])

            # Going from low to high resolution splines is slightly weird, but makes
            # the output look like measurements.
            z = np.linspace(zmin, zmax)
            totable = []
            totable.append(z)
            for obs in observD['all_fluct']:
                spl = pop['alpha']['{0}_spl'.format(obs)]
                totable.append(splev(z, spl))

            table = self._fb.createTable('/slopes', pop_name, descr)
            h5setattr(table, 'pop', pop_name) 
            table.append(np.vstack(totable).T)

    def __call__(self):
        """Theory predictions."""

        wl,observD = self._create_wl_observD()
        self._add_meta(wl, observD)
        self._add_meta_pop(wl, observD)
        self._add_corr(wl, observD)
        self._add_cov_err(wl, observD)

        # Temporary disabled since not existing in Annes files.
        self._add_nz(wl, observD)
        self._add_noise(wl, observD)
        if not ((not self.conf['opts.use_data'] and self.conf['opts.pkg'])):
            self._add_photoz(wl, observD)
            self._add_slopes(wl, observD)

        self._fb.close()

        # Not exactly pretty.
        return sci.pkg.PkgRead(self.out_path)


class meta_write(pkg_write, object):
    def __call__(self):
        """Theory predictions."""

        wl,observD = self._create_wl_observD()
        self._add_meta(wl, observD)
        self._add_meta_pop(wl, observD)

        self._fb.close()
