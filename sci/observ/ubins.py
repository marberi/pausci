#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np

# Defines which l-values and angles to use. This module
# has some inconsistent definitions and not everything is
# implemented.

class umax_basic(object):
    def kmax(self, z):
        a = self.conf['opts.kmax_a']
        b = self.conf['opts.kmax_b']

        return np.exp(a + b*z)

    def ubins(self, fid):
        if self.pkg:
            meta = self.pkg.meta
            return meta['u_mean'], meta['u_width']
        if self.pop['meta']['fourier']:
            return self.l_bins(fid)
        else:
            return self.angle_bins(fid)

    def ulim(self, fid, z):
        if self.pop['meta']['fourier']:
            return self.lmax(fid, z)
        else:
            return self.angle_min(fid, z)

    def angle_bins(self, fid):
        """Mean and width of the w(theta) theta bins."""

        ang_min = self.conf['opts.ang_min']
        ang_max = self.conf['opts.ang_max']
        dang = self.conf['opts.dang']

        edges = np.arange(ang_min, ang_max, dang)

        ang = .5*(edges[:-1] + edges[1:])
        ang_width = edges[1:] - edges[:-1]

        return ang, ang_width

    def l_bins(self, fid):
        """Mean and with of the Cl(l) bins."""

        lmin = fid['lmin']
        z_pop = [self.pop[popid]['z_mean'] for popid in self.pop['meta']['pop']]
        lmax_pop = [np.max(self.lmax(fid, z)) for z in z_pop]
        lmax = np.max(lmax_pop)

        # Require integer steps in l.
        lstep = 1./fid['f_sky']
        lstep = int(np.round(lstep))

        opts = self.conf['opts']
        if 'scale_area' in opts and opts['scale_area']:
            lstep = lstep*fid['area'] / opts['scale_to']

        lvals = np.arange(lmin, lmax, lstep).astype(np.int)

        # In addition use an lmax of 1500.
        lvals = lvals[lvals < self.conf['opts.lmax']]

        return lvals, lstep*np.ones(len(lvals))


class umax_nonlin(umax_basic, object):
    """lmax defind to avoid non-linear scales."""

    def angle_min(self, fid, z):

        # HACK: Only done to compare the predictions for two codes.
        return np.zeros(len(z))


    def lmax(self, fid, z):
        """Maximum value for l for a given redshift."""

        kmax = self.kmax(z)
        r = self.cosmo.cache_ev(fid, 'r', z)

        lmax = kmax * r

        return lmax


class umax_fixed(umax_basic, object):
    def angle_min(self, fid, z):
        """Minimum angle for a redshift."""

        return np.zeros((len(z)))


    def lmax(self, fid, z):
        """Maximum value of l for a specific redshift."""

        VERY_HIGH_L_VALUE = 10000.
        return VERY_HIGH_L_VALUE*np.ones(len(z))

