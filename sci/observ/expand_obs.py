#!/usr/bin/env python
# encoding: UTF8

#Expand the higher level definition of observations
# into a lower level definition.
import ipdb
import numpy as np
import pandas as pd

class expand_obs(object):
    def _find_pairs(self, pop1, pop2, pair_type):
        n1 = self.pop[pop1]['nbins']
        n2 = self.pop[pop2]['nbins']

        if pop1 != pop2 and pair_type == 'cross':
            raise ValueError('Cross should only be used between the same population.')

        if pair_type == 'all':
            pairs = []
            for i in range(n1):
                for j in range(i, n2):
                    pairs.append((i,j))
        elif pair_type == 'abs_all':
            pairs = []
            for i in range(n1):
                for j in range(n2):
                    pairs.append((i,j))
        elif pair_type == 'auto':
            assert n1 == n2, "Depends on the same binning for the lower redshifts."

            assert self.conf['opts']['assume_bin_hack']
            pairs = [(i,i) for i in range(min(n1, n2))]
        elif pair_type == 'cross':
            pairs = []
            for i in range(n1): #[1:]:
                for j in range(i, n2): # HACK
                    pairs.append((i,j))
        elif pair_type == 'onlycross':
            pairs = []
            for i in range(n1): #[1:]:
                for j in range(i+1, n2): # HACK
                    pairs.append((i,j))

#            for i in range(n1):
#                for j in range(i+1, n2): # HACK
#                    pairs.append((i,j))
        elif pair_type == 'none':
            pairs = []

        return pairs

    def use_pairs(self, obs1, obs2, m):
        """Which pairs between two populations to use. This
           function defines which correlations will be used.
        """

        ftype1, pop1 = obs1
        ftype2, pop2 = obs2
        pairs = self._find_pairs(pop1, pop2, m[0])
        if not pairs:
            return pairs

        xa = self.conf['opts.cross_lower'] 
        xb = self.conf['opts.cross_upper'] 
        if ftype1 != ftype2:
            xa = max(xa, self.conf['opts.fcross_lower'])
            xb = min(self.conf['opts.fcross_upper'], xb)

        if ftype1 == ftype2 == 'counts':
            xb = min(self.conf['opts.clust_zmax'], xb)

        z1 = self.pop[pop1]['z_mean']
        z2 = self.pop[pop2]['z_mean']

        i,j = zip(*pairs)
        diff = np.abs(z1[np.array(i)] - z2[np.array(j)])
        inds = np.logical_and(xa <= diff, diff <= xb)

        sel_pairs = [pairs[x] for x in inds.nonzero()[0]]

        return sel_pairs

    def all_fluct_pop(self, obs_cov):
        """Find all fluctuations and populations used."""

        all_obs = sum([[x[0], x[1]] for x in obs_cov], [])

        all_fluct, all_pop = zip(*all_obs)

        all_fluct = list(set(all_fluct))
        all_fluct.sort()

        all_pop = list(set(all_pop))
        all_pop.sort()

        return all_fluct, all_pop

    def to_remove(self, fid, u, all_pop):
        """Redshift bins to remove."""
   
        # Remove because of non-linear scales...
        ufourier = self.pop['meta']['fourier']
        to_remove = {}
        for pop in all_pop:
            z = self.pop[pop]['z_mean']

            ulim = self.ulim(fid, z)
#            to_remove[pop] = np.nonzero(lmax < l)[0]

#            pdb.set_trace()
            if ufourier:
                rem_nonlin = np.nonzero(ulim < u)[0].tolist()
            else:
                rem_nonlin = np.nonzero(u < ulim)[0].tolist()

            excl_bins = self.conf['fid.pop.%s.excl_bins' % pop]
            all_bins = range(len(z))

            to_rem = set(excl_bins) | set(rem_nonlin)
            to_rem = list(to_rem)
            to_rem.sort()
           
            to_remove[pop] = np.array(to_rem)
 
#            pdb.set_trace()

        return to_remove

    def obs_exp(self, fid, u, obs_cov, all_pop):
        """Find which of the observables that are used."""

        to_remove = self.to_remove(fid, u, all_pop)

        # TODO: This is the right place to add some functionality for not
        #       including specific observable.

        ans = {}

        # Code for removing different parts which should not be included.
        if self.conf['opts.lmax_clust'] < u:
            obs_cov_trimmed = []
            rem_cross = self.conf['opts.clmax_cross']

            for part in obs_cov:
                (ftype0, pop0), (ftype1, pop1), m = part

                if ftype0 == ftype1 == 'counts':
                    continue

#                ipdb.set_trace()
                if [ftype0,ftype1].count('counts') == 1 and rem_cross:
                    continue

#                if set([ftype0,ftype1]) == set(['counts']) and rem_cross:
#                    continue

                obs_cov_trimmed.append(part)
        else:
            obs_cov_trimmed = obs_cov

        print('in expand_obs')
        for obs1,obs2,m in obs_cov_trimmed:
            ftype1, pop1 = obs1
            ftype2, pop2 = obs2

            to_rem1 = to_remove[pop1]
            to_rem2 = to_remove[pop2]

            pairs = self.use_pairs(obs1, obs2, m)
            pairs_after = [(x,y) for x,y in pairs if not \
                           (x in to_rem1 or y in to_rem2)]

            ans[(obs1, obs2)] = pairs_after

#        if self.conf['opts.pop'] == ['bright']:
#            ipdb.set_trace()
#        if self.conf['opts.lmax_clust'] < u:
#            ipdb.set_trace()

        return ans

    def sel_obs(self, corrD, observD):
        """Returns a vector with the right observables to use.
        """

        obs_cov = observD['obs_cov']
        obs_expD = observD['obs_exp']

        res = []
        for obs1,obs2,m in obs_cov:
            key = (obs1,obs2)
            if not key in obs_expD:
                continue

            obs_exp = obs_expD[key]
            if not obs_exp:
                res.append(np.array([]))
                continue

            x,y = zip(*obs_exp)
            res.append(np.array(corrD['corr_obs'][key])[x,y])

        obs = np.hstack(res) if res else np.array([])
   
#        if self.conf['opts.lmax_clust'] < u:
        if False:
            shear = (('shear', 'faint'), ('shear', 'faint'))
            R =  np.diag(corrD['corr_obs'][shear]) / np.diag(corrD['corr_act'][shear])
            print('DEBUG', corrD['u'], R.tolist())
#        print('nexp', len(observD['obs_exp'].keys()), observD['obs_exp'].keys())
#        ipdb.set_trace() 

        return obs

    def sel_obs_tmp(self, corrD, inds):
        # Selecting on the value itself is not good...
        sub = inds[inds.u == corrD['u']]
        assert len(sub), 'Did something go wrong?'

        df = pd.DataFrame()
        obsnr = sub.obsnr
        for key, val in sub.groupby(['ftype0', 'pop0', 'ftype1', 'pop1']):
            ckey = (key[0], key[1]), (key[2], key[3])
            C = corrD['corr_obs'][ckey]

            df = df.append(pd.DataFrame({'obsnr': sub.obsnr, 'corr': C[val.i, val.j]}),
                           ignore_index=True)

        return df

