#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pdb
import sys
import pandas as pd
import numpy as np
import itertools as it

from sci.observ.ubins import umax_nonlin, umax_fixed
from sci.observ import expand_obs
from sci.observ import obs_def


d = {'nonlin': umax_nonlin,
     'fixed': umax_fixed}

class Observ(object):
    def _cache_obsexp(self, df):
        res = {}
        for u, val in df.groupby('u_mean'):
            res_u = {}
            for key, val2 in val.groupby(['ftype0', 'ftype1', 'pop0', 'pop1']):
                ftype0,ftype1,pop0,pop1 = key

                if not len(val2): continue

                pairs = zip(val2.i, val2.j) 

                obs0 = ftype0,pop0
                obs1 = ftype1,pop1

                res_u[obs0, obs1] = pairs

            res[u] = res_u

        return res

    def __init__(self, df, all_fluct, all_pop, obs_cov, u_mean, u_width, obs_exp=False):
        self.df = df

#        ipdb.set_trace()

        self._compat = {'all_fluct': all_fluct, 'all_pop': all_pop,
                        'obs_cov': obs_cov, 'u_mean': u_mean, 'u_width': u_width}

        if obs_exp:
            self._compat['obs_exp'] = obs_exp
        else:
            self._obsexp = self._cache_obsexp(df)

    def static_observD(self):

#        observD = {'obs_cov': obs_cov, 'all_fluct': all_fluct,
#                   'all_pop': all_pop, 'obs_exp': obs_exp,
#                   'u_mean': u_mean, 'u_width': u_width}

        # Not quite sure what to return. See where it fails.
        keys = ['all_fluct', 'all_pop']
        res = dict([(x, self._compat[x]) for x in keys])

        return res

    def observD(self, u):
        res = {}
        for key in ['obs_cov', 'all_fluct', 'all_pop']:
            res[key] = self._compat[key]

        # Not really sure if we should return an answer if it does not work!
        res['obs_exp'] = self._obsexp[u] if u in self._obsexp else \
                         np.array([])
            
        return res

    def obsexp(self, u):
        return self._obsexp[u]

    def ubins(self):
        u_mean = self._compat['u_mean']
        u_width = self._compat['u_width']

        return u_mean, u_width

    def __getitem__(self, key):
        if key in self._compat:
            return self._compat[key]

        if key == 'df':
            return self.df

        # This could be merged with the general dictionary??
        if key == 'obs_exp':
            return self._obsexp
#            ipdb.set_trace()

        raise NotImplementedError, key

class observ_combined(object):
    """Collects different ways of expressing the observables
       in one dictionary.
    """

#    def _product(self, obs_cov, u_mean, u_width, obs_exp):
#        # There should *not* be a product here!!!
#        nu = len(u_mean)
#        df = pd.DataFrame()
#        for obs0,obs1,parts in obs_cov:
#            key = obs0,obs1
#
#            I_l,J_l = zip(*obs_exp[key])
#            nobs = len(I_l)
#            I = np.repeat(I_l, nu)
#            J = np.repeat(J_l, nu)
#            F = pd.DataFrame({'i': np.repeat(I_l, nu),
#                              'j': np.repeat(J_l, nu),
#                              'ftype0': nu*nobs*[obs0[0]],
#                              'pop0': nu*nobs*[obs0[1]],
#                              'ftype1': nu*nobs*[obs1[0]],
#                              'pop1': nu*nobs*[obs1[1]],
#                              'u_mean': np.tile(u_mean, nobs),
#                              'u_width': np.tile(u_width, nobs)})
#
#            df = df.append(F)
#
#        return df

    def _to_df(self, obs_cov, obs_exp, umean_i, uwidth_i):
        df = pd.DataFrame()
        for obs0,obs1,parts in obs_cov:
            key = obs0,obs1

            if (not key in obs_exp) or (not obs_exp[key]):
                continue

#            if not obs_exp[key]: continue
            I,J = zip(*obs_exp[key])

            nobs = len(I)
            F = pd.DataFrame({'i': I,
                              'j': J,
                              'ftype0': nobs*[obs0[0]],
                              'pop0': nobs*[obs0[1]],
                              'ftype1': nobs*[obs1[0]],
                              'pop1': nobs*[obs1[1]],
                              'u_mean': nobs*[umean_i],
                              'u_width': nobs*[uwidth_i]})

            df = df.append(F, ignore_index=True)

        return df

    def _create_obsexpD(self, exp_type, obs_cov):
        ipdb.set_trace()

    def _find_partypack(self, fid, exp_type='select'):
        # Not good. Used 

        obs_cov = self.find_obs()
        all_fluct, all_pop = self.all_fluct_pop(obs_cov)
#        obs_exp = self.obs_exp(fid, u, obs_cov, all_pop)
        u_mean, u_width = self.ubins(fid)

        # So the pkg_write code can output all the correlations. 
        obs_expD = {}
        if exp_type == 'select':
            for umean_i, uwidth_i in zip(u_mean, u_width):
#                ipdb.set_trace()
                obs_expD[umean_i] = self.obs_exp(fid, umean_i, obs_cov, all_pop)
        elif exp_type == 'all':
            new_exp = {}
            for obs1, obs2, _ in obs_cov:
                f1, p1 = obs1
                f2, p2 = obs2
                n1 = self.pop[p1]['nbins']
                n2 = self.pop[p2]['nbins']
                new_exp[obs1,obs2] = list(it.product(range(n1), range(n2)))
            for umean_i in u_mean:
                obs_expD[umean_i] = new_exp
 
        df = pd.DataFrame()
        for umean_i, uwidth_i in zip(u_mean, u_width):
#            obs_exp = self.obs_exp(fid, umean_i, obs_cov, all_pop)
            obs_exp = obs_expD[umean_i]
            df_part = self._to_df(obs_cov, obs_exp, umean_i, uwidth_i)
            df = df.append(df_part, ignore_index=True)

        A = df[['u_mean', 'u_width']].drop_duplicates().sort(columns='u_mean')
        u_mean = np.array(A['u_mean'])
        u_width = np.array(A['u_width'])

#        ipdb.set_trace()
        observ = Observ(df, all_fluct, all_pop, obs_cov, u_mean, u_width)

        return observ
#        return df, all_fluct, all_pop, obs_cov, obs_exp

#    def _find_partypack(self, fid):


    def __call__(self, fid, u):
        """Observables to use for a given u."""

        # STILL KEPT TO ENSURE THE INTERNAL LAYERS ARE CONSISTENT...

        # Why does this even exists????
        obs_cov = self.find_obs()
        all_fluct, all_pop = self.all_fluct_pop(obs_cov)
        obs_exp = self.obs_exp(fid, u, obs_cov, all_pop)
        u_mean, u_width = self.ubins(fid)


        # Storing the mean and width is strange since many
        # quantities depends on one specific u value.
        observD = {'obs_cov': obs_cov, 'all_fluct': all_fluct,
                   'all_pop': all_pop, 'obs_exp': obs_exp,
                   'u_mean': u_mean, 'u_width': u_width}

        return observD

def observ_lin(conf, pkg, pop, cosmo):
    """Creates a new class combining the code for creating the
       normal observables and the bins in angle or l.
    """

    umax_type = conf['opts.umax_type']

    bases = (obs_def.obs_def, expand_obs.expand_obs, \
             observ_combined, d[umax_type])

    F = type('observ_lin', bases, {'conf': conf, 'pkg': pkg, \
             'cosmo': cosmo, 'pop': pop})

    return F()
