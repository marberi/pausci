#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np
import itertools as it

# Yes, some of the code here is confusing. It contains all the definition
# of the WL probes. At the start the goal was to write something general.
# That failed when things had to be changed. The generator expressions was
# very unreadable and confused me.

class obs_def(object):
    def obs_cross2(self, pop, same_field, ftype1, ftype2, btw_same='all', btw_diff='auto'):
        """Gal-gal observables."""

        obs_auto1 = [[(ftype1, x), (ftype2, x), [btw_same]] for x in pop] \
                    if ftype1 == ftype2 else []

        iterator = it.combinations(pop, 2) if ftype1 == ftype2 else \
                   it.permutations(pop, 2)
#                   it.combinations_with_replacement(pop, 2)

#        iterator = it.combinations(pop, 2)
        obs_auto2 = [[(ftype1, pop1), (ftype2, pop2), [btw_diff]] \
                     for pop1, pop2 in iterator]

        if same_field:
            return obs_auto1 + obs_auto2
        else:
            return obs_auto1

    def obs_gg_cross_test(self, pop, same_field):

        assert len(pop) == 2

        basis = [[('counts', 'faint'), ('counts', 'faint'), ['all']], \
                 [('counts', 'bright'), ('counts', 'bright'), ['all']]]

        if not same_field:
            return basis

        key = self.conf['opts.cross_test']
        assert key in ['auto', 'all', 'abs_all']

        last_line = [('counts', 'faint'), ('counts', 'bright'), [key]]

        return basis + [last_line]

    def obs_gg(self, pop, same_field):
        assert not self.conf['opts.cross_test']
#        if self.conf['opts.cross_test']:
#            return self.obs_gg_cross_test(pop, same_field)
        btw_diff = self.conf['opts.pop_overlap']
        btw_same = self.conf['opts.pop_same']

        ans = self.obs_cross2(pop, same_field, 'counts', 'counts', 
                               btw_same='cross',
                               btw_diff='abs_all')

#        raise ValueError(ans)
        return ans

    def obs_magn_nox(self, pop, same_field):
        assert not self.conf['opts.cross_test']
#        if self.conf['opts.cross_test']:
#            return self.obs_gg_cross_test(pop, same_field)
        btw_diff = self.conf['opts.pop_overlap']
        btw_same = self.conf['opts.pop_same']

        ans = self.obs_cross2(pop, same_field, 'counts', 'counts', 
                               btw_same='cross',
                               btw_diff='none')

        return ans

    def obs_magn_nox2(self, pop, same_field):
        """Technical test to see why nox is creating problems."""

        assert not self.conf['opts.cross_test']
#        if self.conf['opts.cross_test']:
#            return self.obs_gg_cross_test(pop, same_field)
        btw_diff = self.conf['opts.pop_overlap']
        btw_same = self.conf['opts.pop_same']

        ans = self.obs_cross2(pop, same_field, 'counts', 'counts', 
                              btw_same='cross',
                              btw_diff='none')

        return filter(lambda X: X[2] != ['none'], ans)



    obs_magn = obs_gg


    def obs_gg_auto(self, pop, same_field):
        return self.obs_cross2(pop, same_field, 'counts', 'counts', btw_same='auto')


    def obs_gg_cross(self, pop, same_field):
        return self.obs_cross2(pop, same_field, 'counts', 'counts', btw_same='onlycross')

#                               btw_same=self.conf['opts.pop_overlap'])

    def obs_ss(self, pop, samefield):
        return [[('shear', 'all'), ('shear', 'all'), ['all']]]

    obs_shear = obs_ss

    def obs_mm(self, pop, samefield):
        if len(pop) == 2:
            return [[('mag', 'bright'), ('mag', 'bright'), ['cross']], 
                    [('mag', 'bright'), ('mag', 'faint'), ['cross']],
                    [('mag', 'faint'), ('mag', 'faint'), ['cross']]]
        
        return self.obs_cross2(pop, samefield, 'mag', 'mag', btw_same='cross')

    def obs_gs(self, pop, samefield):
        """Correlations of galaxy counts and shear."""

        if len(pop) == 1 and pop[0] == 'bright' and not samefield:
#            pdb.set_trace()
            return [[('counts', 'bright'), ('counts', 'bright'), ['all']]]


        obs_cov_gg = self.obs_gg(pop, samefield)

        # Original..
        if samefield:
            obs_cov_cross = [[('counts', x), ('shear', 'all'), ['abs_all']] for x in pop]
            #obs_cov_cross = [[('counts', x), ('shear', 'all'), ['all']] for x in pop] # HACK
        else:
#            some_pop = [x for x in pop if not x == 'bright']
            obs_cov_cross = [[('counts', 'all'), ('shear', 'all'), ['abs_all']]] # for x in some_pop]

#        pdb.set_trace()
        obs_cov_ss = [[('shear', 'all'), ('shear', 'all'), ['auto']]]

        #obs_cov_ss = [[('shear', 'all'), ('shear', 'all'), ['auto']]]

#        pdb.set_trace()
#:::::FIRST ROUND OF DEBUGGING:::::
        
        return obs_cov_gg + obs_cov_cross + obs_cov_ss

    def _conv(self, pop_name):
        if pop_name == 'all':
            return all_pop
        else:
            return pop_name


    def _vol_toincl(self, pop, X):
        obs1,obs2,fpart = X

        all_pop = 'faint' if 'faint' in pop else 'bright'
        x1 = all_pop if obs1[1] == 'all' else obs1[1]
        x2 = all_pop if obs2[1] == 'all' else obs2[1]

#            pdb.set_trace()
        return x1 == x2


    def obs_gs_vol(self, pop, samefield):
        """Test the effect of having overlapping volumes."""
        assert samefield, 'Probably meant to be used for overlapping surveys.'

        ans = []
        for X in self.obs_gs(pop, samefield):
            if self._vol_toincl(pop, X):
                ans.append(X)

        return ans


    def obs_counts_vol(self, pop, samefield):
        """Test the effect of having overlapping volumes."""
        assert samefield, 'Probably meant to be used for overlapping surveys.'

#            conv = lambda pop_name: all_
#            return obs1[1] == obs2[1]

        ans = []
        for X in self.obs_magn(pop, samefield):
            if self._vol_toincl(pop, X):
                ans.append(X)
#        ans2 = [self._vol_toincl(pop, X) for X in ans]

        return ans

    def obs_gs_minus(self, pop, samefield):
        def toincl(X):
            ftypes = set([X[0][0], X[1][0]])
            pops = [X[0][1], X[1][1]]
            pops = [('faint' if x=='all' else x) for x in pops]
            pops = set(pops)

            if len(ftypes)==2 and len(pops)==2:
                return False
            else:
                return True

        from_gs = self.obs_gs(pop, samefield)
        res = filter(toincl, from_gs)

        return res

    def obs_gs_minus2(self, pop, samefield):
        def toincl(X):
            ftypes = set([X[0][0], X[1][0]])

            if len(ftypes)==2:
                return False
            else:
                return True

        from_gs = self.obs_gs(pop, samefield)
        res = filter(toincl, from_gs)

        return res

    def obs_gs_minus3(self, pop, samefield):
        def toincl(X):
            ftypes = set([X[0][0], X[1][0]])
            pops = [X[0][1], X[1][1]]
            pops = [('faint' if x=='all' else x) for x in pops]
            pops = set(pops)

            if len(ftypes)==2 and len(pops)==1:
                return False
            else:
                return True

        from_gs = self.obs_gs(pop, samefield)
        res = filter(toincl, from_gs)

        return res

    def obs_gs_nocc(self, pop, samefield):
        def toincl(X):
            if (X[0][0] == 'counts') and (X[1][0] == 'counts'):
                return False
            else:
                return True

        from_gs = self.obs_gs(pop, samefield)
        res = filter(toincl, from_gs)

        return res




    def obs_plot(self, pop, samefield):
        res = \
          [[('counts', 'bright'), ('counts', 'bright'), ['all']],
           [('counts', 'bright'), ('shear', 'bright'), ['all']],
           [('shear', 'bright'), ('shear', 'bright'), ['all']]]


        return res
    def obs_ys(self, pop, samefield):
        if pop == ['faint']:
            return [[('shear', 'faint'), ('shear', 'faint'), ['auto']]]
        elif pop == ['bright']:
            return [[('counts', 'bright'), ('counts', 'bright'), ['all']]]
        elif pop == ['faint', 'bright']: 
            return [[('counts', 'bright'), ('counts', 'bright'), ['all']],
                    [('counts', 'bright'), ('shear', 'faint'), ['all']],
                    [('shear', 'faint'), ('shear', 'faint'), ['auto']]]

        return res

    def obs_gs_mod(self, pop, samefield):
#        obs_cov_gg = self.obs_gg(pop, samefield)
#        gg_auto = [[('counts', x), ('counts', x), ['auto']] for x in pop] 
#        gg_cross = [[('counts', x), ('counts', y), ['auto']] for x,y in it.combinations(pop, 2)]

#        obs_cov_gg = gg_auto + gg_cross if samefield else gg_auto

        # HACK!!
        assert len(pop) == 1
        pop = pop[0]
        return [[('counts', pop), ('counts', pop), ['cross']], 
                [('counts', pop), ('shear', pop), ['cross']]]
#                [('counts', 'bright'), ('counts', 'faint'), ['all']]]

        obs_cov_gg = [[('counts', x), ('counts', x), ['all']] for x in pop]


        obs_cov_cross = [[('counts', x), ('shear', 'all'), ['all']] for x in pop]
        obs_cov_ss = [[('shear', 'all'), ('shear', 'all'), ['all']]]
        
        return obs_cov_gg + obs_cov_cross + obs_cov_ss

# This one actually works..
#        return [[('counts', 'faint'), ('counts', 'faint'), ['all']], \
#                [('counts', 'bright'), ('counts', 'bright'), ['all']], \
#                [('counts', 'faint'), ('counts', 'bright'), ['all']], \
#
#                [('mag', 'faint'), ('mag', 'faint'), ['all']], \
#                [('mag', 'bright'), ('mag', 'bright'), ['auto']], #, \
#
#                [('counts', 'faint'), ('mag', 'faint'), ['auto']],
#                [('counts', 'bright'), ('mag', 'faint'), ['auto']]]
#


    def OLD_obs_gm(self, pop, samefield):


        ftype_list = ['counts', 'mag']
        toexclude = [(('counts', 'faint'), ('mag', 'faint'))]

        res = self.my_auto(pop, ftype_list, toexclude)

        return res

        import pdb; pdb.set_trace()
        res.extend(self.obs_cross2(pop, samefiled, 'counts', 'counts'))
        res.extend(self.obs_cross2(pop, samefiled, 'mag', 'mag'))

        l1 = list(it.combinations_with_replacement(ftype_list, 2))
        res = [self.obs_cross2(pop, samefield, ftype1, ftype2) for ftype1, ftype2 \
               in l1]

        res = sum(res, [])

        return res

    def my_auto(self, pop, ftypes, toexclude, cross='abs_all'):

        res = []
        observ = list(it.product(ftypes, pop))
        toexclude = [set(x) for x in toexclude]
        for obs1 in observ:
            for obs2 in observ:
                if obs1 < obs2: continue
                isauto = (obs1 == obs2)

                if set([obs1,obs2]) in toexclude: continue
                if isauto:
                    res.append((obs1, obs2, ['all']))
                else:
                    res.append((obs1, obs2, [cross]))

        return res

    def obs_des(self, pop, same_fiels):
        assert pop == ['faint']

        return [(('counts', 'faint'), ('counts', 'faint'), ['auto'])]

    def obs_gm(self, pop, same_field):

#        return [(('counts', 'bright'), ('mag', 'faint'), ['cross']),
#                (('counts', 'bright'), ('counts', 'bright'), ['auto'])]

#                (('mag', 'faint'), ('mag', 'faint'), ['auto'])]

        toexclude = [(('counts', 'faint'), ('counts', 'faint')),
                     (('counts', 'faint'), ('mag', 'faint')),
                     (('counts', 'bright'), ('counts', 'bright')),
                     (('counts', 'bright'), ('mag', 'bright'))]

        btw_diff = self.conf['opts.pop_overlap']
        if len(pop) == 1:
#            return self.my_auto(pop, ['counts', 'mag'], [], 'auto')
            pop1 = pop[0]
            ans = [[('counts', pop1), ('counts', pop1), ['all']],
                   [('counts', pop1), ('mag', pop1), ['abs_all']]]
#                   [('mag', pop1), ('mag', pop1), ['all']]]

            return ans
#            import pdb; pdb.set_trace()
        elif len(pop) == 2:
#                  [('mag', 'faint'), ('mag', 'faint'), ['auto']], # Disable

            pop1 = 'faint'
            ans = [[('counts', pop1), ('counts', pop1), ['all']],
                   [('counts', 'bright'), ('counts', 'bright'), ['all']],
                   [('counts', 'bright'), ('mag', 'bright'), ['abs_all']],
                   [('counts', pop1), ('mag', pop1), ['abs_all']]]

#                   [('mag', 'bright'), ('mag', 'bright'), ['cross']],
#                   [('mag', pop1), ('mag', pop1), ['cross']],

            if same_field:
                ans.extend(\
                  [[('counts', 'bright'), ('counts', pop1), ['abs_all']],
                   [('counts', 'bright'), ('mag', pop1), ['abs_all']],
                   [('counts', pop1), ('mag', 'bright'), ['abs_all']]])
#                   [('mag', 'bright'), ('mag', pop1), ['abs_all']]])

#                   [('counts', 'bright'), ('mag', pop1), ['abs_all']],
#                   [('mag', pop1), ('counts', 'bright'), ['abs_all']]]




            if False: #same_field:
                ans.extend(\
                 [[('counts', 'faint'),  ('mag', 'bright'), [btw_diff]],
                  [('counts', 'bright'), ('mag', 'faint'), [btw_diff]]]) #Disable

            if False: # or same_field:
                ans.extend(\
                 [[('counts', 'faint'), ('counts', 'bright'), ['auto']],
                  [('counts', 'faint'), ('mag', 'bright'), ['abs_all']],
                  [('mag', 'faint'), ('counts', 'faint'), ['abs_all']],
                  [('counts', 'bright'), ('mag', 'faint'), ['abs_all']]])

#            import pdb; pdb.set_trace()
            return ans


    def obs_gm_red(self, pop, same_field):

        ans = self.obs_gm(pop, same_field)
        import pdb; pdb.set_trace()

    def obs_cross(self, pop, samefield, type1, type2, gk_comp=False):
        # For counts-shear we need some extra backwards compatability.
#        gk_comp = (type1, type2) == ('counts', 'shear')

        # Writing in terms of counts-shear cross correlation makes the
        # expressions more readable, but they are still fully general.
        counts, shear = type1, type2
        obs_auto1 = [[(counts, x), (counts, x), ['auto']] for x in pop]
        obs_auto2 = [[(counts, pop1), (counts, pop2), ['auto']] \
                     for pop1, pop2 in it.combinations(pop, 2)]

        obs_upper = [[(counts, x), (shear, 'all'), ['all']] for x in pop]

        if gk_comp:
            return obs_auto1, obs_auto2, obs_upper
        else:
            return obs_auto1 + obs_auto2 + obs_upper

#        return self.obs_cross(pop, samefield, counts, shear)

    def obs_gs_cross(self, pop, samefield):
        obs_cov = []
        npop = len(pop)

        obs_auto1, obs_auto2, obs_upper = self.obs_cross(pop, samefield, 'counts', 'shear', True)

        res = obs_auto1 + obs_upper
    
        return res

    def obs_gs_mini(self, pop, samefield):
        # Actually the same for the same sky or different sky..

        return [[('counts', x), ('shear', 'all'), ['cross']] for x in pop]

    def obs_gm_cross(self, pop, samefield):
        return self.obs_cross2(pop, samefield, 'counts', 'mag')

    def obs_mg_cross(self, pop, samefield):
        return self.obs_cross2(pop, samefield, 'mag', 'counts')


    def find_obs(self):
        assert len(self.conf['opts']['exp_type']), 'Ugly hack.'

        pop = self.pop['meta']['pop']
        exp_type = self.conf['opts']['exp_type'][0]
        same_field = self.conf['opts']['same_field']

        try:
            f_name = 'obs_%s' % exp_type
            f = getattr(self, f_name)
        except AttributeError:
            msg_probe = 'Not implemented probe: %s' % exp_type
            raise AttributeError(msg_error)


        obs_cov = f(pop, same_field)

#        pdb.set_trace()
        # All this is to replace the old 'all' definition...
        all_pop = 'faint' if 'faint' in pop else 'bright'
        def replace_all(x):
            for i in range(2):
                if not x[i][1] == 'all': continue
                tmp = list(x[i])
                tmp[1] = all_pop
                x[i] = tuple(tmp)

            
            return x

        obs_cov = [replace_all(x) for x in obs_cov]

        return obs_cov
