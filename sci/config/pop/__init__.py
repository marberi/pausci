#!/usr/bin/env python
# encoding: UTF8

import glob
import os
import ipdb
import sys
import yaml

pop_dir = os.path.dirname(__file__)
paths = glob.glob(os.path.join(pop_dir, '*.yaml'))

for file_path in paths:
    locals().update(yaml.load(open(file_path, 'r')))

gold = faint
