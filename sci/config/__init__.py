#!/usr/bin/env python
# encoding: UTF8

# The implementation here can be improved. Earlier I used python
# files for the configurations and when moving the focus was
# to check the results.

import glob
import os
import pdb
import sys
import yaml

import sci.config.pop
import sci.config.survey

def update(d, u):
    for k, v in u.iteritems():
        if k in d:
            d[k].update(v)
        else:
            d[k] = v

# The default configuration for the program.
progconf_dir = os.path.dirname(__file__)
conf_glob = os.path.join(progconf_dir, '*.yaml')

config = {}
for file_path in glob.glob(conf_glob):
    update(config, yaml.load(open(file_path,'r')))


# Add custom configuration.
env_conf = os.environ.get('SCICONFPATH')
conf_dir = env_conf if env_conf else '~/.sci'
conf_path = os.path.join(conf_dir, '.sci.yaml')
conf_path = os.path.expanduser(conf_path)

if os.path.exists(conf_path):
    update(config, yaml.load(open(conf_path, 'r')))

locals().update(config)

# So handling of ~ has not to be handled many places in the code.
paths = {}
paths['cachedir'] = os.path.expanduser(general['cachedir'])
