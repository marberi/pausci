#!/usr/bin/env python
# encoding: UTF8

import glob
import os
import pdb
import sys
import yaml

survey_dir = os.path.dirname(__file__)
paths = glob.glob(os.path.join(survey_dir, '*.yaml'))

for file_path in paths:
    locals().update(yaml.load(open(file_path, 'r')))
