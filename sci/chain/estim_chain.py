#!/usr/bin/env python
# encoding: UTF8 
from __future__ import print_function

try:
    import tables
except ImportError:
    pass

try:
    import pymc
except ImportError:
    pass

import copy
import os
import ipdb
import numpy as np

import sci
from sci.lib.libh5attr import h5getattr, h5setattr
from sci.chain import base_chain

import emcee

class estim_chain(base_chain.base_chain, object):
    """Estimate uncertainties in parameters using MCMC
       chains.
    """

    def write_bestfit(self, file_name, best_fit):
        """Add info with the MAP to the HDF5 file."""

        # Ideally the best fit could be found from the chains later.
        # By now I have problems doing that...
        f = tables.openFile('{0}.hdf5'.format(file_name), 'a')

        # Create array to store values.
        try:
            node_bfit = f.getNode('/', 'bestfit/bestfit')
        except tables.NodeError:
            f.createGroup('/', 'bestfit')
            node_bfit = f.createArray('/bestfit', 'bestfit', np.zeros(1))

        for p, fit_val in best_fit.iteritems():
            setattr(node_bfit.attrs, p, fit_val)

        f.close()


    def _output_path(self):
        """Path to store the chain."""

        cachedir = sci.config.paths['cachedir']
        file_name = self.conf.hashid()
        return os.path.join(cachedir, 'chain', file_name)

    def _create_chain(self, params, file_path):
        """Create an empty HDF5 file with a table for the chain."""

        # This code should perhaps also use Pandas. For another
        # time...
        fields = params + ['lnprob']
        if not file_path:
            file_path = self._output_path()

        fb = tables.openFile(file_path, 'w')

        meta = fb.createArray('/', 'meta', np.zeros(1))
        h5setattr(meta, 'conf', self.conf.data)
        fb.createGroup('/', 'chains')

        descr = dict([(x,tables.FloatCol(pos=i)) for i,x in enumerate(fields)])
        chain = fb.createTable('/chains', 'chain0', descr)

        return fb, chain

    def sample_params(self, output_path=''):
        
        par = copy.deepcopy(self.conf.fid)
 
        # Slight offset in one parameter.
        
        obs_vec = self.obs_vec()
        obs_cov = self.obs_cov()
        
        icov = np.linalg.pinv(obs_cov)
        params = self.conf['opts.params']
       
        def logp(p, param_keys, mean, icov):
            par.update(dict(zip(param_keys, p)))


            if True:
                thr_vec = self.thr_vec(par)
                diff = thr_vec - obs_vec
                chi2 = np.dot(diff, np.dot(icov, diff))
            else:
                chi2 = 0.

            # Priors.
            if self.use_priors:
                params = self.conf['opts.params']
                dy_pr = p - self.prior_fid

                chi2 += np.dot(dy_pr, np.dot(self.prior_covinv, dy_pr))

            return -0.5*chi2

        param_keys = params
 
        verbose = self.conf['info.mcmc.verbose']
        npoints = self.conf['info.npoints']
        nburn = self.conf['info.mcmc.nburn']
        nwalkers = 2*len(params)
        ndim = len(params)

#        ipdb.set_trace()

        args = [param_keys, obs_vec, icov]
        sampler = emcee.EnsembleSampler(nwalkers, ndim, logp, args=args)

        fb, chain = self._create_chain(params, output_path) 
        p0 = self.initial_pos(nwalkers, params)

        pos, prob, state = sampler.run_mcmc(p0, nburn)
        sampler.reset()

        print('nburn', nburn)
        for result in sampler.sample(pos, iterations=npoints, storechain=False):
            # Also including the probability.
            tostore = np.vstack([result[0].T, result[1]]).T
            chain.append(tostore)
            chain.flush()

            print('finished one part...')

        fb.close()

    def get_chain(self, output_path=''):
        self.setup_observables()
        self.setup_stepsize()
        self.setup_priors()
        self.sample_params(output_path)
