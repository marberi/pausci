#!/usr/bin/env python
# encoding: UTF8

import copy
import hashlib
import json
import ipdb
import numpy as np
import pandas as pd
import itertools as it
from scipy.linalg import block_diag

import sci
import sci.tasks

# Weave does not support Python3.x
try:
    from scipy import weave
    from scipy.weave import converters
except ImportError:
    pass


code_weave_v2 = """
using namespace std;
int obsnr0, obsnr1;
int u0, u1;
int i,j,k,l;

int nobs0 = Ninds0[0];
int nobs1 = Ninds1[0];

for (int x=0; x < nobs0; x++){
  obsnr0 = inds0(x, 0);
  u0 = inds0(x, 1);
  i  = inds0(x, 2);
  j  = inds0(x, 3);

  for (int y=0; y < nobs1; y++){
    obsnr1 = inds1(x, 0);
    u1 = inds1(x, 1);
    k  = inds1(y, 2);
    l  = inds1(y, 3);

    cov(obsnr0, obsnr1) = cov_in(u0,u1,i,j,k,l);
  }
}
"""


class base_chain(object):
#    def __init__

    def setup_observables(self):

        fid = self.conf.fid
        self.fix_cache(fid)
        u_mean, u_width = self.observ.ubins(fid)

        extobsD = {}
        for u in u_mean:
            extobsD[u] = self.observ(fid, u)

        self.u_mean = u_mean
        self._extobsD = extobsD

        self.inds = self.conv_inds(u_mean, extobsD)


    def conv_inds(self, u_mean, expD):
        """Create a DataFrame from the passed indices."""

        # This operation is partly duplicating the work in the indices
        # object. I have introduced this here while converting some
        # of the other code.
        assert isinstance(expD, dict), 'Testing we have actually passed a dictionary.'
     
        df = pd.DataFrame()
        for u in u_mean:
            exp_part = expD[u]

            for obs, inds in exp_part['obs_exp'].iteritems():
                df_part = pd.DataFrame(inds, columns=['i','j'])
                df_part['u'] = u
                df_part['ftype0'] = obs[0][0]
                df_part['pop0'] = obs[0][1]
                df_part['ftype1'] = obs[1][0]
                df_part['pop1'] = obs[1][1]

                df = df.append(df_part, ignore_index=True)

        # Not sure is this is needed. The DataFrame indexing can
        # be confusing.
        df['obsnr'] = range(len(df))

        return df



    def _fisher_stepsize(self):
        """Set the stepsize based on the Fisher matrix calculation."""

        # This code has two problems:
        # 1 - The Fisher matrix use the default redshift binning instead
        #     of the one passed. Depending on the configuration, this can
        #     have quite a large effect on the redshift binnin.

        params = self.conf['opts.params']

        # For doing the Fisher matrix using fourier space is preferred.
        fisher_myconf = copy.deepcopy(self.conf['myconf'])
        if 'opts.pkg' in fisher_myconf:
            del fisher_myconf['opts.pkg']
            del fisher_myconf['info.estim']
            fisher_myconf['opts.fourier'] = True


    	task = sci.tasks.fisher(fisher_myconf)
    	F = task.run(False, False, True)

        return F.cov

    def setup_stepsize(self):

        params = self.conf['opts.params']
        V = [self.conf['fid.{0}'.format(x)] for x in params]
        V = np.array(V)

        cov = np.diag((0.1*V)**2)
#        ipdb.set_trace()
        return cov

    def setup_priors(self):
        """Use the Fisher matrices to generate a prior on the observables."""

        mypriors = sci.priors.libpriors.mypriors()
        prlist = self.conf['info.priors']

        if prlist:
            F_priors = [mypriors[x] for x in prlist]
            F_all = sum(F_priors[1:], F_priors[0]) if \
                    1 < len(F_priors) else F_priors[0]
    
            fid = self.conf.fid
            params = self.conf['opts.params']

            # Select only the parameters where we have priors. This could
            # have been done elsewhere...
            prior_params = F_all.conf['opts.params']
            prior_covinv = np.zeros(2*(len(params),))
            common_params = list(set(prior_params) & set(params))
            toind = map(params.index, common_params)

            # If none of the parameters have priors.
            if not common_params:
                self.use_priors = False
                return

            prior_covinv[np.ix_(toind,toind)] = F_all.select(common_params)
            self.prior_covinv = prior_covinv

            # Use the comology of the input.
            pr_fid = self.pkg.meta['fid'] if 'fid' in self.pkg.meta \
                     else self.conf.fid

            self.prior_fid = np.array([pr_fid[x] for x in params])
            self.use_priors = True
        else:
            self.use_priors = False

    def fix_cache(self, par):
        """Handle the cache in different modules."""

        par['hash'] = par.hashid()
        self.cosmo.cache_param(par)
        self.corr.lens.cache_param(par)

    def thr_vec(self, par):
        """Vector with the theoretical predictions."""

        print('One theory vector.. yes.')
        # This code is a weird mixture. The selection can not yet
        # be done using the indices for all l/theta values.
        self.fix_cache(par)

        corr_vec = np.nan*np.ones(len(self.inds))

        one_observD = self._extobsD.values()[0] # HACK
        corr_iter = self.corr.create_iter(par, one_observD)

        df = pd.DataFrame()
        for corrD in corr_iter:
            u = corrD['u']
            corrD = self.syst(par, u, corrD)

            #corr_vec = self.observ.sel_obs(corrD, observD)
            corrX  = self.observ.sel_obs_tmp(corrD, self.inds)
            df = df.append(corrX)

#        ipdb.set_trace()
#        print('par', par)
        print([par[x] for x in ['om_m', 'om_de', 'h', 'sigma8', 'om_b', 'w0', 'wa', 'ns', 'gamma']])

        print('observable(0)', df['corr'].values[10])
        return df['corr'].values


    def obs_vec(self):
        """Vector with the observations."""

        corr_vec = np.nan*np.ones(len(self.inds))

        df = pd.DataFrame()
        for u in self.u_mean:
            #observD = self.extobsD[u]
            corrD = {'corr_obs': self.pkg.corr[u]}
            corrD['u'] = u

            corrX = self.observ.sel_obs_tmp(corrD, self.inds)
            df = df.append(corrX)

        return df['corr'].values




    def obs_cov(self):
        """Convert the input covariance to a 2D structure."""

        inds = self.inds

        data_cov = self.pkg.cov
        cov = np.zeros((len(inds), len(inds)))

        for key0, val0 in inds.groupby(['ftype0', 'pop0', 'ftype1', 'pop1']):
            ftype0, pop0, ftype1, pop1 = key0

            inds_df0 = val0[['obsnr','u','i','j']]
            inds0 = inds_df0.values

            for key1, val1 in inds.groupby(['ftype0', 'pop0', 'ftype1', 'pop1']):
                ftype2, pop2, ftype3, pop3 = key1
                inds_df1 = val1[['obsnr', 'u','i','j']]
                inds1 = inds_df1.values

                cov_key = (ftype0,pop0), (ftype1,pop1), (ftype2,pop2), (ftype3,pop3)
                cov_in = data_cov[cov_key]

                args = ['cov', 'cov_in', 'inds0', 'inds1']
                weave.inline(code_weave_v2, args, type_converters=converters.blitz)

        return cov

#        self.obs_cov_6d()

    def initial_pos(self, nwalkers, params):
        """Find the initial position for the chain."""

        fid = self.conf.fid
        step_cov = self.setup_stepsize() # Will change name when Jacobo merge.

        mean = [fid[x] for x in params]
        ipos = np.random.multivariate_normal(mean, step_cov, nwalkers)

        return ipos
