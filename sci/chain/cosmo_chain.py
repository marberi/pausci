#!/usr/bin/env python
# encoding: UTF8

import os
import pdb
#import pymc
import numpy as np
from scipy.stats import scoreatpercentile

import sci

class cosmo_chain(object):
    has_trace = True

    def __init__(self, conf=False, data={}):

        # So the user can pass myconf dictionaries.
        if isinstance(conf, dict): 
            conf = sci.libconf(conf)

        self.conf = conf
        self._data = data 

    def acronym(self):
        """Short name to describe the probes, area and if they are
           over the same part of the sky.
        """

#        raise NotImplementedError('opts.pop can not come from the configuration.')
        pop = self.conf['opts.pop']
        exp_type = self.conf['opts.exp_type']
        same_field = self.conf['opts.same_field']

        # Map from population name to a simple letter.
        pop_letter = dict((y,x) for (x,y) in sci.config.general['pop_name'].items())

        op = 'x' if same_field else '+'
        part1 = op.join(pop_letter[x].upper() for x in pop)
        part2 = '%'.join(exp_type)

        acro = '{0}-{1}'.format(part1, part2)

        return acro

    def hist(self, paramx, paramy):
        """Returns probability histogram for two parameters."""

        tr1 = self._data[paramx]
        tr2 = self._data[paramy]
#        dev = self._data['deviance']

        # Prefactor found when working with Gaussian. This is probably very
        # wrong for what we are doing by now....
        A = -8.7

        chi2 = dev + 2*A
        chi2diff = chi2 - np.min(chi2)
        likenorm = np.exp(chi2diff) / np.exp(A)

        bins = 2*(self.conf['info.nhist_bins'],)
        H, xedges, yedges = np.histogram2d(tr1, tr2, bins=bins) #, weights=likenorm)
#        N, _, _ = np.histogram2d(tr1, tr2, bins=bins)
#        
#        # Normalize based on the number..
#        np.seterr(divide='ignore', invalid='ignore')
#        H = H/N
#        H = np.where(np.isnan(H), 0, H)

        #extent = [yedges[0], yedges[-1], xedges[0], xedges[-1]]
        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
        percents = [34.1, 13.6, 2.1]
        levels = [scoreatpercentile(likenorm, x) for x in percents]
    
        return tr1, tr2, levels, H, extent


        pdb.set_trace()

    def foms(self, fom_types, prior_list, mypriors='', suf=''):
        """Figures of Merit. Not implemented yet, but returning some values
           makes integration easier..
        """

        foms = dict((x, -1) for x in fom_types)

        return foms
