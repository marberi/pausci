#!/usr/bin/env python
# encoding: UTF8

import pdb
import tables

from sci.lib.libh5attr import h5getattr

def load(file_path):
    """Load a chain. Assume PYMC chain for now."""

    fb = tables.openFile(file_path)
    traces = fb.getNode('/chains/chain0').read()
    
    meta = fb.getNode('/meta')
    conf = h5getattr(meta, 'conf')

    return conf, traces
