#!/usr/bin/env python
# encoding: UTF8

import pdb

import numpy as np
from scipy.special import erf

parameters = { 'dc':1.686, 'a': 1./np.sqrt(2.), 'b':.35, 'c':.8, 'Mmin':10.**11.18, 'Siglm':.19, 'M0':10.**9.81, 'M1':10.**12.43, 'alpha': 1.04}

class hod:
    """Implements a bias from an HOD."""

    def __init__(self, conf, par):
        self.conf = conf
        self.par = par
    
    def smt01_bias(self, par, nu):
        """Bias as a function of nu, from Sheth, Mo and Tormen 2001, arXiv:astro-ph/9907024."""

        return 1. + (1/np.sqrt(par['a'])/par['dc'])*(np.sqrt(par['a'])*(par['a']*nu**2.) + np.sqrt(par['a'])*par['b']*(par['a']*nu**2.)**(1.-par['c']) - (par['a']*nu**2.)**par['c']/( (par['a']*nu**2.)**par['c'] + par['b']*(1. - par['c'])*(1. - par['c']/2.)))

    def hod_cen(self, par, M):
        """Number of central galaxies for mass M, from Zheng et al. 2005, arXiv:astro-ph/0408564."""

        return .5*(1 + erf((np.log10(M) - np.log10(par['Mmin']))/par['Siglm']))

    def hod_sat(self, par, M):
        """Number of satelites for mass M, from Zheng et al. 2005, arXiv:astro-ph/0408564."""

        sat = ((M - par['M0'])/par['M1'])**par['alpha']
        sat[M < par['M0']] = 0.

        return sat

    def mean(self, par, obs, z, k, use_diag=True):
        """Mean of the bias."""

        # Construct M array..
        # Construct nu array
        M = np.array([1.00000000e+10,   1.06000000e+10,   1.12000000e+10,1.19000000e+10, \
                      1.26000000e+10,   1.34000000e+10,1.41000000e+10,   1.50000000e+10, \
                      1.59000000e+10,1.68000000e+10])

        nu = np.array([0.419846,  0.422511,  0.425203,  0.427922,  0.430669,  0.433442, \
                       0.436244,  0.439074,  0.441933,  0.44482])


        # HACK. The parameters should be set in the main configuration file.
        par.update(parameters)

        # TODO: delta_c should be passed to the ST bias function.
        delta_c = self.cosmo.cache_ev(par, 'delta_c', z)
        bias = self.smt01_bias(par, nu)
        cen = self.hod_cen(par, M)
        sat = self.hod_sat(par, M)

        M = np.logspace(10,15)
        logM = np.log10(M)

        z = np.array([0., 0.5, 1.])
        nu = self.cosmo.cache_ev2dgrid(par, 'nu', z, logM)
#        mf = self.cosmo.cache_ev(par, 'st_mf', nu)

        from matplotlib import pyplot as plt
        import os
        file_name = 'nu_vs_log10M_mice_parms_z0.0-0.5-1.0.dat'
        file_path = os.path.join('/Users/marberi/Downloads', file_name)


        def S(x,y):
            for i, zi in enumerate(z):
                plt.plot(x+logM, y*np.sqrt(nu[i,:]), label='Eriksen, z = {0}'.format(zi))

            A = np.loadtxt(file_path)
            for i, zi in enumerate(z):
                plt.plot(A[:,0], A[:,i+1], label='Lizard, z = {0}'.format(zi))

#            plt.yscale('log')
            plt.legend(loc=0)
            plt.show()

        pdb.set_trace()


    def stoc(self, par, z, k, obs, use_diag=False):
        """The stochastic r variables for the bias."""

    	raise NotImplementedError()
