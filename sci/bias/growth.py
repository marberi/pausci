#!/usr/bin/env python
# encoding: UTF8

class growth(object):
    def __call__(self, par, z, pop, l=False):
        """Test of an alternative bias evolution."""

        b0 = par['bias_cons']
        alpha = par['bias_exp']

        return b0*self.D(par, z)**alpha
