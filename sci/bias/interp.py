#!/usr/bin/env python
# encoding: UTF8

import pdb
from scipy.interpolate import splev, splrep
import numpy as np

from sci.lib import libinterp

class interp(object):
    """Bias which interpolates between some paramenters."""

    def __init__(self, conf, par):
        self.conf = conf
        self.par = par

    def mean(self, par, obs, z, k, use_diag=False):
        """Mean bias."""

        z_part = libinterp.use_spl(par, z, obs, '', 'bias_z')

        if use_diag:
            return z_part
        else:
            ans = np.tile(z_part, (len(k),1)).T
            assert ans.shape == (len(z), len(k))

            return ans

    def stoc(self, par, obs, z, k, use_diag=False):
        """Stocaticity of the bias."""

        return libinterp.use_spl(par, z, obs, 'r', 'bias_z')

