#!/usr/bin/env python
# encoding: UTF8

# Modules which implements the galaxy bias. Used for different
# measurements like galaxy counts and galaxy magnitudes.
__all__ = ['fnl', 'interp', 'hod']

from sci.bias.fnl import fnl
from sci.bias.interp import interp
from sci.bias.hod import hod
