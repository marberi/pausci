#!/usr/bin/env python
# encoding: UTF8

import pdb
from sci.bias import interp

import numpy as np

class fnl(interp.interp, object):
    """Implements a bias with non-gaussianities."""

    def __init__(self, conf, par):
        self.conf = conf
        self.par = par

    def mean(self, par, obs, z, k, use_diag=True):
        """Mean of the bias."""

        msg_k_l = 'The comoving scale is used.'
#        assert np.max(k) < 10., msg_k_l
        b_orig = interp.interp.mean(self, par, obs, z, k)

        # Old code: see arxiv/1106.3999v3
        T = self.cosmo.cache_ev(par, 'T', k)
        D = self.cosmo.cache_ev(par, 'D', z)
        om_m = self.cosmo.cache_ev(par, 'om_m', z)
        
        pre = 3.*par['fnl']*par['H_0']**2*par['delta_c'] / par['c']**2
        delta_b = pre*(b_orig-1)*np.outer(om_m/D, 1./(T*k**2))

        b = b_orig + delta_b

        if use_diag:
            b = np.diag(b)

        return b

    def stoc(self, par, z, k, obs, use_diag=False):
        """The stochastic r variables for the bias."""

        return interp.interp.stoc(self, par, z, k, obs)
