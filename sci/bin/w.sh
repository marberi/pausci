#!/bin/sh

# Extremely dirty, but works. It looks for printstatements in the code
# which does not contain #z. The permanent print messages have the comment
# "#z". In that way I can easier detect comments which was added for 
# debugging.

egrep -r print *|egrep -v "tech/"|egrep -v "#z" |egrep -v "notes/" |egrep -v "proj/" |egrep -v "tmp/"|egrep -v "Binary"|egrep -v "telecon/" \
|egrep -v "__future__"|egrep -v "notes_test/" |egrep -v "\.c:"|egrep -v "email/"|egrep -v "restlite"|egrep -v "play/"|egrep -v "pycamb.py" \
|egrep -v "w.sh"
