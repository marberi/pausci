#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function

import ipdb
import sys
import time
import numpy as np

# Weave does not support Python3.x
try:
    from scipy import weave
    from scipy.weave import converters
except ImportError:
    pass

code_cov = """
using namespace std;
int i,j,k,l;

int nobs1 = Nobserv1[1];
int nobs2 = Nobserv2[1];

float nl_inv = 1./nl;

for (int x=0; x<nobs1; x++)
{
    i = observ1(0, x);
    j = observ1(1, x);
    for (int y=0; y<nobs2; y++)
    {
        k = observ2(0, y);
        l = observ2(1, y);

        cov_mat(x,y) = nl_inv*(c1(i,k)*c2(j,l) + c3(i,l)*c4(j,k));
    }
}

"""


code_var = """

using namespace std;
int i,j;

int nobs = Nobserv[1];

float nl_inv = 1./nl;

for (int x=0; x<nobs; x++)
{
    i = observ(0, x);
    j = observ(1, x);

    var_mat(x) = nl_inv*(c1(i,i)*c2(j,j) + c3(i,j)*c4(j,i));
}
"""

class cov_weave(object):
    """Calculate the covariance using weave for the numerical
       calculations.
    """

    def __init__(self, conf):
        self.conf = conf

    def scale_factor(self):

        if self.conf['opts.scale_area']:
            return self.conf['opts.scale_to'] / self.conf['fid.area']
        else:
            return 1.

    def cov(self, par, corr, observD, nl, use_pieces=False):
        """Gluing together the covariance calculated in parts 
           by the c++ code.
        """

        # If something breaks here it is because of passing an old version
        # of observD. Or the new hack..
        obs_cov = observD['obs_cov']
#        obs_exp = observD['obs_exp'].values()[0] # This line is retarded.
        obs_exp = observD['obs_exp'] #.values()[0] # This line is retarded.


        # Weave need a Python float instead of a numpy float.
        nl = float(nl)
        res = []

        r = self.scale_factor()
        if use_pieces:
            cov_pieces = {}
#        cl_obs = cl_dict['']
        for a,b,_ in obs_cov:
            row = []
            for d,e,_ in obs_cov:
                c1 = np.array(corr[a,d])
                c2 = np.array(corr[b,e])
                c3 = np.array(corr[a,e])
                c4 = np.array(corr[b,d])

                if not (((a,b) in obs_exp) and ((d,e) in obs_exp)):
                    continue

                observ1 = np.array(list(zip(*obs_exp[a,b])))
                observ2 = np.array(list(zip(*obs_exp[d,e])))

                empty_cov = (observ1.ndim == 1 or observ2.ndim == 1)
                if empty_cov:
                    continue

                args = ['cov_mat', 'observ1', 'observ2', 'nl', 'c1','c2','c3','c4'],
                cov_mat = np.zeros((observ1.shape[1], observ2.shape[1]))
                weave.inline(code_cov, *args, type_converters=converters.blitz)
                cov_mat = r*cov_mat

                if self.conf['opts.cov_diag']:
                    if a == b == d == e:
                        cov_mat = np.diag(np.diag(cov_mat))
                    else:
                        cov_mat = np.zeros_like(cov_mat)

                row.append(cov_mat)
                if use_pieces:
                    cov_pieces[a,b,d,e] = cov_mat 

            # In case of empty rows.
            if row:
                res.append(np.hstack(row)) 

        if use_pieces:
            return cov_pieces

        if res:
            cov_mat = np.vstack(res)
        else:
            cov_mat = np.array([])

        return cov_mat


    def var(self, par, corr, observD, nl):
        """Calculate the variance."""

        # If something breaks here it is because of passing an old version
        # of observD.

#        ipdb.set_trace()
#        u = corr['u']
        obs_cov = observD['obs_cov']
#        obs_exp = observD['obs_exp'][u]
        obs_exp = observD['obs_exp'].values()[0] # This line is retarded.

        # Weave need a Python float instead of a numpy float.
        nl = float(nl)
        res = []

        r = self.scale_factor()
        var = {}
        for a,b,_ in obs_cov:
            row = []
            d = a
            e = b
            c1 = np.array(corr[a,d])
            c2 = np.array(corr[b,e])
            c3 = np.array(corr[a,e])
            c4 = np.array(corr[b,d])

            observ = np.array(list(zip(*obs_exp[a,b])))
            empty_var = observ.ndim == 1
            if empty_var:
                continue

            print('in var')
            args = ['var_mat', 'observ', 'nl', 'c1','c2','c3','c4'],
            var_mat = np.zeros((observ.shape[1]))
            weave.inline(code_var, *args, type_converters=converters.blitz)
            var_mat = r*var_mat


            var[a,b] = var_mat 

        return var

    __call__ = cov
