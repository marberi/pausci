#!/usr/bin/env python
# encoding: UTF8

import pdb
import itertools as it
import numpy as np

def obsexp(nbins, observD):
    """Needed when wanting to calculate the covariance of all the
       bins.
    """

    # For this particular purpose the ordering of the observables matter.
    # Later the covariance is tranformed from 2 to 4 indices assuming
    # the ordering used here.

    new_exp = {}
    for obs1, obs2, _ in observD['obs_cov']:
        f1, p1 = obs1
        f2, p2 = obs2

        n1 = nbins[p1]
        n2 = nbins[p2]
        new_exp[obs1,obs2] = list(it.product(range(n1), range(n2)))

    observD['obs_exp'] = new_exp

    return observD

def to4d(nbins, cov_2d):
    """Convert from using the obervable numbers to the 4 redshift bins
       as indices. Re
    """


    cov_4d = {}
    xcov_4d = {}
    for key, cov_part in cov_2d.iteritems():
        (_, pop1), (_, pop2), (_, pop3), (_, pop4) = key
        shape = (nbins[pop1], nbins[pop2], nbins[pop3], nbins[pop4])

        cov_4d[key] = cov_part.reshape(*shape)

        s1 = shape[1]
        p2 = (nbins[pop3], nbins[pop4])
        tmp = np.zeros(shape)
        for i in range(cov_part.shape[0]):
            n, m = i / s1, i % s1 #-1
            tmp[n,m] = cov_part[i].reshape(p2)

    return cov_4d
