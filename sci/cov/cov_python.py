#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function

import pdb
import sys
import time
import numpy as np

class cov_python(object):
    """Calculate the covariance using only Python. In practive not
       used. It can be used to benchmark how fast the Python code
       is or have a solution only based on Python.
    """

    def __init__(self, conf):
        self.conf = conf

    def cov(self, par, corrD, observD, nl, use_pieces=False):
        """Pure Python function to calculate the covariance matrix.
           Only adapted to one population to test py3k code.
        """

        assert len(observD['obs_cov']) == 1
        obs1,obs2,f = observD['obs_cov'][0]
        corr = corrD[(obs1,obs2)]

        obs_exp = observD['obs_exp']
        observ = obs_exp[(obs1,obs2)]

        nr_obs = np.shape(observ)[0]
        cov_matrix = np.zeros((nr_obs, nr_obs))

        for n1, (i,j) in enumerate(observ):
            for n2, (m,n) in enumerate(observ):
                cov_matrix[n1,n2] = corr[i,m]*corr[j,n] + corr[i,n]*corr[j,m]

        cov_matrix = cov_matrix / nl

        return np.matrix(cov_matrix)

    __call__ = cov
