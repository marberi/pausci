#!/usr/bin/env python
# encoding: UTF8
# Interface for caching and loading Bessel function splines.

import os
import pdb
import time
import numpy as np
import scipy
from scipy.interpolate import splrep
from scipy.special import sph_jn, jv
import tables

import sci

def rsd_factors(l):
    """See eq. 21 in arxiv: 1004.4640"""

    L1 = -l*(l-1.)/((2*l-1)*(2*l+1))
    L2 = (2.*l**2.+2*l-1.)/((2*l+3)*(2*l-1.))
    L3 = -(l+1.)*(l+2)/((2*l+1)*(2*l+3))

    return L1,L2,L3

def gen_jv(lvals, kr):
    """Calculate the values used for the splines."""

    pre = np.sqrt(0.5*np.pi/kr)
    for l in lvals:
        L1,L2,L3 = rsd_factors(l)

        jv_cen = pre*jv(l+0.5, kr)
        jv_mod = pre*(L1*jv(l-1.5, kr) + L2*jv(l+0.5, kr) + L3*jv(l+2.5,kr))

        yield l, jv_cen, jv_mod

def create_cache(file_path, kr):
    """Evaluate the spherical Bessel function and save it to a HDF5 file."""

    print('Caching bessel functions. No need to panick. It will only be done once.')

    fb = tables.open_file(file_path, 'w')
    gr_clust = fb.create_group('/', 'clust')
    gr_rsd = fb.create_group('/', 'rsd')

    scipy.seterr(under='ignore')
    pre = np.sqrt(0.5*np.pi/kr)
    lvals = range(sci.config.general['jvspl_lmax'])
    for l, jv_cen, jv_mod in gen_jv(lvals, kr):
    	print('l', l)
        fb.create_array('/clust', 'l'+str(l), np.vstack([kr, jv_cen]))
        fb.create_array('/rsd', 'l'+str(l), np.vstack([kr, jv_mod]))
 
    fb.close() 

def read_cache(file_path, lvals, order):
    """Import the relevant lvalues from a file."""

    fb = tables.open_file(file_path)

    jv_spls = {}
    jvmod_spls = {}

    def read_node(grp_name, lvals):
        ans = {}
        for l in lvals:
            node_name = '/{0}/l{1}'.format(grp_name, l)
            node = fb.get_node(node_name)

            kr, y_val = node.read()
            ans[l] = splrep(kr, y_val, k=order)

        return ans

    try:
        jv_spls = read_node('clust', lvals)
        jvmod_spls = read_node('rsd', lvals)
    except tables.exceptions.NoSuchNodeError:
        raise ValueError('The Bessel cache does not include all lvalues needed.') 

    fb.close()

    return jv_spls, jvmod_spls

def without_cache(lvals, kr, order):
    """Construct the Bessel splines without using the cache.
       Useful when not having enough disk space or wanting to
       avoid transferring the cache.
    """

    jv_spls, jvmod_spls = {}, {}
    for l, jv_cen, jv_mod in gen_jv(lvals, kr):
        print('l', l)
        jv_spls[l] = splrep(kr, jv_cen, k=order)
        jvmod_spls[l] = splrep(kr, jv_mod, k=order)

    return jv_spls, jvmod_spls

def spls(lvals, kr, order=3):
    """Create splines of jn for selected lvalues."""

    import sci
    if sci.config.general['cache_bessel']:
        cachedir = sci.config.paths['cachedir']
        file_path = os.path.join(cachedir, 'bessel', 'the_only.hdf5')
        if not os.path.exists(file_path):
            create_cache(file_path, kr)

        return read_cache(file_path, lvals, order)
    else:
        return without_cache(lvals, kr, order)
