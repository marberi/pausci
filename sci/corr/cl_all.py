#!/usr/bin/env python
# encoding: UTF8

# Module for gluing together different contributions to
# the correlations. If you think this code can be improved,
# then I totally agree. It started with only count-count
# correlations and then more was added. Later the exact 
# calculations was added. Those have the potential of 
# unifying all the calculations of correlations. At the
# present time 2012-11-20 this work is not finished. I
# have finished the code and ran some forecasts. We still
# need to understand the results better to move away from
# the Limber approximation. There has also been progress
# in implementing the Limber approximation using parts of
# the exact calculations.

# TODO: When the code progress consider completely removing
# this code.

from __future__ import print_function, unicode_literals
import copy
import functools
import pdb
import sys
import numpy as np
from scipy.interpolate import splev, splrep

import itertools
from sci.corr import cl_gg
from sci.corr import cl_gk
from sci.corr import cl_kk
from sci.corr import combine_cls
from sci.corr import interval
from sci.corr import lens

class cl(cl_gg.cl_gg, cl_gk.cl_gk, cl_kk.cl_kk,
         combine_cls.combine_cls, object):

    def __init__(self, bias, conf, cosmo, pop, observ):
        self.bias = bias
        self.conf = conf
        self.cosmo = cosmo
        self.pop = pop

        self.lens = lens.lens(conf, cosmo, pop)
        self.c_cache = {}

        assert self.pop['meta']['fourier']
        assert not self.conf['opts.use_nl']

    def c_part(self, par, l, obs, observD):
        """Correlation of overdensities of galaxy counts and/or
           galaxy magnitudes. The same formulas apply in both
           cases.
        """

        # Caching. I tried using some more fancy decorators and
        # it failed badly.
        param_hash = par['hash']
        cache_key = (obs, l, param_hash)

        if cache_key in self.c_cache:
            return copy.deepcopy(self.c_cache[cache_key])

        type1, pop1, type2, pop2 = obs
        TT = (type1, type2)
        A = (type1, pop1)
        B = (type2, pop2)

        if TT == ('shear', 'shear'):
            ans = self.cl_kk(par, l, A + B, observD)
        elif A == B:
            part1 = self.cl_gg(par, l, A + B, observD)
            ans = np.triu(part1) + np.triu(part1, 1).T
        else:
            use_auto = False if 'shear' in TT else True
            part1 = self.cl_gg(par, l, A + B, observD, use_auto)
            part2 = self.cl_gg(par, l, B + A, observD, use_auto)

            tolens = self.lens.cache[par['hash']]['tolens'][(pop1, pop2)]
            test = np.logical_not(tolens)
            ans = np.where(test, part2.T, part1)

        # Then we cache the result.
        self.c_cache[cache_key] = copy.deepcopy(ans)

        return ans

    def create_iter(self, par, observD):

        u_mean = observD['u_mean']
        for u in observD['u_mean']:
            yield self.combine_cls(par, u, observD)

#        pdb.set_trace()

    def __call__(self, fid, u, observD):
        # For some reason Python3 require this.
        return self.combine_cls(fid, u, observD)
