#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np

from sci.corr import interval

class cl_gg(object):
    """Gal-gal part of the correlations.""" 

#    @profile
    def cl_gg(self, par, l, obs, observD, use_auto=True):
        """Eq.45 and 60. Create a matrix representing the two-point correlation
           function between galaxies.
        """

        type1, pop1, type2, pop2 = obs
        obs1 = type1, pop1
        obs2 = type2, pop2

#        z = self.pop[0]['z']

        # Be aware that the lensing is indendent of the
        # fluctuation type..
        par_hash = par['hash']
        cache_key = (pop1, pop2)
        lensing_eff = self.lens.cache[par_hash]['lens_eff'][cache_key]

        tolens = self.lens.cache[par_hash]['tolens'][cache_key]

        z1 = self.pop[pop1]['z_mean']
        z2 = self.pop[pop2]['z_mean']

        P_funk = self.cosmo.P_funk(par, l, pop1, z1)

        #c_auto = np.diag(P_funk/width)

        width1 = self.pop[pop1]['z_width']
        c_auto = np.diag(P_funk/width1)

        # Bias also has a stochastich part
        k1 = self.cosmo.k(par, z1, l)
        k2 = self.cosmo.k(par, z2, l)
        bias1 = self.bias.mean(par, obs1, z1, k1, use_diag=True)
        bias2 = self.bias.mean(par, obs2, z2, k2, use_diag=True)
        stoc1 = self.bias.stoc(par, obs1, z1, k1, use_diag=True)

        alpha1 = self.pop[pop1]['alpha'][type1]
        alpha2 = self.pop[pop2]['alpha'][type2]

        za1 = self.pop[pop1]['za']
        zb1 = self.pop[pop1]['zb']
        za2 = self.pop[pop2]['za']
        zb2 = self.pop[pop2]['zb']

        _, inv = interval.calc_overlap_inv(za1, zb1, za2, zb2)
        eff_upper = np.where(tolens, lensing_eff, 0)

        c_ii = (bias1*P_funk*inv.T).T*bias2 
        c_ij = (bias1*stoc1*P_funk*eff_upper.T).T * alpha2
        part1 = c_ii + c_ij if use_auto else c_ij

        if self.conf['opts.ckk_included']:
            cl_kk = self.cl_kk(par, l, obs, observD)
            res = part1 + (alpha1*cl_kk.T).T*alpha2
        else:
            res = part1

        res = np.triu(res)

        return res
