#!/usr/bin/env python
# encoding: UTF8

# This code is actually no longer needed. If the last changes are
# OK, then I can delete this code.
# TODO: Wait a bit and remove this file!!!

import pdb
import numpy as np

class cl_gk(object):
    def cl_gk(self, par, l, obs, observD):
        """Eq. ? notes3. Galaxy-shear observables."""

        type1, pop1, type2, pop2 = obs
        obs1 = type1, pop1

        z1 = self.pop[pop1]['z']
        k1 = self.cosmo.k(par, z1, l)
        bias1 = self.bias.mean(par, obs1, z1, k1, use_diag=True)
        P_funk = self.cosmo.P_funk(par, l, pop1, z1)

        par_hash = par['hash']
        cache_key = (pop1, pop2)
        lensing_eff = self.lens.cache[par_hash]['lens_eff'][cache_key]

        #lensing_eff = self.cache['lens_eff'][(par['hash'], pop1, pop2)]

        term1 = (bias1*P_funk*np.triu(lensing_eff, 1).T).T

        if False:
            alpha1 = self.pop[pop1]['sj'][type1]
            term2 = (alpha1*c_kk.T).T


        return term1
