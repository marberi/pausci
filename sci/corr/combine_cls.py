import copy
import pdb
import itertools as it
import numpy as np

class combine_cls(object):
    def find_obs_c(self, obs_cov):
        """Array with the different fluctions."""

        obs_c = []
        for obs1, obs2, m in obs_cov:
            if not obs1 in obs_c: obs_c.append(obs1)
            if not obs2 in obs_c: obs_c.append(obs2)

        return obs_c

    def combine_cls(self, par, l, observD):
        obs_cov = observD['obs_cov']
        obs_c = self.find_obs_c(obs_cov)
        cl_parts = {}

        cl_act = {}
        for obs1, obs2 in it.product(obs_c, obs_c):
            obs = obs1 + obs2
            corr_act = self.c_part(par, l, obs, observD)
            cl_act[(obs1, obs2)] = corr_act.copy()

        cl_dict = {'u': l, 'corr_act': cl_act}

        return cl_dict

    def w(self, par, ang, observD):
        raise NotImplementedError('Use the exact calulations, -o module.corr="exact"')

