#!/usr/bin/env python
# encoding: UTF8

# Extra term in the lensing-lensing contribution.
import pdb
import numpy as np
import sys
import time
import itertools as it

from sci.lib import libcc

class ex_kappamod(object):
    """Small correction term to kappa-kappa."""

    def _kappa_mod_precalc(self):
        """Terms used for calculation kappa modifications which is
           independent of cosmology.
        """

        pop = self.pop['meta']['pop']
        zmin_conf = [self.pop[x]['za'][0] for x in pop]
        assert len(set(zmin_conf)) == 1, 'Different zmin for the different populations.\
currently this is not implementer in the kappa modification term. Not much remains for\
doing so, but its not done.'

        # These two are actually configuration...
        ZMIN = 0.001
        N = 10

        D = libcc.find_D(N)
        d = libcc.find_d(N)
        w = np.dot(d.T, D)

        z_minD = {}
        z_bigD = {}
        width_modkappaD = {}
        w_lens_modkappaD = {}
        nl = len(self.lvals)
        for popid in self.all_pop:
            zmax = self.pop[popid]['za'][0]
            ker1,ker2 = libcc.find_ker(N, ZMIN, zmax)
            ker = np.hstack([ker1, ker2])
            z_minD[popid] = ker
            z_bigD[popid] = np.tile(ker, nl)

            width = zmax - ZMIN 
            width_modkappaD[popid] = width
            w_lens_modkappaD[popid] = 0.5*width*np.hstack([w,w])

        self.z_minD = z_minD
        self.z_bigD = z_bigD
        self.width_modkappa = width_modkappaD

        self.w_lens_modkappaD = w_lens_modkappaD


    def _kappa_mod_terms(self, par):
        """Calculate terms central when doing the correction in the 
           kappa-kappa term.
        """

        lensD = {}
        funkD = {}
        nl = len(self.lvals)
        for popid in self.all_pop:
            z = self.z_minD[popid]
            z_big = self.z_bigD[popid]
            k_big = np.hstack(self.cosmo.k(par, z, l) for l in self.lvals)

            # P_funk code...
            power_spec = self.P(par, z_big, k_big, True)
            power_spec = power_spec.reshape((nl, len(z)))
            r = self.cosmo.cache_ev(par, 'r', z)
            r_der = self.cosmo.cache_ev(par, 'r_der', z)
            funkD[popid] = np.sqrt(power_spec/(r**2*r_der))

#            # First index: sources, Second index: Lenses. This comes from
#            # multiplying the window functions on the left side.
            lensD[popid] = self.lens.lensing_eff_core(par, z, self.zD[popid])

        alphaD = self.alphaD()
        win_lensD = {}
        for obs in self.all_obs:
            ftype, popid = obs

            w_z = self.w_redshiftD[popid]
            w_lens = self.w_lens_modkappaD[popid] 
            win_lensD[obs] = np.dot(w_z, w_lens*(alphaD[obs]*lensD[popid]).T)

        return funkD, win_lensD

    def find_kappa_mod(self, par):
        """Generator returning corretion terms to kappa-kappa."""

        funkD, win_lensD = self._kappa_mod_terms(par)

        npop = len(self.all_pop)
        nobs = len(self.all_obs)

        for x,l in enumerate(self.lvals):
            ans = {}
            for i,j in it.product(range(nobs), range(nobs)):
                if i < j: continue

                obs1 = self.all_obs[i]
                obs2 = self.all_obs[j]
                _, popid1 = obs1
                _, popid2 = obs2

                clx1 = (win_lensD[obs1]*funkD[popid1][x]).sum(axis=1)
                clx2 = (win_lensD[obs2]*funkD[popid2][x]).sum(axis=1)
                over = 1./self.width_modkappa[popid1]

                ans[obs1,obs2] = np.outer(clx1*over, clx2.T)
                ans[obs2,obs1] = ans[obs1,obs2].T
 
            yield l, ans

    def add_kappa_correction(self, par, l_in, cl_in, corr_iter):
        """Add the correction term to the ordinary Cls. Introduced just to hide
           the logic.
        """

        if not (self.conf['opts.kappa_mod'] and self.conf['opts.use_lens']):
            return cl_in

        l_mod, cl_mod = corr_iter.next()
        assert l_mod == l_in, 'Trying to add kappa-modifications for the wrong l-values.'
        t1 = time.time()

        for key in cl_in.keys():
            cl_in[key] += cl_mod[key]

        return cl_in 
