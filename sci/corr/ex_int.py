#!/usr/bin/env python
# encoding: UTF8

import itertools as it
import pdb
from scipy.interpolate import splev
import numpy as np

nr_bes = 0

class ex_int(object):
    """Integrand to evaluated the expression."""

    def iter_bessel(self, ker_rkD):
        ker_flatD = {}
        for pop in self.all_pop:
            ker = ker_rkD[pop]
            nk,nr = ker.shape
            ker_flatD[pop] = ker.reshape(nk*nr)

        use_rsd = self.conf['opts.use_rsd']
        for l in self.ex_lvals:
            liC = self.lmap[l]
            j_mat, j_r = {}, {}

            for pop in self.all_pop:
                ker_flat = ker_flatD[pop]
                shape = ker_rkD[pop].shape
                j_mat[pop] = splev(ker_flat, self.jv_spl[l], ext=2).reshape(shape)
                if use_rsd:
                    j_r[pop] = splev(ker_flat, self.jvmod_spl[l], ext=2).reshape(shape)

            yield j_mat, j_r

    def select_scales(self, N):
        """Scales to use in the integration."""
        
        assert not N % 2
        n = np.arange(N/2+1)
        ker = np.cos(n*np.pi/N)
        ker1D, ker2D, yD, y1D, y2D = {},{},{},{},{}

        if self.conf['opts.int_logk']:
            kmin = np.log(self.kmin)
            kmax = np.log(self.kmax)
        else:
            kmin = self.kmin
            kmax = self.kmax

        k0 = 0.5*(kmin+kmax)
        kw = 0.5*(kmax-kmin)
#        ker1_k = kmax+.5*(ker-1)*(kmax-kmin)
#        ker2_k = kmax+.5*(-ker-1)*(kmax-kmin)
        ker1_k = k0 + kw*ker
        ker2_k = k0 - kw*ker

        ker_k = np.hstack([ker1_k, ker2_k])

        if self.conf['opts.int_logk']:
            ker_k = np.exp(ker_k)
            ker1_k = np.exp(ker1_k) 
            ker2_k = np.exp(ker2_k) 

        ker_k = np.hstack([ker1_k, ker2_k])
        
        return ker_k

    def find_pre(self, par, ker_k):
        """Part of the integration."""
 
        w_sc = np.sqrt(self.w_scale)
        w_sc_new = np.tile(w_sc, 2)

        p = 1.5 if self.conf['opts.int_logk'] else 1.
        preD = {}
        for pop in self.all_pop:
            z = self.zD[pop]
            r = self.cosmo.cache_ev(par, 'r', z)

            ker = np.outer(r, ker_k)
            pre = np.sqrt(self.P(par, z, ker_k))*(ker_k**p)*w_sc_new
            preD[pop] = pre

        return preD

    def psi(self, par, N):
        """Array y in the Clenshaw-Curtis integration."""

        np.seterr(under='ignore')

        zD = self.zD
        
        ker_k = self.select_scales(N)
        ker_kD = {}
        ker_rkD = {}

        for pop in self.all_pop:
            ker_kD[pop] = ker_k
            z = zD[pop]
            r = self.cosmo.cache_ev(par, 'r', z)
            ker_rkD[pop] = np.outer(r, ker_k)

        use_rsd = self.conf['opts.use_rsd']
        biasD = self.biasD(par, ker_kD)
        fD = self.fD(par, self.zD, ker_kD)
        preD = self.find_pre(par, ker_k)

#        pdb.set_trace()
        # Iterates of the different vlaues
        jiter = self.iter_bessel(ker_rkD)

        has_rsd = self.conf['opts.has_rsd']
        psi_mat, psi_gal = {}, {}
        for j_mat, j_r in jiter:
            for obs in self.all_obs:
                ftype,pop = obs

                if use_rsd and has_rsd[ftype] and has_rsd[pop]:
                    psi_r = fD[pop]*j_r[pop]
                    psi_gal[obs] = preD[pop]*(biasD[obs]*j_mat[pop] + psi_r)
                else:
                    psi_gal[obs] = preD[pop]*biasD[obs]*j_mat[pop]

                psi_mat[pop] = preD[pop]*j_mat[pop]

            yield psi_mat, psi_gal
