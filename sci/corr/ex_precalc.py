#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import itertools as it
import numpy as np
from scipy.integrate import simps
from scipy.linalg import block_diag
from scipy.special import erf
from scipy.stats import norm

from sci.lib import libcc
from sci.corr import libbessel_spl
from sci.corr import libclconv

class ex_precalc(object):
    def find_bins(self, N_bin, za, zb):
        # TODO: Rename this function since its quite misleading..

        n = np.arange(N_bin/2+1)
        ker = np.cos(n*np.pi/N_bin)

        # Some points are repeated, but trying to optimize would
        # lead to more complicated code..
        z_lists = []
        for xmin,xmax in zip(za,zb):
            z_lists.append(xmax + .5*(ker-1.)*(xmax-xmin))
            z_lists.append(xmax + .5*(-ker-1.)*(xmax-xmin))

        ans1 = np.hstack(z_lists)

        return ans1, None

    def find_zD(self, all_pop):
        zD = {}
        widthD = {}
        for pop in all_pop:
            N_bins = self.conf['opts.N_bins.{0}'.format(pop)]
            za = self.pop[pop]['za']
            zb = self.pop[pop]['zb']

            x, width = self.find_bins(N_bins, za, zb)
            zD[pop] = x
            widthD[pop] = width

        return zD, widthD

    def repeat_weighted(self, weight, a):
        """Repeat array a twice for each weight."""

        # Hint: It might look convoluted. I use an outer
        # product to save some loops.
        weight_twice = np.repeat(weight, 2)
        A = np.outer(weight_twice, a)

        return np.hstack(A.tolist())
 
    def w_lensing(self):
        """Weight function used when finding the lensing
           contributions.
        """

        # The width  comes from the CC integration.
        w_lensD = {}
        for pop in self.all_pop:
            N = self.conf['opts.N_bins.{0}'.format(pop)]
            d = libcc.find_d(N)
            D = libcc.find_D(N)
            w = np.dot(d.T, D)

            width = self.pop[pop]['z_width']
            w_lensD[pop] = 0.5*self.repeat_weighted(width, w)

        return w_lensD

    def w_scale(self):
        """Weights when integrating over scale."""

        nk = self.conf['opts.nk']
        kmin = self.conf['opts.kmin']
        kmax = self.conf['opts.kmax']

        if self.conf['opts.int_logk']:
            #kmin = np.log(self.kmin)
            #kmax = np.log(self.kmax)
            pre = (np.log(kmax) - np.log(kmin))/2.
        else:
            pre = (kmax - kmin)/2.

        D = libcc.find_D(nk)
        d = libcc.find_d(nk)

        w = pre*np.dot(d, D)

        return w

    def selection(self, pop_name):
        """Selection function."""


        pop = self.pop[pop_name]
        bin_type = self.conf['fid.pop.{0}.bin_type'.format(pop_name)]
        N_bins = self.conf['opts.N_bins.{0}'.format(pop_name)]
        za = pop['za']
        zb = pop['zb']

        toev = []
        for zai,zbi in zip(pop['za'], pop['zb']):
            ker1, ker2 = libcc.find_ker(N_bins, zai, zbi)
            toev.append(np.hstack([ker1,ker2]))

        assert not bin_type == 'gauss', NotImplementedError('I removed support for this. Use packages instead.')

        # Top-hat bins.
        thr_smooth_nz = ((not self.pop.pkg) and self.conf['opts.trans_mat'])
        data_tophat = 'top_hat' in self.pop[pop_name] and self.pop[pop_name]['top_hat']

        # Hacked in support for overriding the photo-z in the input files. Useful for
        # testing.
        data_tophat = data_tophat or self.conf['fid.pop.{0}.use_tophat'.format(pop_name)]

        if thr_smooth_nz or data_tophat:
            nbins = pop['nbins']
            return block_diag(*[np.ones(len(x)) for x in toev])

        order = ['cat_kde', 'nz_spls']
        pop_keys = set(self.pop[pop_name].keys())
        assert set(order) & pop_keys, 'No photoz information specified.'
        for input_type in order:
            if input_type in pop_keys:
                pz_input = self.pop[pop_name][input_type]
                break


        import scipy
        scipy.seterr(under='ignore')

        # TODO: Move this to another function.
        x = np.linspace(za[0], zb[-1], 400)
        A = np.zeros(len(pz_input))
        
        if input_type == 'nz_spls':
            for i,nz_smooth in enumerate(pz_input):
                y = nz_smooth(x)
                A[i] = simps(y, x)
        elif input_type == 'cat_kde':
            for i,nz_smooth in enumerate(pz_input):
                A[i] = nz_smooth.integrate_box_1d(za[0], zb[-1])

#        pdb.set_trace()

        # Underflow is OK here...
        # TODO: Reset the error again??
        import scipy
        scipy.seterr(under='ignore')

        res = []
        width = pop['z_width']
        for i,nz_smooth in enumerate(pz_input):
#            f = 1./kde.integrate_box_1d(za[0], zb[-1])
            f = 1./A[i]

            res.append(f*np.hstack([nz_smooth(X)*wi for X,wi in zip(toev, width)]))

        phi = np.vstack(res)
        return phi

    def w_redshift(self):
        """Weights when integrating over redshift."""

        # Currently the same resultion is used for all 
        # redshift bins.
        w_redshiftD = {}

        print('w_redshifts')
        # The division of 2 is the remainder from normalization
        # over redshift and a prefactor in the CC integration.
        for pop_name in self.all_pop:
            N_bins = self.conf['opts.N_bins.{0}'.format(pop_name)]
            D = libcc.find_D(N_bins)
            d = libcc.find_d(N_bins)
            w = np.dot(d, D)

            pop = self.pop[pop_name]
            nbins = pop['nbins']
#            Cnbins = pop['Cnbins']
            Cnbins = nbins

            print('before calling..')
            sel = self.selection(pop_name)
            w_redshiftD[pop_name] = 0.5*sel*np.tile(w, (nbins, 2*Cnbins))

        return w_redshiftD

    def _set_lvals(self, observD):
        # Definitions.
        # lvals - lvalues to return if using Fourier.
        # calc_lcals - lvalues to calculte.
        # ex_lvals - lvalues calculated using the exact calculations.
        # limber_lvals - lvalues calculated using the limber equation.

        use_fourier = self.pop['meta']['fourier']
        if use_fourier:
            l_mean, _ = self.observ.ubins(self.conf.fid)
            lvals = np.round(l_mean).astype(np.int)

            # This removal should not really be needed. Instead pass the
            # lbins which is used through observD.
            used_lvals = set(observD['df']['u_mean'].drop_duplicates())
            lvals = [x for x in lvals if x in used_lvals]

        linterp = True if not use_fourier else False


        if linterp and use_fourier:
            calc_lvals = libclconv.camb_lvals(lvals)
        elif linterp and not use_fourier:
            calc_lvals = libclconv.camb_lvals()
        elif self.pop['meta']['fourier']:
            calc_lvals = l_mean
        else:
            raise ValueError('For w(theta) interpolation in l is required.')

        if not use_fourier:
            th_mean, th_width = self.observ.ubins(self.conf.fid)
            self.th_mean = th_mean

            maxl = 5000
            self.S = libclconv.find_S(th_mean, th_width, maxl, self.conf['opts.int_angle'])

        lsplit = self.conf['opts.lsplit']
        if self.conf['opts.use_limber']:
            self.ex_lvals = calc_lvals[calc_lvals < lsplit]
            self.limber_lvals = calc_lvals[lsplit <= calc_lvals]
        else:
            self.ex_lvals = lvals
            self.limber_lvals = np.array([])

#        ipdb.set_trace()

    def _intr_mask(self, all_pop):
#        def outer_sum(A,B
        mask = {}

        intr_lim = self.conf['opts.intr_lim']
        for pop1 in all_pop:
            z1 = self.pop[pop1]['z_mean']
            nbins1 = len(z1)
            for pop2 in all_pop:
                z2 = self.pop[pop2]['z_mean']
                nbins2 = len(z2)

                outer_sum = np.tile(z1, (nbins2,1)).T - np.tile(z2, (nbins1,1))

                mask[pop1, pop2] = intr_lim < np.abs(outer_sum)

        return mask

    def _precalc(self, observD):
        """Precalculate quantities independent on cosmology. Useful when sampling."""

        all_fluct = observD['all_fluct']
        all_pop = observD['all_pop']

        self._set_lvals(observD)

#        if self.conf['opts.intr_rem']:
        self.mask = self._intr_mask(all_pop)

        self.kmin = self.conf['opts.kmin']
        self.kmax = self.conf['opts.kmax']
        kr_min = self.conf['opts.kr_spl_min']
        kr_max = self.conf['opts.kr_spl_max']
        dkr = self.conf['opts.kr_spl_res']

        kr = np.arange(kr_min, kr_max, dkr)

        order = self.conf['opts.spl_k']
        self.jv_spl, self.jvmod_spl = libbessel_spl.spls(self.ex_lvals, kr, order)


        self.zD, self.widthD = self.find_zD(all_pop)
        self.all_fluct = all_fluct
        self.all_pop = all_pop
        self.all_obs = list(it.product(all_fluct, all_pop))

        self.w_lensD = self.w_lensing()
        self.w_scale = self.w_scale()
        self.w_redshiftD = self.w_redshift()

        self.lmap = dict((l,i) for i,l in enumerate(self.ex_lvals))

        # Currently this is needed even if we don't add the correction.
        if self.conf['opts.kappa_mod']:
            self._kappa_mod_precalc()

        self._finished_precalc = True

#        pdb.set_trace()
