#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import time
import scipy
import numpy as np
from numpy.polynomial.legendre import legvander
from scipy.interpolate import splrep, splev

from sci.lib import libcc

def camb_lvals(lvals=False):
    """lvals used in CAMB sources."""

    a1 = np.arange(2,10+1)
    a2 = np.arange(10+1, 39, 2)
    a3 = np.arange(40, 95, 5)
    a4 = np.arange(110, 150+1, 20)
    a5 = np.array([175])
    a6 = np.arange(200, 5001, 50)
    a6 = np.arange(200, 8001, 50) # TEMPORARY

    calc_lvals = np.hstack([a1,a2,a3,a4,a5,a6])

    if lvals:
        ind = np.searchsorted(calc_lvals, max(lvals), side='right') + 1
        calc_lvals = calc_lvals[:ind]
    
    return calc_lvals

def int_angles(th_mean, th_width):
    """Terms used for a Clenshaw-Curtis integral over the 
       angles.
    """

    # phi - Weights
    # mu - Value to evaluate the function.

    th_a = th_mean - 0.5*th_width
    th_b = th_mean + 0.5*th_width

#    pre = (th_b - th_a) / 2.
    pre = np.ones(len(th_b)) / 2.
    N_wbin = 50
#    N_wbin = 0

    # Weighting
    D = libcc.find_D(N_wbin)
    d = libcc.find_d(N_wbin)
    one_w = np.dot(d, D)
    two_w = np.hstack([one_w, one_w])
    phi = scipy.linalg.block_diag(*[x*two_w for x in pre])

    # Points for where to sample the legendre polynomial...
    # Note: Ker is in angles, *not* in mu. It is the distribution
    # the CC algorihm use.
    n = np.arange(N_wbin/2+1)
    ker = np.cos(n*np.pi/N_wbin)

    x = []
    for xmin,xmax in zip(th_a, th_b):
       x.append(xmax + .5*(ker-1.)*(xmax-xmin))
       x.append(xmax + .5*(-ker-1.)*(xmax-xmin))

    x_all = np.hstack(x)*(np.pi/180.)
    mu = np.cos(x_all)

    return phi, mu

def cen_angles(th_mean):
    """When using the center of the angle bins."""

    # Assuming th_mean is in degrees.
    phi = np.identity(len(th_mean)-1)
    mu = np.cos(th_mean*(np.pi/180.))

    return phi, mu

def find_S(th_mean, th_width, maxl, int_angle=True):
    """Combined cl conversion and angle integration."""

    if int_angle:
        phi, mu = int_angles(th_mean, th_width)
    else:
        phi, mu = cen_angles(th_mean)

    V = legvander(mu, maxl)
    lvals = np.arange(0, maxl+1)

    pre = (2.*lvals+1)/(4.*np.pi)
    S = V*pre

    S = np.dot(phi, S)

    return lvals, S

def _cl_flatten(obs_cov, cl):
    """Flatten cl."""

    res = []
    for obs1,obs2,_ in obs_cov:
        res.append(cl[obs1,obs2].flatten())

    return np.hstack(res)

def conv_to_flat(observD, cls, lvals):
    """Flatten the Cls for all observables and l values."""

    obs_cov = observD['obs_cov']
    res = []
    
    for l in lvals:
        res.append(_cl_flatten(obs_cov, cls[l]))

    return np.vstack(res)

def conv_to_fluffy(observD, w_flat, angles, cls):
    """Convert the flat format to a fluffy one."""

    obs_exp = observD['obs_exp']
    nelem = []
    shapes = []
    m = [] 
    c1 = cls.itervalues().next()
    for obs1, obs2,_ in observD['obs_cov']:
        shape = c1[obs1,obs2].shape
        nelem.append(np.product(shape))
        shapes.append(shape)
        m.append((obs1, obs2))

    res = dict((x, {}) for x in angles)

    nelem_cum = np.cumsum(nelem)
    A = np.split(w_flat, nelem_cum, axis=1)
    for (obs1, obs2), Ai, shape in zip(m, A, shapes):
        for j, ang in enumerate(angles):
            res[ang][(obs1,obs2)] = Ai[j].reshape(shape)


    return res

def new_try(cl_interp, angles):
    # Likely written during the DES meeting to debug some problem.
    shape = cl_interp.shape
    mu = np.cos(angles*(np.pi/180.))
   
    W = np.zeros((len(angles), shape[0]))
    for i in range(len(angles)):
        P = legvander(mu[i], shape[1])[0]
        for j in range(shape[0]):
            for l in range(shape[1]):
                print(i, j, l)
                W[i,j] += (2*l + 1)*P[l]*cl_interp[j,l]

    W = W/(4.*np.pi)

    return W

    pdb.set_trace()


def cl_to_w(observD, S, angles, cls):
    """Convert cl to w(theta)."""

    new_l, S = S

    lvals = cls.keys()
    lvals.sort()
 
    cls_new = conv_to_flat(observD, cls, lvals)

    ncorr = cls_new.shape[1] 
    cl_interp = np.zeros((ncorr, len(new_l)))

    for i in range(ncorr):
        spl = splrep(lvals, cls_new[:, i])
        cl_interp[i] = splev(new_l, spl)

    if False:
        from matplotlib import pyplot as plt
        plt.plot(lvals, cls_new[:,0], 'o-', label='Input cls')
        plt.plot(new_l, cl_interp[0], label='Interpolated values')
        plt.loglog()
        plt.legend()
        plt.show()
    
#    w2 = new_try(cl_interp, angles)
    w_flat = np.dot(S, cl_interp.T)

    #plt.plot(angles, angles**2*w_flat[:,0], label='Right after conversion')
    if False:
        plt.plot(angles, w_flat[:,0], 'o-', label='Right after conversion')
        plt.loglog()
        plt.legend() 
        plt.show()
#    plt.plot(
#    pdb.set_trace()

    w = conv_to_fluffy(observD, w_flat, angles, cls)

    return w

