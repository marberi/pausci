import numpy as np
import combine_cls

raise NotImplementedError, "Check opts.pop move!"

class cl_camb(combine_cls.combine_cls, object):

    def __init__(self, bias, conf, cosmo, pop):
        self.bias = bias
        self.conf = conf
        self.cosmo = cosmo
        self.pop = pop

    def c_part(self, par, l, obs, changed_par=False, no_trans=False):
        # Some old code for caching copied.

        type1, pop1, type2, pop2 = obs

        if not hasattr(self, 'c_cache'):
            self.c_cache = {}

        key = (obs, l, changed_par)
        if key in self.c_cache:
            to_copy = self.c_cache[key]
            new_copy = {}
            for key,val in to_copy.items():
                new_copy[key] = val.copy()

            return new_copy #self.c_cache[key].copy()


        msg_pop = 'Only works for one populations yet...'
        assert len(self.pop['meta']['pop']) == 1, msg_pop

        msg_de = 'It only supports a cosmological constant.'
        assert not set(['w0', 'wa']) & set(self.conf['opts.params']), msg_de

        popid = self.pop['meta']['pop'][0]
        z = self.pop[popid]['z']
        width = self.pop[popid]['width'] 
        bias = self.bias(par, z, popid, l)
      
#        width = 2.* width  # HACK 
        msg_bins = 'We limit the number of bins to 10 by now.'
        assert len(z) <= 10, msg_bins

        # CAMB wants the bin order reversed...        
        indx = range(len(z)-1, -1, -1)
        z_rev = z.take(indx)
        width_rev = width.take(indx)
        bias_rev = bias.take(indx)
        
       
        camb_input = {'H0': par['h']*par['H_0'],
                      'omegab': par['om_b'],
                      'omegac':  par['om_m'] - par['om_b'],
                      'omegav': par['om_de'],
                      'scalar_index': par['ns'],
                      'scalar_amp': 2.1e-9}
#                      'w': par['w0']}

        # Setting the right amplitude.
        # http://cosmocoffee.info/viewtopic.php?t=475&sid=f1de7ce54932d23c46bf97866fa8fa35
        import pycamb
#        a = pycamb.transfers(**camb_input)
#        camb_input['scalar_amp'] *= (par['sigma8']/a[2][0])**2

        cls_out = pycamb.cls(z_rev, width_rev, bias_rev, **camb_input)
        del pycamb  

        ans = cls_out[l-1]
        ans = ans.take(indx, 0)
        ans = ans.take(indx, 1)

        # Bin transition formalism.
        if self.conf['opts']['syst'] and (not no_trans):
            ans = self.add_system(par, ans, l, pop1, pop2)

        else:
            n = np.shape(ans)[0] # HACK          
            A = np.zeros((n,n))
            B = np.zeros((n,n))

            ans = {'cl_act': ans, 'cl_obs': ans.copy(), 'A': A, 'B': B}

        to_cache = {}
        for key, val in ans.items():
            to_cache[key] = val.copy()

        self.c_cache[key] = to_cache

       
        return ans
