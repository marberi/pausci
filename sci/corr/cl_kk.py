#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals

# Kappa-kappa correlations. To get a precise answer you need to 
# actually integrate.
import pdb
import time
from scipy.interpolate import splev, splrep
from scipy.integrate import trapz, quad
import numpy as np
import itertools as it

class cl_kk(object):
    def OLD_cl_kk(self, par, l, obs):
        # The reason for not deleting this code is to keep an independent
        # test of the new algorithms. I have directly compared them to
        # see they produce the same result.
        t1 = time.time()
        type1, pop1, type2, pop2 = obs
        nbins1 = len(self.pop[pop1]['z'])
        nbins2 = len(self.pop[pop2]['z'])
        zbar1 = self.pop[pop1]['z']
        zbar2 = self.pop[pop2]['z']

        chi1 = self.cosmo.cache_ev(par, 'chi', zbar1)
        chi2 = self.cosmo.cache_ev(par, 'chi', zbar2)
        r1 = self.cosmo.cache_ev(par, 'r', zbar1)
        r2 = self.cosmo.cache_ev(par, 'r', zbar2)
        r0 = par['c'] / par['H_0']

        f_K = self.cosmo.f_K

        H_spl = self.cosmo.find_spl(par, 'H')
        r_spl = self.cosmo.find_spl(par, 'r')
        r_der_spl = self.cosmo.find_spl(par, 'r_der')
        chi_spl = self.cosmo.find_spl(par, 'chi')
        D_spl = self.cosmo.find_spl(par, 'D')
        plin_spl = self.cosmo.find_spl(par, 'plin')

        def to_int(z):
            r_H = par['c'] / splev(z, H_spl) if self.conf['opts.fs_bug'] \
                  else splev(z, r_der_spl)

            k = self.cosmo.k(par, z, l)
            D = splev(z, D_spl)
#            power_spec = D**2.*self.power(par, k)
            power_spec = D**2.*splev(k, plin_spl)

            H = splev(z, H_spl)
            a = 1./(1.+z)
            chi = splev(z, chi_spl)

            part1 = (1./r_H)*(3.*par['om_m']*par['H_0']/(2.*H*a*r0))**2.
#            if self.conf['opts.h_bug']:
#                part1 = part1 * par['h']**2.

            g1 = f_K(par, chi1[i] - splev(z, chi_spl)) / r1[i]
            g2 = f_K(par, chi2[j] - splev(z, chi_spl)) / r2[j]

#            if (g1 < 0).any() or (g2 < 0).any() or (part1 <0).any():
#                import pdb; pdb.set_trace()

#            if (i==66 and j==66): pdb.set_trace()

            return part1*g1*g2*power_spec


        cl_kk = np.zeros((nbins1,nbins2))
        max_err = 0.
        for i in range(nbins1):
            for j in range(i, nbins2):
            #for j in range(nbins2):
#                Uncomment for accurate integration..
#                cl_kk[i,j] = quad(to_int, 0.001, zbar1[i])[0]


                z_interp = np.arange(0.001, zbar1[i], 0.001)
                y = to_int(z_interp)
                cl_kk[i,j] = trapz(y, z_interp) 

#        pdb.set_trace()
        cl_kk += np.triu(cl_kk, 1).T
        return cl_kk

#    @profile
    def NEW_cl_kk(self, par, l, obs, all_fluct='', all_pop=''):
        type1, pop1, type2, pop2 = obs

        nz = 10000
        z1 = self.pop[pop1]['z_mean'] 
        z2 = self.pop[pop2]['z_mean'] 
#        minz = min(min(z1), min(z2))
        minz = 0.0001
#        minz = 0.0
        maxz = max(max(z1), max(z2))
        hint = (maxz - minz)/(nz-1)
        zintrp = np.linspace(minz, maxz, nz)

#        pdb.set_trace()

        r0 = par['c'] / par['H_0']
        a = 1./(1.+zintrp)
        H = self.cosmo.cache_ev(par, 'H', zintrp)
        r = self.cosmo.cache_ev(par, 'chi', zintrp)
        r1 = self.cosmo.cache_ev(par, 'chi', z1) 
        r2 = self.cosmo.cache_ev(par, 'chi', z2)

#        pdb.set_trace()
        r_der = self.cosmo.cache_ev(par, 'r_der', zintrp)
        k = self.cosmo.k(par, zintrp, l)
        D = self.cosmo.cache_ev(par, 'D', zintrp)
#        P = self.cosmo.cache_ev(par, 'plin', k)
        plin_spl = self.cosmo.find_spl(par, 'plin')
        P = splev(k, plin_spl)

        pre = D**2*P*(1./r_der)*(3.*par['om_m']*par['H_0']/(2.*H*a*r0))**2.

#            part1 = (1./r_H)*(3.*par['om_m']*par['H_0']/(2.*H*a*r0))**2.


        f_K = self.cosmo.f_K
        outsum = lambda x, y: np.vstack([xiter + y for xiter in x])

        g1 = f_K(par, outsum(r1, -r))
        g2 = f_K(par, outsum(r2, -r))
        g1 = (g1.T/r1).T
        g2 = (g2.T/r2).T
        g1 = np.clip(g1, 0., np.inf)
        g2 = np.clip(g2, 0., np.inf)
        ans = np.dot(hint*pre*g1,g2.T)

#        pdb.set_trace()
        return ans

    def cl_kk(self, par, l, obs, observD):
        import time
        t1 = time.time()
#        ans1 = self.OLD_cl_kk(par, l, obs)
        t2 = time.time()
        ans2 = self.NEW_cl_kk(par, l, obs)
        t3 = time.time()


        return ans2
#        #assert ans1 == ans2
#
#        return ans2
