#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals
import sys
import numpy as np

def calc_overlap(za1, zb1, za2, zb2):
    """Calculate the overlap between two sets of redshift
       bins.
    """

    # Just definitions.
    land = np.logical_and
    lnot = np.logical_not
    lor = np.logical_or
    z1 = .5*(za1 + zb1)
    z2 = .5*(za2 + zb2)
    w1 = zb1 - za1
    w2 = zb2 - za2

    # K - |z_i - z_j|
    # L - .5*(w_i - w_z)
    e = np.ones((len(z1), len(z2)))
    K = np.abs((e.T*z1).T - e*z2)
    L = .5*((e.T*w1).T + e*w2)

    # p1 - Interval 1 starts after 2
    # p2 - Interval 1 ends before 2
    p1 = (e.T*za1).T >= e*za2
    p2 = (e.T*zb1).T <= e*zb2

    # Test1 - Interval 1 inside 2
    # Test2 - Interval 2 inside 1
    # Test3 - Partly overlapping
    test1 = land(p1,p2)
#    test2 = land(land(lnot(p1),lnot(p2)), lnot(test1))
    test2 = land(lnot(p1),lnot(p2))
    test3 = land(lnot(lor(test1,test2)), K<L)

    a1 = (test1.T*w1).T
    a2 = test2*w2
    a3 = test3*(L-K)

    return a1 + a2 + a3


def calc_overlap_inv(za1, zb1, za2, zb2):
    """Calculate both the overlap and the inverse. The
       inverse was used in the Limber approximation.
       Likelt it was not correct.
    """

    ans = calc_overlap(za1, zb1, za2, zb2)

    # Temporary disible errors when dividing on zero.
    old_val = np.seterr(divide='ignore')['divide']
    inv = 1./ans
    np.seterr(divide=old_val)
    inv = np.where(np.abs(ans) < .00001, 0., inv)

    return ans, inv
