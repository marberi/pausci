#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np
import itertools as it
from scipy.linalg import block_diag

from sci.corr import interval 

class ex_limber(object):
    """Limber approximation using different pieces from the exact
       calculations.
    """

    def limber_binnings(self, par):
        """Binning in z and k used for the Limber approximation."""

        z_bigD = {}
        k_bigD = {}

        for pop_name in self.all_pop:
            z = self.zD[pop_name]
            k = np.vstack(self.cosmo.k(par, z, l) for l in self.limber_lvals)
            z_big = np.tile(z, len(self.limber_lvals))
            k_big = k.flatten()

            z_bigD[pop_name] = z_big
            k_bigD[pop_name] = k_big

        return z_bigD, k_bigD

    def P_funkD(self, par, z_bigD, k_bigD):
        """Part containing the power spectrum."""

        funkD = {}
        nl = len(self.limber_lvals)
        for pop_name in self.all_pop:
            z = self.zD[pop_name]
            z_big = z_bigD[pop_name]
            k_big = k_bigD[pop_name]

            power_spec = self.P(par, z_big, k_big, True)
            power_spec = power_spec.reshape((nl, len(z)))

#            assert sum(power_spec < 0.) < 10
            assert (power_spec < 0.).sum() < 10
            power_spec = np.clip(power_spec,0., np.inf)

#            power_spec = np.clip(power_spec, 0., np.inf) # HACK
            r = self.cosmo.cache_ev(par, 'r', z)
            r_der = self.cosmo.cache_ev(par, 'r_der', z)

            P_funk = np.sqrt(power_spec/(r**2*r_der))

            funkD[pop_name] = P_funk

        return funkD

    def limber_bias(self, par, z_bigD, k_bigD):
        """Bias term when using the Limber approximation."""

        biasD = {}
        nl = len(self.limber_lvals)
        for obs in self.all_obs:
            ftype, pop = obs
            z_big = z_bigD[pop]
            k_big = k_bigD[pop]

            shape = (nl, len(self.zD[pop]))
            bias = self.bias.mean(par, obs, z_big, k_big, use_diag=True)
            biasD[obs] = bias.reshape(shape)

        return biasD

    def proj_overlap(self):
        """Projection and overlap matrices."""

        projD = {}
        for pop in self.all_pop:
            N_bins = self.conf['opts.N_bins.{0}'.format(pop)]
            nelem = N_bins + 2

            nbins = self.pop[pop]['nbins']
            projD[pop] = block_diag(*(nbins*[np.ones(nelem)])).T

        # Calculates the overlap between the bins and divide out with
        # the length of the bins.
        overD = {}
        npop = len(self.all_pop)
        for i,j in it.product(range(npop), range(npop)):
            if i < j: continue

            pop_name1 = self.all_pop[i]
            pop_name2 = self.all_pop[j]
            pop1 = self.pop[pop_name1]
            pop2 = self.pop[pop_name2]
            width1 = pop1['z_width']
            width2 = pop2['z_width']

            A = interval.calc_overlap(pop1['za'], pop1['zb'], pop2['za'], pop2['zb'])
            overlap = (A.T/width1).T/width2
            overD[pop_name1, pop_name2] = overlap
            overD[pop_name2, pop_name1] = overlap.T


        return projD, overD



    def TMPcl_limber(self, par):
        """Limber approximation using parts of the exact calculations."""

        # TODO: Move the new iterator interface inside here...

        nl = len(self.limber_lvals)
        nobs = len(self.all_obs)
        win_galD, win_lensD = self.windows(par)
        z_bigD, k_bigD = self.limber_binnings(par)
        funkD = self.P_funkD(par, z_bigD, k_bigD)
        biasD = self.limber_bias(par, z_bigD, k_bigD)
        projD, overD = self.proj_overlap()

        use_clust = self.conf['opts.use_clust']
        use_lens = self.conf['opts.use_lens']
        deriv_bias = self.conf['opts.deriv_bias']

        ans_cl = dict((l,{}) for l in self.limber_lvals)
        ans_D1 = dict((l,{}) for l in self.limber_lvals)
        ans_D2 = dict((l,{}) for l in self.limber_lvals)

        for i,j in it.product(range(nobs), range(nobs)):
            if i < j: continue

            obs1 = self.all_obs[i]
            obs2 = self.all_obs[j]
            ftype1, pop1 = obs1
            ftype2, pop2 = obs2

            F1 = funkD[pop1]*biasD[obs1]
            F2 = funkD[pop2]*biasD[obs2]
            over = overD[(pop1, pop2)]
            overM = np.dot(projD[pop1], np.dot(over, projD[pop2].T))

#            pdb.set_trace()
            for x,l in enumerate(self.limber_lvals):
                clx1 = use_clust*win_galD[obs1]*F1[x] + \
                      use_lens*win_lensD[obs1]*funkD[pop1][x]
                clx2 = use_clust*win_galD[obs2]*F2[x] + \
                      use_lens*win_lensD[obs2]*funkD[pop2][x]

                cl = np.dot(clx1, np.dot(overM, clx2.T))
                ans_cl[l][obs1,obs2] = cl
                ans_cl[l][obs2,obs1] = cl.T

                if deriv_bias:
                    Dx1 = use_clust*win_galD[obs1]*funkD[pop1][x] + \
                         use_lens*win_lensD[obs1]*funkD[pop1][x]
                    Dx2 = use_clust*win_galD[obs2]*funkD[pop2][x] + \
                         use_lens*win_lensD[obs2]*funkD[pop2][x]

                    D1 = np.dot(Dx1, np.dot(overM, clx2.T))
                    D2 = np.dot(clx1, np.dot(overM, Dx2.T))

                    ans_D1[l][obs1,obs2] = D1
                    ans_D2[l][obs1,obs2] = D2
                    if not i == j:
                        ans_D1[l][obs2,obs1] = D2.T
                        ans_D2[l][obs2,obs1] = D1.T

#                ans_D[l][obs2,obs1] = D.T
        ans = {'cl': ans_cl, 'D1': ans_D1, 'D2': ans_D2}

        return ans_cl, ans_D1, ans_D2 #_cl, ans_D

    def cl_limber(self, par):
        deriv_bias = self.conf['opts.deriv_bias']
        ans_cl, ans_D1, ans_D2 = self.TMPcl_limber(par)
        for l in self.limber_lvals:
            ans = {'u': l, 'corr_act': ans_cl[l]}
            if deriv_bias:
                ans['D1'] = ans_D1[l]
                ans['D2'] = ans_D2[l]

            yield ans


