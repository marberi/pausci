#!/usr/bin/env python
# encoding: UTF8
# Exact integration of the cross-correlations.

import ipdb
import itertools as it
import pdb
import sys
import time
import numpy as np

from sci.corr import ex_core
from sci.corr import ex_int
from sci.corr import ex_kappamod
from sci.corr import ex_limber
from sci.corr import ex_phys
from sci.corr import ex_precalc
from sci.corr import lens
from sci.corr import libclconv

def nrcorr(ans):
    """Number of correlations in ans."""

    if hasattr(ans, 'shape'):
        return np.product(ans.shape)
    elif isinstance(ans, dict):
        return np.sum(nrcorr(x) for x in ans.values())
    else:
        raise

class corr_exact(ex_core.ex_core,
                 ex_kappamod.ex_kappamod,
                 ex_int.ex_int,
                 ex_limber.ex_limber,
                 ex_phys.ex_phys,
                 ex_precalc.ex_precalc,
                 object):

    msg_recalc = 'Some lvalues calculated both with limber and the exact calculations.'

    def __init__(self, bias, conf, cosmo, pop, observ):
        self.bias = bias
        self.conf = conf
        self.cosmo = cosmo
        self.pop = pop
        self.observ = observ

        self.lens = lens.lens(conf, cosmo, pop)
        self._corr_cache = {}

    def find_cl(self, par):
        exact_iter = self.cl_exact(par)
        for CL in exact_iter:
            yield CL

        # In cases use_limber is True, but is not used since
        # opts.lsplit is too large.
        if not self.conf['opts.use_limber'] or \
           not len(self.limber_lvals):
            raise StopIteration

        limber_iter = self.cl_limber(par)
        for CL in limber_iter:
            yield CL

    def NEWfind_cl(self, par):
        exact_iter = self.cl_exact(par)
        limber_iter = self.cl_limber(par)

        for cl_exact, cl_limber in it.izip(exact_iter, limber_iter):
#            for key1 in cl_exact.keys():
            for key, exact_val in cl_exact.iteritems():
                if key == 'u': continue

                limber_val = cl_limber[key]
                for obs, corr in exact_val.iteritems():
                    (ftype1, pop1), (type2, pop2) = obs
#                    corr
#                    val2[
                    mask = self.mask[pop1,pop2]
                    corr[mask] = limber_val[obs][mask]

#                    pdb.set_trace()
#            pdb.set_trace()

            yield cl_exact

    def to_wtheta(self, A, key, uvals):

        B = [x[key] for x in A]

        pdb.set_trace()

    def interp_values(self, cl_iter, observD, return_cl=False):
        # This functionality should be improved and then moved elsewhere. It is
        # a first test...

        A = list(cl_iter)
        uvals = [x['u'] for x in A]
        if self.pop['meta']['fourier'] or return_cl:
            print('using fourier..')
            for x in A:
                yield x

            raise StopIteration

        # Converting to w(theta).
        keys = A[0].keys()
        keys.remove('u')

#        tmp_wtheta = {}

        # Useless code. Needed because how the data is stored.
        wtheta = dict([(x,{'u': x}) for x in self.th_mean]) 
        for key in keys:
            cls = dict(zip(uvals, [x[key] for x in A]))
            tmp_wtheta = libclconv.cl_to_w(observD, self.S, self.th_mean, cls) 
            for th in self.th_mean:
                wtheta[th][key] = tmp_wtheta[th]

        for th in self.th_mean:
            yield wtheta[th]


    def create_iter(self, par, observD, return_cl=False):

        if not hasattr(self, '_finished_precalc'):
            all_fluct = observD['all_fluct']
            all_pop = observD['all_pop']
            self._precalc(observD)

        cl_iter = self.find_cl(par)

#        pdb.set_trace()
        return self.interp_values(cl_iter, observD, return_cl)

##        pdb.set_trace()
#
#        if self.pop['meta']['fourier']:
#            return cl_iter
#
#        A = list(cl_iter)
#
#        assert False, 'Debug...'
#        # I have not considered the derivatives yet..        
#        cls_act = dict(zip(self.all_lvals, [x['corr_act'] for x in A]))
#        w_act = libclconv.cl_to_w(observD, self.S, self.th_mean, cls_act)
#
#        def _w_iter():
#            for u in observD['u_mean']:
#                print('u', u)
#                res = {'corr_act': w_act[u], 'u': u}
#
#                yield res
#
#        return _w_iter()
 
    def __call__(self, par, u, observD):
        """Return the observables for all combinations."""

        raise NotImplemented('This functionality is no longer supported...')

        all_fluct = observD['all_fluct']
        all_pop = observD['all_pop']

        if not hasattr(self, '_finished_precalc'):
            self._precalc(all_fluct, all_pop)

        ufourier = self.pop['meta']['fourier']
        if not par['hash'] in self._corr_cache:
            cls = self.find_cl(par)

            if ufourier:
                self._corr_cache[par['hash']] = cls
            else:
                w0 = libclconv.cl_to_w(observD, self.S, self.th_mean, cls[0])
                w1 = libclconv.cl_to_w(observD, self.S, self.th_mean, cls[1])
                w2 = libclconv.cl_to_w(observD, self.S, self.th_mean, cls[2])
                self._corr_cache[par['hash']] = w0,w1,w2

#                ipdb.set_trace()

        # Not exactly the best way...
        if ufourier:
            u_key = np.round(u)
        else:
            u_key = np.round(u, decimals=4)


        incache = self._corr_cache[par['hash']]
        ans = {'corr_act': incache[0][u_key], 'D1': incache[1][u_key], 'D2': incache[2][u_key]} 

        return ans
