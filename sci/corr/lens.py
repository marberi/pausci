#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function, unicode_literals

import pdb
import numpy as np
import itertools as it

class lens(object):
    def __init__(self, conf, cosmo, pop):
        self.conf = conf
        self.cosmo = cosmo
        self.pop = pop

        self.cache = {}

    def cache_param(self, par, keep_values=False):
        """Lensing cache for parameter."""

        lens_eff = {}
        tolens = {}

        pop = self.pop['meta']['pop']
        for pop1 in pop:
            for pop2 in pop:
                key = (pop1, pop2)

                lens_eff[key] = self.lensing_eff(par, pop1, pop2)
                tolens[key] = self.tolens(pop1, pop2)


        cache = {'lens_eff': lens_eff, 'tolens': tolens}
        par_hash = par['hash']
        if keep_values:
            self.cache[par_hash] = cache
        else:
            self.cache = {par_hash: cache}

    def gen_cache(self, diffs, to_calc):
        for param, (par, delta) in diffs.items():
            self.cache_param(par, True)

    def eta(self, par, z1, z2):

        n1 = len(z1)
        n2 = len(z2)

        chi1 = self.cosmo.cache_ev(par, 'chi', z1)
        chi2 = self.cosmo.cache_ev(par, 'chi', z2)

        e1 = np.ones((n1,n1))
        e2 = np.ones((n2,n2))
        e = np.ones((n1,n2))

        # Lensing only apply when the second bin lies
        # behind the first. The binnings can be different,
        # so we can not rely on index tricks.
        b = chi2*e-(e.T*chi1).T 
        to_lens = (0 < b)
        eta = self.cosmo.f_K(par, b)/\
              self.cosmo.f_K(par, chi2)

#        if 51  < len(z1) or 51 < len(z1):
#            pdb.set_trace()


        eta = to_lens*eta
#        pdb.set_trace()

        return eta

    def lensing_eff_core(self, par, z1, z2):
        """Lensing effiency for lens redshift z1 and source
           redshift z2.
        """

        r = self.cosmo.cache_ev(par, 'r', z1)
        H = self.cosmo.cache_ev(par, 'H', z1)

        chi_H0 = par['c'] / (par['H_0'])
        if self.conf['opts.h_bug']:
            chi_H0 = chi_H0 / par['h']

        eta = self.eta(par, z1, z2) #pop1, pop2)
        a = 1./(1.+z1)
        pre = (par['H_0']/H)*(r/(a*chi_H0))

        if self.conf['opts.h_bug']:
            pre = pre * par['h']

        ans = (3./2.)*par['om_m']*(eta.T*pre.T).T 

#        if 51  < len(z1) or 51 < len(z1):
#            pdb.set_trace()

        return ans

    def lensing_eff(self, par, pop1, pop2):
        """Efficiency of the lensing."""

        z1 = self.pop[pop1]['z_mean']
        z2 = self.pop[pop2]['z_mean']

        return self.lensing_eff_core(par, z1, z2)

    def tolens(self, pop1, pop2):
        z1 = self.pop[pop1]['z_mean']
        z2 = self.pop[pop2]['z_mean']

        n1 = len(z1)
        n2 = len(z2)
    
        tolens = np.zeros((n1, n2))
        for i in range(n1):
            for j in range(n2):
                if z1[i] < z2[j]:
                    tolens[i,j] = True
    
        return tolens
    
