#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np
from scipy.interpolate import splev

class ex_phys(object):
    def P(self, par, z, k, only_diag=False):

        if self.conf['opts.use_nl']:
            # The non-linear power spectrum already had D**2 included.
            par_hash = par['hash']
            spl = self.cosmo.cache[par_hash]['pk_nl']

            if not only_diag:
                nk, nz = len(k), len(z)
                k_vals = np.repeat(k, len(z))
                z_vals = np.tile(z, len(k))

                A = spl.ev(k_vals, z_vals)
                pk_nl = A.reshape((nk, nz)).T
            else:
                pk_nl = spl.ev(k, z)

#            pdb.set_trace()
            return pk_nl

        else:
            pk = self.cosmo.cache_ev(par, 'plin', k)
            D = self.cosmo.cache_ev(par, 'D', z)
            pk_lin = D**2*pk if only_diag else np.outer(D**2, pk)

            return pk_lin

    def lensD(self, par):
        """Lensing efficiency between different populations."""

        lensD = {}
        for pop1 in self.all_pop:
            for pop2 in self.all_pop:
                z1 = self.zD[pop1]
                z2 = self.zD[pop2]

                p = self.lens.lensing_eff_core(par,z1,z2)
                lensD[(pop1,pop2)] = p

        return lensD

#    def fD(self, par, zD, kD):
#
#        fD = {}
#        for pop in self.all_pop:
#            z = zD[pop]
#            k = kD[pop]
#
#            om_m = self.cosmo.cache_ev(par, 'om_m', z)
#            if self.conf['opts.use_limber']:
#                fD[pop] = {}
#                for li,l in enumerate(self.ex_lvals):
#                    gamma = self.cosmo.gamma(par, z, k[li], True)
#                    fD[pop][l] = (om_m**gamma.T).T
#
##                    pdb.set_trace()
#            else:
#                gamma = self.cosmo.gamma(par, z, k)
#                fD[pop] = (om_m**gamma.T).T
#
#        return fD

    def fD(self, par, zD, kD):

        fD = {}
        for pop in self.all_pop:
            z = zD[pop]
            k = kD[pop]

            om_m = self.cosmo.cache_ev(par, 'om_m', z)
            gamma = self.cosmo.gamma(par, z, k)
            fD[pop] = (om_m**gamma.T).T

        return fD





    def biasD(self, par, kD):
        """Dictionary with bias for different observables."""

        biasD = {}
        for obs in self.all_obs:
            _, pop = obs
            z = self.zD[pop]
            k_pop = kD[pop]

#            if self.conf['opts.use_limber']:
#                biasD[obs] = {}
#                for li,l in enumerate(self.all_lvals):
#                    biasD[obs][l] = self.bias.mean(par, obs, z, k_pop[li], True)
#            else:

#            pdb.set_trace()
            biasD[obs] = self.bias.mean(par, obs, z, k_pop, use_diag=False)

        return biasD

    def alphaD(self):
        """Dictionary with slope for different observables."""

        alphaD = {}
        for obs in self.all_obs:
            ftype, pop = obs
            z = self.zD[pop]
            alpha_spl = self.pop[pop]['alpha']['%s_spl' % ftype]
            alphaD[obs] = splev(z, alpha_spl)

        return alphaD

