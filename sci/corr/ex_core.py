#!/usr/bin/env python
# encoding: UTF8
from __future__ import print_function
import hashlib
import pdb
import sys
import time
import numpy as np
import itertools as it

try:
    import matplotlib.pyplot as plt
    has_mpl = True
except ImportError:
    has_mpl = False

# For benchmarking..
def nrcorr(ans):
    """Number of correlations in ans."""

    if hasattr(ans, 'shape'):
        return np.product(ans.shape)
    elif isinstance(ans, dict):
        return np.sum(mcorr(x) for x in ans.values())
    else:
        raise

# For tesing..
def corrhash(A):
    """Hash value for the correlation function. Useful for
       testing.
    """

    obj = A[50]
    keys = obj.keys()
    keys.sort()

    s = ''
    for key in keys:
        s += (1e9*obj[key]).astype(np.int).tostring()

      
    return hashlib.sha1(s).hexdigest()
 
class ex_core(object):
#    @profile
    def windows(self, par):
        """Galaxy and lensing window functions."""

        use_lens = self.conf['opts.use_lens']
        if use_lens:
            alphaD = self.alphaD()
            lensD = self.lensD(par)

        win_galD, win_lensD = {}, {}
        for obs in self.all_obs:
            ftype, pop = obs

            w_z = self.w_redshiftD[pop]
            win_galD[obs] = w_z
            if use_lens:
                lens_eff = lensD[(pop, pop)]
                w_lens = self.w_lensD[pop]
                alpha = alphaD[obs]

                win_lensD[obs] = np.dot(w_z, w_lens*(lens_eff*alpha).T)
            else:
                win_lensD[obs] = 0.

        return win_galD, win_lensD

#    @profile
    def _core(self, win_galD, win_lensD, ypsi_mat, ypsi_gal):
        """Core of the cl calculations."""

        pre = 2. / np.pi
        use_clust = self.conf['opts.use_clust']
        use_lens = self.conf['opts.use_lens']
        intr_rem = self.conf['opts.intr_rem']

        galD, lensD = {}, {}
        derivD = {}
        for obs in self.all_obs:
            ftype, pop = obs
            galD[obs] = np.dot(win_galD[obs], ypsi_gal[obs])
            derivD[obs] = np.dot(win_galD[obs], ypsi_mat[pop])

            if use_lens:
                lensD[obs] = np.dot(win_lensD[obs], ypsi_mat[pop])
            else:
                lensD[obs] = 0.

        nobs = len(self.all_obs)
        ans_cl = {}
        ans_D1 = {}
        ans_D2 = {}
        deriv_bias = self.conf['opts.deriv_bias']
        for i,j in it.product(range(nobs), range(nobs)):
            if i < j: continue

            obs1 = self.all_obs[i]
            obs2 = self.all_obs[j]
            ftype1,pop1 = obs1
            ftype2,pop2 = obs2

            L1 = use_clust*galD[obs1] + use_lens*lensD[obs1]
            L2 = use_clust*galD[obs2] + use_lens*lensD[obs2] 

            cl = pre*np.dot(L1, L2.T)

            if intr_rem and use_clust:
                cl -= pre*self.mask[pop1,pop2]*np.dot(galD[obs1], galD[obs2].T)

#            pdb.set_trace()
            # Removing problem with long range artificial intrinsic 
            # correlations.
#            if False and use_clust:
#                cl_gg = pre*np.dot(galD[obs1], galD[obs2].T)
#                cl -= np.where(np.abs(cl) < 1e-6, cl_gg, np.zeros(cl.shape))

            ans_cl[(obs1,obs2)] = cl
            if not i == j:
                ans_cl[(obs2,obs1)] = cl.T

            # Derivative of the bias.
            if deriv_bias:
                D1 = pre*np.dot(derivD[obs1], L2.T)
                D2 = pre*np.dot(L1, derivD[obs2].T)
                ans_D1[obs1, obs2] = D1
                ans_D2[obs1, obs2] = D2

                if not i == j:
                    ans_D1[obs2, obs1] = D2.T
                    ans_D2[obs2, obs1] = D1.T

        res = {'corr_act': ans_cl}
        if deriv_bias:
            res['D1'] = ans_D1
            res['D2'] = ans_D2

        return res

    def cl_exact(self, par):
        """Exact calculation of the correlation functions."""

        N = self.conf['opts.nk']

        win_galD, win_lensD = self.windows(par)
        psi_iter = self.psi(par, N)
        kappa_mod = self.find_kappa_mod(par)

        # The splitting here is not good yet..
        for l in self.ex_lvals:
            psi_mat, psi_gal = psi_iter.next()
            X = self._core(win_galD, win_lensD, psi_mat, psi_gal)
            X['corr_act'] = self.add_kappa_correction(par, l, X['corr_act'], kappa_mod)
            X['u'] = l

            yield X
