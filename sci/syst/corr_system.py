#!/usr/bin/env python
# encoding: UTF8
from __future__ import unicode_literals

# Module for adding systematic. It was separated
# from the correlation code to make it easier to
# write several different correlation codes.
import copy
import hashlib
import pdb
import time
import itertools as it

import numpy as np

import sci

def rev_keys(d):
    """Swap orders of keys in a tree."""

    keys = [list(x.keys()) for x in d.values()]
    unique_keys = list(set(sum(keys, [])))

    new_d = dict((x,{}) for x in unique_keys)
    for key1,val1 in d.items():
        for key2,val2 in val1.items():
            new_d[key2][key1] = val2

    return new_d

class corr_system(object):
    """Observational systematics like photo-z and
       shot-noise which should be added to the
       observables.
    """

    def __init__(self, conf, pop):
        self.conf = conf
        self.pop = pop

#        self.int_overD = self.create_int_over()
        self.int_overD, self.lens_overD = self._deriv_overlap()

    def _lens_overlap(self, za1, zb1, za2, zb2):
        A = np.zeros((len(za1), len(za2)))

        w1 = zb1 - za1
        for i in range(len(za1)):
            for j in range(len(za2)):
                if zb1[i] <= za2[j]+1e-4:
                    A[i,j] = 1.
                elif zb2[j] < za1[i]+1e-4:
                    A[i,j] = 0.
                else:
                    A[i,j] = 0. #(zb1[i] - za2[j]) / w1[i]

        return A

    def _deriv_overlap(self): # observD):

        # Overlap 
        deriv_overD = {}
        all_pop = self.pop['meta']['pop']
        npop = len(all_pop)
        int_overD = {}
        lens_overD = {}
        for i,j in it.product(range(npop), range(npop)):
#            if i < j: continue

            pop_name1 = all_pop[i]
            pop_name2 = all_pop[j]
            pop1 = self.pop[pop_name1]
            pop2 = self.pop[pop_name2]
            width1 = pop1['z_width']
            width2 = pop2['z_width']

            t1 = time.time()
            A = sci.corr.interval.calc_overlap(pop1['za'], pop1['zb'], pop2['za'], pop2['zb'])
            B = self._lens_overlap(pop1['za'], pop1['zb'], pop2['za'], pop2['zb'])
            t2 = time.time()

            # Only looking at how much overlap there is with the first bin.
            int_overD[pop_name1, pop_name2] = (A.T/width1).T #/width2
            lens_overD[pop_name1, pop_name2] = B
#            pdb.set_trace()

        return int_overD, lens_overD

    def add_trans(self, par, corr_act, u, obs):
        """Add photoz transitions to correlations."""

        (ftype1, pop1), (ftype2, pop2) = obs

        if (not self.pop.pkg) and self.conf['opts.trans_mat']:
            r1 = self.pop[pop1]['rel_trans']
            r2 = self.pop[pop2]['rel_trans']

            corr_part = {}
            corr_part['corr_act'] = corr_act
            corr_part['corr_obs'] = r1*corr_act*r2.T 
            corr_part['L'] = r1*corr_act
            corr_part['R'] = corr_act*r2.T
        else:
            all_fields = ['corr_act', 'corr_obs', 'L', 'R']
            corr_part = dict(((x,corr_act) for x in all_fields))

        return corr_part
    
    def add_bias_deriv(self, par, corrD, u, obs):
        """Term used when finding the derivative of bias in each
           redshift bin.
        """

        (ftype1, pop1), (ftype2, pop2) = obs
        obs1, obs2 = obs

        if not self.pop.pkg:
            r1 = self.pop[pop1]['rel_trans']
            r2 = self.pop[pop2]['rel_trans']

            corr_part = {}
            corr_part['I1'] = (corrD['D1'][obs1,obs2]*self.int_overD[pop1, pop2])*r2.T
            corr_part['I2'] = r1*(corrD['D2'][obs1,obs2]*self.int_overD[pop1, pop2])
            corr_part['L1'] = (corrD['D1'][obs1,obs2]*self.lens_overD[pop1, pop2])*r2.T
            corr_part['L2'] = r1*(corrD['D2'][obs1,obs2]*self.lens_overD[pop1, pop2])

            corr_part['I1'] = corrD['D1'][obs1,obs2]*r2.T
            corr_part['I2'] = r1*corrD['D2'][obs1,obs2]

#            pdb.set_trace()
        #    H1 = np.logical_or(self.int_overD[pop1, pop2], self.lens_overD[pop1, pop2])
        #    corr_part['K1'] = (corrD['D1'][obs1,obs2]*(self.int_overD[pop1, pop2])*r2.T
        else:
            all_fields = ['DL', 'DR']
            corr_part = dict(((x,corr_deriv) for x in all_files))

        return corr_part
 
    def add_shotnoise(self, par, c, u, obs):
        """Add shotnoise, shape measurement noise or errors in magnitudes depending
           on the observable.
        """

        c_noise = c.copy()

        if obs in self.pop['noise']:
            noise_part = self.pop['noise'][obs]
            c_noise += noise_part

        return c_noise

    def add_stoch(self, par, c, u, obs):

        (ftype0, pop0), (ftype1, pop1) = obs
        if not (ftype0 == 'counts' and ftype1 == 'counts'):
            return c

        if not pop0 == pop1:
            return c

        stoch_level = self.conf['fid.pop.{0}.stoch'.format(pop0)]

        c += stoch_level*np.diag(np.ones(c.shape[0]))

        return c

    def __call__(self, par, u, corrD_in):

        res = {}
        for obs, corr_act in corrD_in['corr_act'].items():
            corr_part = self.add_trans(par, corr_act, u, obs)
            corr_part['corr_obs'] = self.add_shotnoise(par, corr_part['corr_obs'], u, obs)
            corr_part['corr_obs'] = self.add_stoch(par, corr_part['corr_obs'], u, obs)

            res[obs] = corr_part

        if self.conf['opts.deriv_bias']:
            for obs in corrD_in['corr_act'].keys():
                res[obs].update(self.add_bias_deriv(par, corrD_in, u, obs))

        corrD = rev_keys(res)

        # It should probably have been set before.
        corrD['u'] = u

        return corrD
